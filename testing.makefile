SHELL:=bash

clean:
	conda remove --name dtop_spey --all

reset: clean
	conda env create --name dtop_spey --file environment.yml --force 
	conda env list

# -------------------------------------------
# Run Pytest tests locally

x-rm-dist:
	rm -rf ./dist

x-create-dist:
	(cd ./dtop_shared_library_submodule && python ./setup.py sdist --dist-dir ../dist)
	python setup.py sdist

x-uninstall:
	pip uninstall --yes dtop_shared_library
	pip uninstall --yes dtop_spey

x-install: x-uninstall x-rm-dist x-create-dist
	pip install dtop_shared_library --no-cache-dir --find-links dist
	pip install dtop_spey --no-cache-dir --find-links dist

pytest: x-pytest-prepare x-pytest

x-pytest-prepare:
	pip install jsonschema
	pip install jsonpath-rw
	pip install pact-python
	pip install pyyaml
	pip install pytest==6.2.4 pytest-cov==2.12.1
	pip install dtop_spey --no-cache-dir --find-links dist

x-pytest:
	export DATABASE_URL=sqlite:///../databases/spey.db ; python -m pytest --cov=dtop_spey --junitxml=report.xml --ignore=dtop_shared_library_submodule

# -------------------------------------------
# Run Dredd tests locally

dredd-local: x-dredd-prepare x-dredd-local

x-dredd-prepare:
	pip install python-dotenv
	pip install dredd_hooks
	npm uninstall dredd --global
	npm install dredd@12.2.1 --global

x-dredd-local:
	flask init-db
	bash -c "dredd --config dredd-local.yml"
