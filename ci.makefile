ifeq ($(OS),Windows_NT)
	SHELL:=bash
	win_pwd := $(shell cygpath -w $(shell pwd))
	python_v := python
	docker-run = docker run --rm --volume $(win_pwd):/code
	docker-kept-run = docker run --volume $(win_pwd):/code
else
	python_v := python3
	docker-run = docker run --rm --volume ${PWD}:/code
	docker-kept-run = docker run --volume ${PWD}:/code
endif

export DTOP_DOMAIN = dto.test

test: clean pytest-mb dredd

install: spey-deploy
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --detach  spey client nginx

spey-base:
	cp -f ./environment.yml ./docker/base/
	(cd ./docker/base/ && docker build --tag spey-base .)

rm-dist:
	rm -rf ./dist

dist/dtop_spey-*.tar.gz:
	$(python_v) setup.py sdist

dist/dtop_shared_library-*.tar.gz:
	(cd ./dtop_shared_library_submodule && $(python_v) ./setup.py sdist --dist-dir ../dist)

x-build: rm-dist dist/dtop_spey-*.tar.gz dist/dtop_shared_library-*.tar.gz

build: spey-base x-build

spey-deploy: build
	cp -fr ./dist ./docker/deploy/
	(cd ./docker/deploy/ && docker build --cache-from=spey-deploy --tag spey-deploy .)

spey-deploy-docker: spey-base
	(docker build -f deploy.dockerfile --cache-from=spey-deploy --tag spey-deploy .)

spey-pytest: spey-deploy-docker
	cp -fr ./test ./docker/pytest/
	cp -f ./testing.makefile ./docker/pytest/
	(cd ./docker/pytest/ && docker build --tag spey-pytest .)

pytest: spey-pytest
	$(docker-kept-run) --name spey-pytest-cont spey-pytest make --file testing.makefile pytest
	docker cp spey-pytest-cont:/app/report.xml .
	docker container rm spey-pytest-cont

pytest-mb: spey-deploy-docker
	docker-compose \
	--file docker-compose.yml \
	--file docker-compose.test.yml \
	up --abort-on-container-exit --exit-code-from=pytest \
	spey pytest mc.dto.test sc.dto.test lmo.dto.test ed.dto.test ec.dto.test et.dto.test

shell-pytest: spey-pytest
	$(docker-run) --tty --interactive spey-pytest bash

dredd: spey-deploy-docker
	touch ./hooks-output.txt
	docker-compose --file docker-compose.yml --file docker-compose.test.yml down
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --build --abort-on-container-exit --exit-code-from=dredd  dredd

clean:
	rm -fr dist
	rm -fr ./docker/deploy/dist
	rm -fr ./docker/pytest/test
	docker image rm --force spey-base spey-deploy spey-pytest spey-dredd

cypress-run: spey-deploy-docker
	docker-compose --file docker-compose.yml --file docker-compose.test.yml down
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --detach  spey client nginx e2e-cypress
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 client:8080
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 spey:5000
	docker exec e2e-cypress npx cypress run
	docker exec e2e-cypress npx nyc report --reporter=text-summary

## !
## Set DTOP domain
## DTOP_DOMAIN=dto.opencascade.com


## !
## Set the value to cors urls
ALL_MODULES_NICKNAMES := si sg sc mc ec et ed sk lmo spey rams slc esa cm mm
CORS_URLS := $(foreach module,${ALL_MODULES_NICKNAMES},http://${module}.${DTOP_DOMAIN},https://${module}.${DTOP_DOMAIN},)


## !
## Set the required module nickname
MODULE_SHORT_NAME=spey

## !
## Set the required docker image TAG
SPEY_IMAGE_TAG=v1.7.1


## !
## Update the values of SPEY_CI_REGISTRY to your module registry
#SPEY_CI_REGISTRY?=registry.gitlab.com/opencascade/dtocean/fork-spey
SPEY_CI_REGISTRY=registry.gitlab.com/dtoceanplus/dtop_spey

login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${SPEY_CI_REGISTRY}

logout:
	docker logout ${SPEY_CI_REGISTRY}

build-prod-be:
	docker pull ${SPEY_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${SPEY_IMAGE_TAG} || true
	docker build --cache-from ${SPEY_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${SPEY_IMAGE_TAG} \
	  --tag ${SPEY_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${SPEY_IMAGE_TAG} \
	  --file ./${MODULE_SHORT_NAME}-prod.dockerfile \
          .
	docker push ${SPEY_CI_REGISTRY}/${MODULE_SHORT_NAME}_backend:${SPEY_IMAGE_TAG}
	docker images

build-prod-fe:
	docker pull ${SPEY_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${SPEY_IMAGE_TAG} || true
	docker build --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
	--cache-from ${SPEY_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${SPEY_IMAGE_TAG} \
	  --tag ${SPEY_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${SPEY_IMAGE_TAG} \
      --file ./src/dtop_spey/frontend/frontend-prod.dockerfile \
	  ./src/dtop_spey
	docker push ${SPEY_CI_REGISTRY}/${MODULE_SHORT_NAME}_frontend:${SPEY_IMAGE_TAG}
	docker images

