import requests
from requests.exceptions import ConnectionError
from dredd_utils import *

endpoint = "http://localhost:5000/spey"

# Recovering back sys.stdout and sys.stderr redirected into file in dredd_utils.py.
sys.stdout = sys.__stdout__
sys.stderr = sys.__stderr__

try:
    r = requests.get(endpoint)
except ConnectionError:
    print("the module server is not available")
    exit()


def post_project():
    print(
        "POST project :: creating of new SPEY assessment basing on the `body` test data defined in dredd_utils.py"
    )
    r = requests.post(endpoint, json=body)
    print("Returned code :")
    print(r.status_code)
    if r.status_code == 404:
        print("Check server code for endpoint = " + endpoint)
    return r.status_code


post_project()
