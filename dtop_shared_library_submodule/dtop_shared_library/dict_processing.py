"""
Utilities for working with dictionaries.
"""

from copy import deepcopy
from typing import Any, Dict, Iterable, List

import jsonpath_rw


def filter_dict(data: Dict[str, Any], paths: Iterable[str]) -> Dict[str, Any]:
    """
    Given a dictionary, returns another dictionary which include only items that match filters.
    Each filter must be a [JSON Path](https://github.com/kennknowles/python-jsonpath-rw).
    """
    # Find all items that we want to keep
    # Also, find items that are not required explicitly but contain one or more required items
    required_items = []
    required_parents = []
    for path in paths:
        for item in jsonpath_rw.parse(path).find(data):
            required_items.append(item.value)
            while item.context is not None:
                item = item.context
                if item.value not in required_parents:
                    required_parents.append(item.value)
    required_parents = list(x for x in required_parents if x not in required_items)

    def is_required_item(value) -> bool:
        return any(x is value for x in required_items)

    def is_required_parent(value) -> bool:
        return any(x is value for x in required_parents)

    # Produce a copy of given data
    # If data is a dict or a list, copy only items
    # that are required themselves or contain required child items
    # When a child is required explicitly, we copy it directly using deepcopy()
    # When a child is required implicitly, use this function recursively on it
    def _copy(source):
        if isinstance(source, dict):
            target = {}
            for key, value in source.items():
                if is_required_item(value):
                    target[key] = deepcopy(value)
                elif is_required_parent(value):
                    target[key] = _copy(value)
            return target

        assert isinstance(source, list)
        target = []
        for value in source:
            if is_required_item(value):
                target.append(deepcopy(value))
            elif is_required_parent(value):
                target.append(_copy(value))
        return target

    return _copy(data)


def merge_dicts(*dicts: Dict) -> Dict:
    """
    Merge two or more dictionaries recursively.
    """
    result = {}
    for current in dicts:
        for key, new in current.items():
            if key in result:
                old = result[key]
                if isinstance(new, dict):
                    result[key] = merge_dicts(old, new)
                elif isinstance(new, list):
                    result[key] = merge_lists(old, new)
                else:
                    result[key] = new
            else:
                result[key] = new
    return result


def merge_lists(*lists: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    """
    Merge two or more lists, based on 'id' properties inside each item.
    """
    # Instead of reinventing the wheel,
    # re-use the implementation of merge_dicts()
    dicts = []
    for _list in lists:
        dicts.append({x["id"]: x for x in _list})
    result = merge_dicts(*dicts)
    return list(result.values())
