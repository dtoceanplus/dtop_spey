"""
This function simulates the failure events.

"""

import math
import random


def sim_failure_event(component_id, failure_rate, t_life, prob_dist="Exponential"):

    """
    simulates the failure events of basic components in parallel.
    estimates the reliability metric, i.e. MTTFs of basic components.
    int - integer
    Attributes:
    input-
    component_id (list - int): a 1D list containing the Ids of components
    , unit [-]
    failure_rate (list - float): a 1D list including failure rates of
    basic components, unit [1/hour]
    nr_seed (scaler - float): a random number uniformally sampled
    in (0, 1), unit [-]
    num_comp (scaler - int): the no. of components, unit [-]
    prob_dist (string): the probabilitstic distribution of time to failure
    (only 'exponential' considered), unit [-]
    t_random (scaler - int): the randomly simulated failure time
    , unit [hour]
    temp (list): a temporary list storing the failure times of
    a component, unit [-]
    t_life (scaler - int): the lifetime, unit [year]
    t_round (scaler - int): the time of failure events rounded to
    hour, unit [-]
    output-
    mttf (array - float): an array containing the mean time to failure of
    components in componentid, unit [hour]
    ts_failure_event (list - int): a 2D list including the failure time
    , unit [hour]
    """
    try:

        if ((prob_dist == "Exponential")) and (all((0 < i < 1) for i in failure_rate)):
            life_time = int(t_life * 365 * 24)
            num_comp = len(failure_rate)
            ts_failure_event = [[0] * 1] * num_comp
            for i in range(num_comp):
                temp = []
                t_random = 0
                t_0 = 0
                t_round = 0
                while t_round < life_time:
                    nr_seed = random.random()
                    t_random = -(365 * 24 / failure_rate[i]) * math.log(1 - nr_seed)
                    t_round = t_round + math.floor(t_random)
                    if t_round > t_0:
                        temp.append(t_round)
                        t_0 = t_round
                num = len(temp)
                del temp[num - 1]
                ts_failure_event[i] = temp
        else:
            ts_failure_event = [[]]
            raise TypeError
    except TypeError:
        _w = '"Exponential" distribution is the only available option for'
        _w = _w + " TTF at Stage 1! Failure rates must be in (0, 1)!"
        print(_w)
    return [component_id, ts_failure_event]
