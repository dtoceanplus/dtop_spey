# -*- coding: utf-8 -*-
"""
This modules calculates the Energy Production
"""

import pandas as pd


class EnergyProductionBase:
    """
    EnergyProductionBase: the class assesses the Energy Production
                          and Losses due to downtime of the Ocean
                          Energy System at array/device level.
    Args:
    monthly_resource_histogram =
                    The Monthly Histogram of the resource (Hs for Wave
                    Energy, Vc for Tidal Energy) with assigned bins. It
                    is a dictionary, whose keys are "bins" (the centroid
                    of the bin) and "January", "February", "March",
                    "April", "May", "June", "July", "August",
                    "September", "October", "November", "December".
                    The value for each key is a list with the same length
                    of bins
    power_delivery_histogram =
                    It is the histogram of the power delivery of the array.
                    It is a dictionary, with keys "bins" "power". The bins
                    must be the same as monthly_resource_histogram, and the
                    value for power is a list with same lkenght of bins
    number_devices = number of devices
    project_life = the project life in years.

    Optional args:
    downtime_hours_per_device = It is a dictionary of poandas Table. The
                                keys are the device ids; the pandas tables
                                have set as index the year of the project life
                                (form 1 to project life) amd the columns are
                                names as the month (first capital letter)

    Attributes:
    cpx = Level of the complexity design
    array_lifetime_gross_energy_pd = (scalar) the total gross energy of the
                                     array during all the life
    array_annual_gross_energy_pd = (Pandas table) the annual gross energy of
                                   the array during all the life. it is a pandas
                                   table with just one column, the index are the
                                   years. the index are the years (the same values
                                   are repeated); the only column is "Annual Energy"
                                   useful for interacting with the other methods
    array_monthly_gross_energy_pd = (Pandas table) the monthly array power production;
                                    columns are the months, index the years
    device_lifetime_gross_energy_pd = (dictionary of scalars) the total gross energy
                                      for each device. keys are the device id
    device_annual_gross_energy_pd = (dictionary of Pandas tables) the total gross
                                    energy for each device. keys of the dictionary
                                    are the device id. Each pandas has index the years
                                    and column "Annual Energy"
    device_monthly_gross_energy = (dictionary of Pandas tables) the monthly energy
                                  production per device.  keys of the dictionary are
                                  the device id; columns are the months, index the years.

    array_lifetime_lost_energy_pd = (scalar) the total lost energy of the array
                                    during all the life
    array_annual_lost_energy_pd = (Pandas table) the annual lost energy of the
                                  array during all the life. it is a pandas table
                                  with just one column, the index are the years.
                                  the index are the years (the same values are repeated);
                                  the only column is "Annual Energy" useful for interacting
                                  with the other methods
    array_monthly_lost_energy_pd = (Pandas table) the monthly array power production; columns
                                   are the months, index the years
    device_lifetime_lost_energy_pd = (dictionary of scalars) the total lost energy for each
                                     device. keys are the device id
    device_annual_lost_energy_pd = (dictionary of Pandas tables) the total lost energy for
                                   each device. keys of the dictionary are the device id.
                                   Each pandas has index the years and column "Annual Energy"
    device_monthly_lost_energy = (dictionary of Pandas tables) the monthly energy production
                                 per device.  keys of the dictionary are the device id; columns
                                 are the months, index the years.

    array_lifetime_net_energy_pd = (scalar) the total net energy of the array during all the
                                   life
    array_annual_net_energy_pd = (Pandas table) the annual net energy of the array during all
                                 the life. it is a pandas table with just one column, the index
                                 are the years. the index are the years (the same values are
                                 repeated); the only column is "Annual Energy" useful for
                                 interacting with the other methods
    array_monthly_net_energy_pd = (Pandas table) the monthly array power production; columns
                                  are the months, index the years
    device_lifetime_net_energy_pd = (dictionary of scalars) the total net energy for each
                                    device. keys are the device id
    device_annual_net_energy_pd = (dictionary of Pandas tables) the total net energy for each
                                  device. keys of the dictionary are the device id. Each pandas
                                  has index the years and column "Annual Energy"
    device_monthly_net_energy = (dictionary of Pandas tables) the monthly energy production per
                                device.  keys of the dictionary are the device id; columns are
                                the months, index the years.

    array_lifetime_net_ratio_pd = (scalar) ratio between the array_lifetime_net_energy_pd and
                                  array_lifetime_gross_energy_pd
    array_lifetime_lost_ratio_pd = (scalar) ratio between the array_lifetime_lost_energy_pd
                                   and array_lifetime_gross_energy_pd
    array_annual_net_ratio_pd = (Pandas table) ratio between the array_annual_net_energy_pd and
                                array_annual_gross_energy_pd
    array_annual_lost_ratio_pd = (Pandas table) ratio between the array_annual_lost_energy_pd
                                 and array_annual_gross_energy_pd
    array_monthly_net_ratio_pd = (Pandas table) ratio between the array_monthly_net_energy_pd
                                 and array_monthly_gross_energy_pd
    array_monthly_lost_ratio_pd = (Pandas table) ratio between the array_monthly_lost_energy_pd
                                  and array_monthly_gross_energy_pd

    device_lifetime_net_ratio_pd = (dictionary) ratio between the device_lifetime_net_energy_pd
                                   and device_lifetime_gross_energy_pd
    device_lifetime_lost_ratio_pd = (dictionary) ratio between the
                                    device_lifetime_lost_energy_pd
                                    and device_lifetime_gross_energy_pd
    device_annual_net_ratio_pd = (dictionary of Pandas tables) ratio between the
                                 device_annual_net_energy_pd
                                 and device_annual_gross_energy_pd
    device_annual_lost_ratio_pd = (dictionary of Pandas tables) ratio between the
                                  device_annual_lost_energy_pdand device_annual_gross_energy_pd
    device_monthly_net_ratio_pd = (dictionary of Pandas tables) ratio between the
                                  device_monthly_net_energy_pd and
                                  device_monthly_gross_energy_pd
    device_monthly_lost_ratio_pd = (dictionary of Pandas tables) ratio between the
                                  device_monthly_lost_energy_pd and
                                  device_monthly_gross_energy_pd

    """

    # pylint: disable=too-many-instance-attributes
    # They are 38 instead of 7, but we are aware of this, they will be reduced in another round.

    def __init__(
        self,
        monthly_resource_histogram,
        power_delivery_histogram,
        number_devices,
        project_life,
        downtime_hours_per_device=None,
    ):

        self.monthly_resource_histogram = monthly_resource_histogram
        self.power_delivery_histogram = power_delivery_histogram
        self.downtime_hours_per_device = downtime_hours_per_device
        self.number_devices = number_devices

        self.array_lifetime_gross_energy_pd = {}
        self.array_lifetime_net_energy_pd = {}
        self.array_lifetime_lost_energy_pd = {}
        self.array_lifetime_net_ratio_pd = {}
        self.array_lifetime_lost_ratio_pd = {}

        self.device_lifetime_gross_energy_pd = {}
        self.device_lifetime_net_energy_pd = {}
        self.device_lifetime_lost_energy_pd = {}
        self.device_lifetime_net_ratio_pd = {}
        self.device_lifetime_lost_ratio_pd = {}

        self.array_annual_gross_energy_pd = {}
        self.array_annual_net_energy_pd = {}
        self.array_annual_lost_energy_pd = {}
        self.array_annual_net_ratio_pd = {}
        self.array_annual_lost_ratio_pd = {}

        self.device_annual_gross_energy_pd = {}
        self.device_annual_net_energy_pd = {}
        self.device_annual_lost_energy_pd = {}
        self.device_annual_net_ratio_pd = {}
        self.device_annual_lost_ratio_pd = {}

        self.array_monthly_gross_energy_pd = {}
        self.array_monthly_net_energy_pd = {}
        self.array_monthly_lost_energy_pd = {}
        self.array_monthly_net_ratio_pd = {}
        self.array_monthly_lost_ratio_pd = {}

        self.device_monthly_gross_energy_pd = {}
        self.device_monthly_net_energy_pd = {}
        self.device_monthly_lost_energy_pd = {}
        self.device_monthly_net_ratio_pd = {}
        self.device_monthly_lost_ratio_pd = {}

        self.project_life = project_life
        self.number_days_month = {
            "January": 31.0,
            "February": 28.25,
            "March": 31.0,
            "April": 30.0,
            "May": 31.0,
            "June": 30.0,
            "July": 31.0,
            "August": 31.0,
            "September": 30.0,
            "October": 31.0,
            "November": 30.0,
            "December": 31.0,
        }

        self.years = list(range(1, self.project_life + 1))

        if self.downtime_hours_per_device is None:
            self.downtime_hours_per_device = {}

            for i in range(self.number_devices):
                self.downtime_hours_per_device[str(i + 1)] = pd.DataFrame(
                    0.0, index=self.years, columns=list(self.number_days_month.keys())
                )
                self.downtime_hours_per_device[str(i + 1)].index.name = "Years"

        self.number_bins = len(self.power_delivery_histogram["bins"])
        self.inputs = {}
        self.outputs = {}

    def gross_energy(self):
        """
        Method for the estimation of the gross energy
        """
        # the monthly gross energy per device is equal for each device

        monthly_gross_energy_device = {}

        for key in self.number_days_month:
            monthly_gross_energy_device[key] = sum(
                self.monthly_resource_histogram[key][i]
                * self.power_delivery_histogram["power"][i]
                * self.number_days_month[key]
                * 24.0
                / self.number_devices
                for i in range(self.number_bins)
            )

        for key1 in self.downtime_hours_per_device:
            aux_dict = {}
            for key2 in monthly_gross_energy_device:
                aux_dict[key2] = [
                    monthly_gross_energy_device[key2] for i in range(self.project_life)
                ]
            aux_dict["Years"] = self.years
            self.device_monthly_gross_energy_pd[key1] = pd.DataFrame.from_dict(aux_dict)
            self.device_monthly_gross_energy_pd[key1].set_index("Years", inplace=True)

            self.device_annual_gross_energy_pd[
                key1
            ] = self.device_monthly_gross_energy_pd[key1].sum(axis=1)
            self.device_annual_gross_energy_pd[key1].columns = ["Annual Energy"]
            self.device_lifetime_gross_energy_pd[
                key1
            ] = self.device_annual_gross_energy_pd[key1].sum(axis=0)

        self.array_monthly_gross_energy_pd = sum(
            self.device_monthly_gross_energy_pd[key]
            for key in self.device_monthly_gross_energy_pd
        )
        self.array_annual_gross_energy_pd = sum(
            self.device_annual_gross_energy_pd[key]
            for key in self.device_annual_gross_energy_pd
        )
        self.array_lifetime_gross_energy_pd = sum(
            self.device_lifetime_gross_energy_pd[key]
            for key in self.device_lifetime_gross_energy_pd
        )

        self.outputs["array_lifetime_gross_energy_pd"] = {
            "value": self.array_lifetime_gross_energy_pd,
            "Aggregation-Level": "array",
            "Label": "Array Lifetime Gross Energy",
            "Unit": "kWh",
            "Description": "Gross Energy produced by the array during all the lifetime",
            "Tag": "Gross Energy",
        }
        self.outputs["array_annual_gross_energy_pd"] = {
            "value": self.array_annual_gross_energy_pd,
            "Aggregation-Level": "array",
            "Label": "Array Annual Gross Energy",
            "Unit": "kWh",
            "Description": "Gross Energy produced by the array per year",
            "Tag": "Gross Energy",
        }
        self.outputs["array_monthly_gross_energy_pd"] = {
            "value": self.array_monthly_gross_energy_pd,
            "Aggregation-Level": "array",
            "Label": "Array Monthly Gross Energy",
            "Unit": "kWh",
            "Description": "Gross Energy produced by the array per month per year",
            "Tag": "Gross Energy",
        }
        self.outputs["device_lifetime_gross_energy_pd"] = {
            "value": self.device_lifetime_gross_energy_pd,
            "Aggregation-Level": "device",
            "Label": "Device Lifetime Gross Energy",
            "Unit": "kWh",
            "Description": (
                "Gross Energy produced by each device during all the lifetime"
            ),
            "Tag": "Gross Energy",
        }
        self.outputs["device_annual_gross_energy_pd"] = {
            "value": self.device_annual_gross_energy_pd,
            "Aggregation-Level": "device",
            "Label": "Device Annual Gross Energy",
            "Unit": "kWh",
            "Description": "Gross Energy produced by each device per year",
            "Tag": "Gross Energy",
        }
        self.outputs["device_monthly_gross_energy_pd"] = {
            "value": self.device_monthly_gross_energy_pd,
            "Aggregation-Level": "device",
            "Label": "Device Montlhy Gross Energy",
            "Unit": "kWh",
            "Description": "Gross Energy produced by each device per year per month",
            "Tag": "Gross Energy",
        }
        return (
            self.array_lifetime_gross_energy_pd,
            self.array_annual_gross_energy_pd,
            self.array_monthly_gross_energy_pd,
            self.device_lifetime_gross_energy_pd,
            self.device_annual_gross_energy_pd,
            self.device_monthly_gross_energy_pd,
        )

    def downtime_lost_energy(self):
        """
        Method for the estimation of the energy lost due to downtime
        """
        self.gross_energy()
        number_days = list(self.number_days_month.values())
        number_hours = [i * 24.0 for i in number_days]

        for key in self.downtime_hours_per_device:
            self.device_monthly_lost_energy_pd[key] = (
                self.device_monthly_gross_energy_pd[key]
                * self.downtime_hours_per_device[key]
                / (number_hours)
            )
            self.device_annual_lost_energy_pd[key] = self.device_monthly_lost_energy_pd[
                key
            ].sum(axis=1)
            self.device_annual_lost_energy_pd[key].columns = ["Annual Energy"]
            self.device_lifetime_lost_energy_pd[
                key
            ] = self.device_annual_lost_energy_pd[key].sum(axis=0)

        self.array_monthly_lost_energy_pd = sum(
            self.device_monthly_lost_energy_pd[key]
            for key in self.device_monthly_lost_energy_pd
        )
        self.array_annual_lost_energy_pd = sum(
            self.device_annual_lost_energy_pd[key]
            for key in self.device_annual_lost_energy_pd
        )
        self.array_lifetime_lost_energy_pd = sum(
            self.device_lifetime_lost_energy_pd[key]
            for key in self.device_lifetime_lost_energy_pd
        )
        self.outputs["array_lifetime_lost_energy_pd"] = {
            "value": self.array_lifetime_lost_energy_pd,
            "Aggregation-Level": "array",
            "Label": "Array Lifetime Lost Energy",
            "Unit": "kWh",
            "Description": "Lost Energy  by the array during all the lifetime",
            "Tag": "Lost Energy",
        }
        self.outputs["array_annual_lost_energy_pd"] = {
            "value": self.array_annual_lost_energy_pd,
            "Aggregation-Level": "array",
            "Label": "Array Annual Lost Energy",
            "Unit": "kWh",
            "Description": "Lost Energy by the array per year",
            "Tag": "Lost Energy",
        }
        self.outputs["array_monthly_lost_energy_pd"] = {
            "value": self.array_monthly_lost_energy_pd,
            "Aggregation-Level": "array",
            "Label": "Array Monthly Lost Energy",
            "Unit": "kWh",
            "Description": "Lost Energy  by the array per month per year",
            "Tag": "Lost Energy",
        }
        self.outputs["device_lifetime_lost_energy_pd"] = {
            "value": self.device_lifetime_lost_energy_pd,
            "Aggregation-Level": "device",
            "Label": "Device Lifetime Lost Energy",
            "Unit": "kWh",
            "Description": "Lost Energy  by each device during lifetime",
            "Tag": "Lost Energy",
        }
        self.outputs["device_annual_lost_energy_pd"] = {
            "value": self.device_annual_lost_energy_pd,
            "Aggregation-Level": "device",
            "Label": "Device Annual Lost Energy",
            "Unit": "kWh",
            "Description": "Lost Energy  by each device per year",
            "Tag": "Lost Energy",
        }
        self.outputs["device_monthly_lost_energy_pd"] = {
            "value": self.device_monthly_lost_energy_pd,
            "Aggregation-Level": "device",
            "Label": "Device Montlhy Lost Energy",
            "Unit": "kWh",
            "Description": "Lost Energy by each device per year per month",
            "Tag": "Lost Energy",
        }
        return (
            self.array_monthly_lost_energy_pd,
            self.array_annual_lost_energy_pd,
            self.array_lifetime_lost_energy_pd,
            self.device_monthly_lost_energy_pd,
            self.device_annual_lost_energy_pd,
            self.device_lifetime_lost_energy_pd,
        )

    def downtime_net_energy(self):
        """
        Method for the getting the net  energy
        """

        self.downtime_lost_energy()

        self.array_monthly_net_energy_pd = (
            self.array_monthly_gross_energy_pd - self.array_monthly_lost_energy_pd
        )
        self.array_annual_net_energy_pd = (
            self.array_annual_gross_energy_pd - self.array_annual_lost_energy_pd
        )
        self.array_lifetime_net_energy_pd = (
            self.array_lifetime_gross_energy_pd - self.array_lifetime_lost_energy_pd
        )

        self.array_monthly_net_ratio_pd = (
            self.array_monthly_net_energy_pd / self.array_monthly_gross_energy_pd
        )
        self.array_monthly_lost_ratio_pd = (
            self.array_monthly_lost_energy_pd / self.array_monthly_gross_energy_pd
        )
        self.array_annual_net_ratio_pd = (
            self.array_annual_net_energy_pd / self.array_annual_gross_energy_pd
        )
        self.array_annual_lost_ratio_pd = (
            self.array_annual_lost_energy_pd / self.array_annual_gross_energy_pd
        )
        self.array_lifetime_net_ratio_pd = (
            self.array_lifetime_net_energy_pd / self.array_lifetime_gross_energy_pd
        )
        self.array_lifetime_lost_ratio_pd = (
            self.array_lifetime_lost_energy_pd / self.array_lifetime_gross_energy_pd
        )

        for key in self.downtime_hours_per_device:
            self.device_monthly_net_energy_pd[key] = (
                self.device_monthly_gross_energy_pd[key]
                - self.device_monthly_lost_energy_pd[key]
            )
            self.device_annual_net_energy_pd[key] = (
                self.device_annual_gross_energy_pd[key]
                - self.device_annual_lost_energy_pd[key]
            )
            self.device_lifetime_net_energy_pd[key] = (
                self.device_lifetime_gross_energy_pd[key]
                - self.device_lifetime_lost_energy_pd[key]
            )

            self.device_monthly_net_ratio_pd[key] = (
                self.device_monthly_net_energy_pd[key]
                / self.device_monthly_gross_energy_pd[key]
            )
            self.device_monthly_lost_ratio_pd[key] = (
                self.device_monthly_lost_energy_pd[key]
                / self.device_monthly_gross_energy_pd[key]
            )
            self.device_annual_net_ratio_pd[key] = (
                self.device_annual_net_energy_pd[key]
                / self.device_annual_gross_energy_pd[key]
            )
            self.device_annual_lost_ratio_pd[key] = (
                self.device_annual_lost_energy_pd[key]
                / self.device_annual_gross_energy_pd[key]
            )
            self.device_lifetime_net_ratio_pd[key] = (
                self.device_lifetime_net_energy_pd[key]
                / self.device_lifetime_gross_energy_pd[key]
            )
            self.device_lifetime_lost_ratio_pd[key] = (
                self.device_lifetime_lost_energy_pd[key]
                / self.device_lifetime_gross_energy_pd[key]
            )
        self.outputs["array_lifetime_net_energy_pd"] = {
            "value": self.array_lifetime_net_energy_pd,
            "Aggregation-Level": "array",
            "Label": "Array Lifetime Net Energy",
            "Unit": "kWh",
            "Description": "Net Energy produced by the array during lifetime",
            "Tag": "Net Energy",
        }
        self.outputs["array_annual_net_energy_pd"] = {
            "value": self.array_annual_net_energy_pd,
            "Aggregation-Level": "array",
            "Label": "Array Annual net Energy",
            "Unit": "kWh",
            "Description": "Net Energy produced by the array per year",
            "Tag": "Net Energy",
        }
        self.outputs["array_monthly_net_energy_pd"] = {
            "value": self.array_monthly_net_energy_pd,
            "Aggregation-Level": "array",
            "Label": "Array Monthly Net Energy",
            "Unit": "kWh",
            "Description": "Net Energy produced by the array per month per year",
            "Tag": "Net Energy",
        }
        self.outputs["device_lifetime_net_energy_pd"] = {
            "value": self.device_lifetime_net_energy_pd,
            "Aggregation-Level": "device",
            "Label": "Device Lifetime Net Energy",
            "Unit": "kWh",
            "Description": "Net Energy produced by each device during lifetime",
            "Tag": "Net Energy",
        }
        self.outputs["device_annual_net_energy_pd"] = {
            "value": self.device_annual_net_energy_pd,
            "Aggregation-Level": "device",
            "Label": "Device Annual Net Energy",
            "Unit": "kWh",
            "Description": "Net Energy produced by each device per year",
            "Tag": "Net Energy",
        }
        self.outputs["device_monthly_net_energy_pd"] = {
            "value": self.device_monthly_net_energy_pd,
            "Aggregation-Level": "device",
            "Label": "Device Montlhy Net Energy",
            "Unit": "kWh",
            "Description": "Net Energy produced by each device per year per month",
            "Tag": "Net Energy",
        }

        self.outputs["array_lifetime_net_ratio_pd"] = {
            "value": self.array_lifetime_net_ratio_pd,
            "Aggregation-Level": "array",
            "Label": "Array Lifetime Net Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Net Energy/Gross Energy during lifetime",
            "Tag": "Net Energy",
        }
        self.outputs["array_annual_net_ratio_pd"] = {
            "value": self.array_annual_net_ratio_pd,
            "Aggregation-Level": "array",
            "Label": "Array Annual Net Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Net Energy/Gross Energy per year",
            "Tag": "Net Energy",
        }
        self.outputs["array_monthly_net_ratio_pd"] = {
            "value": self.array_monthly_net_ratio_pd,
            "Aggregation-Level": "array",
            "Label": "Array Monthly Net Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Net Energy/Gross Energy per month year",
            "Tag": "Net Energy",
        }
        self.outputs["device_lifetime_net_ratio_pd"] = {
            "value": self.device_lifetime_net_ratio_pd,
            "Aggregation-Level": "device",
            "Label": "Device Lifetime Net Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Net Energy/Gross Energy during lifetime",
            "Tag": "Net Energy",
        }
        self.outputs["device_annual_net_ratio_pd"] = {
            "value": self.device_annual_net_ratio_pd,
            "Aggregation-Level": "device",
            "Label": "Device Annual Net Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Net Energy/Gross Energy per device per year",
            "Tag": "Net Energy",
        }
        self.outputs["device_monthly_net_ratio_pd"] = {
            "value": self.device_monthly_net_ratio_pd,
            "Aggregation-Level": "device",
            "Label": "Device Montlhy Net Energy ratio",
            "Unit": "-",
            "Description": (
                "Ratio of the Net Energy/Gross Energy per device per year per month"
            ),
            "Tag": "Net Energy",
        }

        self.outputs["array_lifetime_lost_ratio_pd"] = {
            "value": self.array_lifetime_lost_ratio_pd,
            "Aggregation-Level": "array",
            "Label": "Array Lifetime Lost Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Lost Energy/Gross Energy during lifetime",
            "Tag": "Lost Energy",
        }
        self.outputs["array_annual_lost_ratio_pd"] = {
            "value": self.array_annual_lost_ratio_pd,
            "Aggregation-Level": "array",
            "Label": "Array Annual Lost Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Lost Energy/Gross Energy per year",
            "Tag": "Lost Energy",
        }
        self.outputs["array_monthly_lost_ratio_pd"] = {
            "value": self.array_monthly_lost_ratio_pd,
            "Aggregation-Level": "array",
            "Label": "Array Monthly Lost Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Lost Energy/Gross Energy per month year",
            "Tag": "Lost Energy",
        }
        self.outputs["device_lifetime_lost_ratio_pd"] = {
            "value": self.device_lifetime_lost_ratio_pd,
            "Aggregation-Level": "device",
            "Label": "Device Lifetime Lost Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Lost Energy/Gross Energy during lifetime",
            "Tag": "Lost Energy",
        }
        self.outputs["device_annual_lost_ratio_pd"] = {
            "value": self.device_annual_lost_ratio_pd,
            "Aggregation-Level": "device",
            "Label": "Device Annual Lost Energy ratio",
            "Unit": "-",
            "Description": "Ratio of the Lost Energy/Gross Energy per device per year",
            "Tag": "Lost Energy",
        }
        self.outputs["device_monthly_lost_ratio_pd"] = {
            "value": self.device_monthly_lost_ratio_pd,
            "Aggregation-Level": "device",
            "Label": "Device Montlhy lost Energy ratio",
            "Unit": "-",
            "Description": (
                "Ratio of the Lost Energy/Gross Energy per device per year per month"
            ),
            "Tag": "Lost Energy",
        }
        return (
            self.array_monthly_net_energy_pd,
            self.array_annual_net_energy_pd,
            self.array_lifetime_net_energy_pd,
            self.array_monthly_net_ratio_pd,
            self.array_monthly_lost_ratio_pd,
            self.array_annual_net_ratio_pd,
            self.array_annual_lost_ratio_pd,
            self.array_lifetime_net_ratio_pd,
            self.array_lifetime_lost_ratio_pd,
            self.device_monthly_net_energy_pd,
            self.device_annual_net_energy_pd,
            self.device_lifetime_net_energy_pd,
            self.device_monthly_net_ratio_pd,
            self.device_monthly_lost_ratio_pd,
            self.device_annual_net_ratio_pd,
            self.device_annual_lost_ratio_pd,
            self.device_lifetime_net_ratio_pd,
            self.device_lifetime_lost_ratio_pd,
        )

    def get_inputs(self):
        """
        Method for the getting the inputs to the module
        """
        self.inputs = {
            "monthly_resource_histogram": {
                "value": self.monthly_resource_histogram,
                "Label": "Monthly Resorce Occurrence",
                "Unit": "-",
                "Description": (
                    "Monthly EJPD (Empirical Joint Probability Density) of  Resource"
                ),
            },
            "power_delivery_histogram": {
                "value": self.power_delivery_histogram,
                "Label": "Power Delivery Histogram",
                "Unit": "kW",
                "Description": "Power Level per bin of the Occurrence ",
            },
            "downtime_hours_per_device": {
                "value": self.downtime_hours_per_device,
                "Label": "Downtime Hours per device",
                "Unit": "hours",
                "Description": (
                    "Downtime Hours per device, per month and per year of operation"
                ),
            },
            "number_devices": {
                "value": self.number_devices,
                "Label": "Number of Devices",
                "Unit": "-",
                "Description": "Number of Devices",
            },
            "project_life": {
                "value": self.project_life,
                "Label": "Project Life",
                "Unit": "years",
                "Description": "Project Life",
            },
        }
        return {
            "Monthly Resource Histogram": self.monthly_resource_histogram,
            "Delivered Power Histogram": self.power_delivery_histogram,
            "Downtime Hours per Month per Device": self.downtime_hours_per_device,
        }
