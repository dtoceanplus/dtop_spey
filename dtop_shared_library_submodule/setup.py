#!/usr/bin/env python

"Installation procedure for the given package"

from setuptools import setup, find_packages

setup(
    name="dtop_shared_library",
    version="0.0.1",
    packages=find_packages(),
    scripts=["bin/schema_from_document"],
    install_requires=["pandas", "numpy"],
)
