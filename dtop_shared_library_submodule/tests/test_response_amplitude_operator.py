from dtop_shared_library.response_amplitude_operator import *


def test_rao():
    nf = 50
    nd = 30
    ndof = 3
    period = np.linspace(2, 15, nf)
    omega = 2 * np.pi / period
    M = np.eye(ndof)
    Madd = np.array([np.eye(ndof)] * nf)
    Cpto = np.eye(ndof)
    Crad = np.array([np.eye(ndof)] * nf)
    Khyd = np.eye(ndof)
    Fex = np.ones((nf, nd, ndof)) + 1j * np.ones((nf, nd, ndof))
    Kfit = np.eye(ndof)
    Cfit = np.eye(ndof)

    rao = response_amplitude_operator(
        omega, M + Madd, Cpto + Crad + Cfit, Khyd + Kfit, Fex
    )
    assert rao.shape == (nf, nd, ndof)
