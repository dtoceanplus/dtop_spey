"""
Tests for Energy Production
"""
from dtop_shared_library import energy_production_base


def test_energy_production_inputs():
    """
    Test for the inputs
    """
    occurrence = [0.2, 0.2, 0.3, 0.1, 0.2]
    bins = [1.0, 2.0, 3.0, 4.0, 5.0]
    power = [30.0, 40.0, 20.0, 50.0, 10.0]
    monthly_resource_histogram = {
        "bins": bins,
        "January": occurrence,
        "February": occurrence,
        "March": occurrence,
        "April": occurrence,
        "May": occurrence,
        "June": occurrence,
        "July": occurrence,
        "August": occurrence,
        "September": occurrence,
        "October": occurrence,
        "November": occurrence,
        "December": occurrence,
    }

    power_delivery_histogram = {"bins": bins, "power": power}

    number_devices = 3
    project_life = 15

    energy_production = energy_production_base.EnergyProductionBase(
        monthly_resource_histogram,
        power_delivery_histogram,
        number_devices,
        project_life,
    )

    inputs = energy_production.get_inputs()

    assert inputs["Monthly Resource Histogram"]["bins"][0] == 1.0


def test_net_downtime_lifetime():
    """
    Test for the net energy production
    """
    occurrence = [0.2, 0.2, 0.3, 0.1, 0.2]
    bins = [1.0, 2.0, 3.0, 4.0, 5.0]
    power = [30.0, 40.0, 20.0, 50.0, 10.0]
    monthly_resource_histogram = {
        "bins": bins,
        "January": occurrence,
        "February": occurrence,
        "March": occurrence,
        "April": occurrence,
        "May": occurrence,
        "June": occurrence,
        "July": occurrence,
        "August": occurrence,
        "September": occurrence,
        "October": occurrence,
        "November": occurrence,
        "December": occurrence,
    }

    power_delivery_histogram = {"bins": bins, "power": power}

    number_devices = 3
    project_life = 15

    energy_production = energy_production_base.EnergyProductionBase(
        monthly_resource_histogram,
        power_delivery_histogram,
        number_devices,
        project_life,
    )

    energy_production.downtime_net_energy()
    assert energy_production.array_lifetime_net_ratio_pd == 1.0


def test_net_downtime_lifetime_gross():
    """
    Test for the gross energy production
    """
    occurrence = [0.2, 0.2, 0.3, 0.1, 0.2]
    bins = [1.0, 2.0, 3.0, 4.0, 5.0]
    power = [30.0, 40.0, 20.0, 50.0, 10.0]
    monthly_resource_histogram = {
        "bins": bins,
        "January": occurrence,
        "February": occurrence,
        "March": occurrence,
        "April": occurrence,
        "May": occurrence,
        "June": occurrence,
        "July": occurrence,
        "August": occurrence,
        "September": occurrence,
        "October": occurrence,
        "November": occurrence,
        "December": occurrence,
    }

    power_delivery_histogram = {"bins": bins, "power": power}

    number_devices = 3
    project_life = 15

    energy_production = energy_production_base.EnergyProductionBase(
        monthly_resource_histogram,
        power_delivery_histogram,
        number_devices,
        project_life,
    )

    energy_production.downtime_net_energy()
    assert energy_production.array_lifetime_gross_energy_pd == 3550230.0
