import jsonpath_rw

from dtop_shared_library.dict_processing import filter_dict, merge_dicts, merge_lists


def test_jsonpath_references():
    # For filter_dict() to work correctly, we have to be sure that jsonpath-rw's implementation
    # produces references to (not copies of!) dictionaties and lists in .value

    data = {"a": {"b": {"c": 100}}}
    assert jsonpath_rw.parse("a.b").find(data)[0].value is data["a"]["b"]

    data = {"a": {"b": [100, 200]}}
    assert jsonpath_rw.parse("a.b").find(data)[0].value is data["a"]["b"]


def test_filter_dict():
    data = {
        "a": {"b": {"c": 100, "d": 200}, "e": 300},
        "f": 400,
    }
    assert filter_dict(data, ("a.b.c",)) == {"a": {"b": {"c": 100}}}
    assert filter_dict(data, ("a.b.c", "a.e")) == {"a": {"b": {"c": 100}, "e": 300}}
    assert filter_dict(data, ("a.b.c", "a.b.d")) == {"a": {"b": {"c": 100, "d": 200}}}
    assert filter_dict(data, ("a.b",)) == {"a": {"b": {"c": 100, "d": 200}}}
    assert filter_dict(data, ("a",)) == {"a": {"b": {"c": 100, "d": 200}, "e": 300}}

    data = {
        "a": ["b", "c", {"d": {"e": [100, 200, 300]}, "f": 400}],
    }
    assert filter_dict(data, ("a[0]",)) == {"a": ["b"]}
    assert filter_dict(data, ("a[0:2]",)) == {"a": ["b", "c"]}
    assert filter_dict(data, ("a[2].d.e",)) == {"a": [{"d": {"e": [100, 200, 300]}}]}
    assert filter_dict(data, ("a[2].f",)) == {"a": [{"f": 400}]}


def test_merge_dicts():
    dict1 = {
        "a": 1,
        "dict": {"subdict": {"c": 3, "d": 4}},
        "list": [{"id": 1, "x": 100}, {"id": 2, "x": 200}],
    }
    dict2 = {
        "dict": {"b": 2, "subdict": {"c": 33}},
        "list": [{"id": 2, "y": 200}, {"id": 1, "y": 100}],
    }
    assert merge_dicts(dict1, dict2) == {
        "a": 1,
        "dict": {"b": 2, "subdict": {"c": 33, "d": 4}},
        "list": [{"id": 1, "x": 100, "y": 100}, {"id": 2, "x": 200, "y": 200}],
    }
    assert merge_dicts(dict2, dict1) == {
        "a": 1,
        "dict": {"b": 2, "subdict": {"c": 3, "d": 4}},
        "list": [{"id": 2, "y": 200, "x": 200}, {"id": 1, "y": 100, "x": 100}],
    }

    dict3 = {"a": 11, "dict": {"bb": 22, "subdict": {"c": 333}}}
    assert merge_dicts(dict1, dict2, dict3) == {
        "a": 11,
        "dict": {"b": 2, "bb": 22, "subdict": {"c": 333, "d": 4}},
        "list": [{"id": 1, "x": 100, "y": 100}, {"id": 2, "x": 200, "y": 200}],
    }


def test_merge_lists():
    list1 = [
        {"id": 100, "a": 1, "b": {"ba": 21, "bb": 22}},
        {"id": 200, "x": 1},
    ]
    list2 = [
        {"id": 300, "x": 1},
        {"id": 200, "x": 2, "y": 2},
        {"id": 100, "b": {"bc": 23}, "c": 3},
    ]
    assert merge_lists(list1, list2) == [
        {"id": 100, "a": 1, "b": {"ba": 21, "bb": 22, "bc": 23}, "c": 3},
        {"id": 200, "x": 2, "y": 2},
        {"id": 300, "x": 1},
    ]
    assert merge_lists(list2, list1) == [
        {"id": 300, "x": 1},
        {"id": 200, "y": 2, "x": 1},
        {"id": 100, "b": {"bc": 23, "ba": 21, "bb": 22}, "c": 3, "a": 1},
    ]
