import sys
from importlib.machinery import SourceFileLoader
from importlib.util import module_from_spec, spec_from_loader
from io import StringIO
from os.path import abspath, dirname, normpath
from tempfile import NamedTemporaryFile
from typing import Dict, Iterable

import jsonschema
from pytest import raises

from dtop_shared_library.pandas_tables import schema_from_document


def _assert_correct_schema_generated(good: Dict, bad_examples: Iterable[Dict] = ()):
    schema = schema_from_document(good)
    jsonschema.validate(schema, good)


def test_example1():
    _assert_correct_schema_generated(
        {
            "Qnf": [135, 1685],
            "Unit_cost": [85.5, 302.76],
        }
    )


def test_example2():
    _assert_correct_schema_generated(
        {
            "Module": ["SK", "SK", "SK", "SK", "SK", "SK"],
            "Catalogue ID": [
                "catalogue_id",
                "catalogue_id",
                "catalogue_id",
                "catalogue_id",
                "catalogue_id",
                "catalogue_id",
            ],
            "Name": ["mooring line", "chain", "anchor", "anchor", "buoy", "shackle"],
            "Qnt": [135, 1685, 1, 2, 10, 100],
            "UOM": ["n.d.", "n.d.", "n.d.", "n.d.", "n.d.", "n.d."],
            "Unit_cost": [85.5, 302.76, 8918, 57624, 6000, 1023],
            "Total_cost": [11542.5, 510150.6, 8918, 115248, 60000, 102300],
        }
    )


def test_validate_string():
    _assert_correct_schema_generated(
        {
            "x": ["abc123@!&*", "", "90"],
        }
    )


def test_validate_number():
    _assert_correct_schema_generated(
        {
            "x": [-1.1, -1, 0, 1, 1.1],
        }
    )


def test_validate_integer():
    _assert_correct_schema_generated(
        {
            "x": [-1, 0, 1],
        }
    )


def test_validate_boolean():
    _assert_correct_schema_generated(
        {
            "x": [True, False],
        }
    )


# def test_different_length_of_columns():
#    with raises(ValueError, match="arrays must all be same length"):
#        _assert_correct_schema_generated(
#            {
#                "x": [1, 2, 3],
#                "y": [1, 2],
#            }
#        )


def test_executable():
    with NamedTemporaryFile("w+") as file:
        file.write('{"Qnf": [135, 1685], "Unit_cost": [85.5, 302.76]}')
        file.seek(0)

        try:
            sys.argv = [sys.argv[0], file.name]
            sys.stdout = StringIO()
            sys.stderr = StringIO()

            executable_path = normpath(
                abspath(dirname(__file__) + "/../bin/schema_from_document")
            )
            spec = spec_from_loader(
                "__main__", SourceFileLoader("__main__", executable_path)
            )
            module = module_from_spec(spec)
            spec.loader.exec_module(module)

            # Check YAML text, including order of attributes
            assert (
                sys.stdout.getvalue().strip()
                == """
type: object
additionalProperties: false
properties:
  index:
    type: array
    items:
      type: integer
  Qnf:
    type: array
    items:
      type: integer
  Unit_cost:
    type: array
    items:
      type: number
""".strip()
            )

        finally:
            sys.stdout = sys.__stdout__
            sys.stderr = sys.__stderr__
