# dtop-shared-library

Introduced to share a common functionality across different modules.

## Installation

### Prerequisites (development)

1. [conda], as an universal package manager
1. [docker], as a powerfull virtualization system
1. [make] and [bash] as a sofisticated automatization tools

### Cloning git repository

```bash
git clone git@gitlab.com:opencascade/dtocean/dtop-shared-library.git
cd dtop-shared-library
```

### Generating source distribution

```bash
python setup.py install
```

## Usage

Can be seen via tests, for example

```python
from dtop_shared_library.dummy import dummy_foo

def test_foo():
    assert dummy_foo()
```

See also [DataFrames and JSON Schemas](docs/json_schemas.md) to read about features related to JSON Schemas.

### Production Usage

#### 1. Installation

```bash
pip install git+ssh://git@gitlab.com/opencascade/dtocean/dtop-shared-library.git
```

#### 2. Test Usage

```bash
python -c "import dtop_shared_library; print('OK')"
```

#### 3. Uninstall

```bash
pip uninstall dtop-shared-library
```

## Development

### Enter a virtual environment

```bash
make dev
```

### Install development prerequisites

```bash
make x-pip-env
```

### Perform necessary all checks

```bash
make x-all
```

### Enter the development environment (if necessary)

```bash
make x-pip-run
```

For more available actions see content of the [Makefile](Makefile)

## Adding modules/functions to be shared

In order to add new modules/functions to be shared, follow the steps:

1. Create a new branch
1. Include the functions/modules in the `dtop-shared-library` folder.
   - The code should compliant with both [pre-commit](https://pre-commit.com) and [PyLint](https://www.pylint.org)/[PEP8](https://www.python.org/dev/peps/pep-0008/) rules/standards
1. Include a set of unit tests in the folder to ensure the full code coverage - `100%`
1. Check if the [pipeline](https://gitlab.com/opencascade/dtocean/dtop-shared-library/pipelines) `pass`
   - If not, advance the code consequently
   - If yes, send a merge request to merge in the `master` branch.

## Including the dtop-shared-library in your module

1. Import the library in your module as submoldue as in point ## Cloning git repository, naming it with a standard name (dtop-shared-library-submodule)
2. Install it, following the section ## Generating source distribution
3. Include the dtop-shared-library in the setup.py file of your module, in the install-requires section
4. Use the dtop-shared-library as required within your module
5. When running the unit tests of your module, use the command python -m pytest test, i.e. only the tests in your your test folder; if yo uallow to run the tests in the submodule, maybe they can break it.
6. No need to include in the coverage the tests of the submodule, as they have already been testing when populating the dtop-shared-library
