REPO_NAME = ci
REGISTRY_PORT = 5500
CI_REGISTRY_IMAGE ?= localhost:$(REGISTRY_PORT)/$(REPO_NAME)
MODULE_NICKNAME = $(shell make module-nickname)


all: registry-start \
	dind-push dind-pull \
	test-push test-run


include inc.makefile


clean: registry-remove
	export CI_REGISTRY_IMAGE=$(CI_REGISTRY_IMAGE) && \
	docker rmi --force $(shell docker images --filter "reference=${CI_REGISTRY_IMAGE}*" --quiet) | true
	$(MAKE) $(@)

registry-start: registry-remove
	docker run --detach --publish $(REGISTRY_PORT):5000 --restart always --name registry registry:2

registry-remove:
	docker container stop registry | true
	docker container rm --volumes registry | true

login:
	@echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} ${CI_REGISTRY} --password-stdin


dind-image:
	$(call image_names,$(@))
	docker build --tag ${LOCAL_NAME} \
	--file dind.dockerfile .

dind-push: dind-image
	$(call image_push,$(@))

dind-pull:
	$(call image_pull,$(@))


test-image:
	$(MAKE) $(@)

test-push: test-image
	$(call image_push,$(@))

test-run:
	$(call image_pull,$(@))
	$(MAKE) $(@)
