FROM spey-deploy-docker

# COPY testing.makefile .

RUN apt-get -y update
RUN apt-get -y install gcc
RUN pip install pytest==6.2.4 pytest-cov==2.12.1 pact-python py==1.10.0
