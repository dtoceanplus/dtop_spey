FROM python:3.8.14

COPY requirements.txt .
COPY setup.py .
COPY src/dtop_spey/ ./src/dtop_spey/
COPY ./dtop_shared_library_submodule/setup.py ./dtop_shared_library_submodule/setup.py
COPY ./dtop_shared_library_submodule/dtop_shared_library/ ./dtop_shared_library_submodule/dtop_shared_library/
COPY ./dtop_shared_library_submodule/bin/ ./dtop_shared_library_submodule/bin/

RUN apt-get update && \
    apt-get install --yes --no-install-recommends gcc libc6-dev && \
    pip install --requirement requirements.txt && \
    (cd ./dtop_shared_library_submodule && pip install --editable .) && \
    pip install --editable . && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get purge --auto-remove --yes gcc libc6-dev

ENV FLASK_APP dtop_spey.service
ENV DATABASE_URL=sqlite:///../databases/spey.db

EXPOSE 5000

RUN flask init-db

CMD flask run --host 0.0.0.0
