# Based on: https://medium.com/@chadlagore/conda-environments-with-docker-82cdc9d25754

FROM continuumio/miniconda3

WORKDIR /app

# Create environment.yml: conda env export > environment.yml'

# RUN conda create -n dtop_stagegate python=3.6
COPY dependency.yml .
RUN conda env create -f dependency.yml
# RUN conda update -n base -c defaults conda  # TODO: Had to comment this line out; was breaking docker-compose
# TODO: see issue here https://github.com/conda/conda/issues/9337

# npm for the front end need to be run as a service on docker-compose
# RUN apt-get --assume-yes install npm

# RUN echo "conda activate dtop_spey" >> ~/.bashrc
# RUN sed -i 's/conda activate base/conda activate dtop_spey/' ~/.bashrc
RUN echo "source activate dtop_stagegate" >> ~/.bashrc
ENV PATH /opt/conda/envs/dtop_stagegate/bin:$PATH

COPY  . .
RUN pip install -e .

ENV FLASK_APP dtop_stagegate.service
EXPOSE 5000

CMD python3 -m flask run
