import sys

import requests

body_project = {
    #    "speyId": 444,
    "title": "Vincenzo",
    # "date": "2020-02-23",
    # "status": "Inputs to be collected",
    "description": "SPEY Test 2",
}

body = {
    "scId": "User-Defined",
    "mcId": "User-Defined",
    "etId": "User-Defined",
    "edId": "User-Defined",
    "ecId": "User-Defined",
    "lmoId": "User-Defined",
    "device_characteristics_general": {
        "floating": True,
        "connector_type": "Dry",
        "constant_power_factor": 0.32,
        "machine_cost": 1000000,
        "max_installation_water_depth": 100,
        "min_installation_water_depth": 30,
        "min_interdistance_x": 120,
        "min_interdistance_y": 20,
        "preferred_fundation_type": "Pile",
        "rated_capacity": 1000000,
        "rated_voltage": 1000,
        "materials": [
            {"material_name": "undefined", "material_quantity": 760839.76},
            {"material_name": "concrete", "material_quantity": 882211.2033092708},
        ],
    },
    "device_characteristics_machine": {
        "complexity": 1,
        "date": "27-12-2019",
        "desc": "Simple project to test potential of Wave Energy Converter in the North Sea",
        "dimensions": 1,
        "general": 1,
        "id": 2,
        "model": 1,
        "status": 20,
        "tags": "wave",
        "title": "Test array",
        "type": "TEC",
    },
    "device_characteristics_dimensions": {
        "characteristic_dimension": 8,
        "draft": 3,
        "height": 10,
        "width": 10,
        "length": 70,
        "submerged_volume": 700,
        "wet_area": 1001,
        "wet_frontal_area": 10,
        "dry_frontal_area": 100,
        "beam_wet_area": 10,
        "mass": 700000,
        "footprint_radius": 50,
    },
    "sc_farm": {
        "name": ["aaa", ["bbb"]],
        "latitude": [0.0],
        "longitude": [0.0],
        "bins": [1, 2, 3],
        "surface": 10.0,
        "description": "bbb",
    },
    "wave_EJPD": {
        "id": [[1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1]],
        "pdf": [[1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1]],
    },
    "current_scenarii": {
        "id": {
            "January": [1, 2, 3],
            "February": [1, 2, 3],
            "March": [1, 2, 3],
            "April": [1, 2, 3],
            "May": [1, 2, 3],
            "June": [1, 2, 3],
            "July": [1, 2, 3],
            "August": [1, 2, 3],
            "September": [1, 2, 3],
            "October": [1, 2, 3],
            "November": [1, 2, 3],
            "December": [1, 2, 3],
        },
        "p": {
            "January": [0.5, 0.2, 0.3],
            "February": [0.5, 0.2, 0.3],
            "March": [0.5, 0.2, 0.3],
            "April": [0.5, 0.2, 0.3],
            "May": [0.5, 0.2, 0.3],
            "June": [0.5, 0.2, 0.3],
            "July": [0.5, 0.2, 0.3],
            "August": [0.5, 0.2, 0.3],
            "September": [0.5, 0.2, 0.3],
            "October": [0.5, 0.2, 0.3],
            "November": [0.5, 0.2, 0.3],
            "December": [0.5, 0.2, 0.3],
        },
        "name": "aaa",
        "description": "bbb",
    },
    "wave_flux": {
        "latitude": [0.0],
        "longitude": [0.0],
        "mean": 5.0,
        "min": [5.0],
        "max": [5.0],
        "median": [5.0],
        "std": [5.0],
        "name": "aaa",
        "description": "bbb",
    },
    "current_flux": {
        "latitude": [0.0],
        "longitude": [0.0],
        "mean": 5.0,
        "min": [5.0],
        "max": [5.0],
        "median": [5.0],
        "std": [5.0],
        "name": "aaa",
        "description": "bbb",
    },
    "ec_devices": [
        {
            "easting": 0,
            "northing": 0,
            "q_factor": 0.98,
            "aep": 100001,
            "captured_power": 100002,
            "captured_power_per_condition": {
                "siteConditionID": [0, 1, 2],
                "capturedPower": [10001, 10001, 10001],
            },
        },
        {
            "easting": 1000,
            "northing": 0,
            "q_factor": 0.98,
            "aep": 100001,
            "captured_power": 100002,
            "captured_power_per_condition": {
                "siteConditionID": [0, 1, 2],
                "capturedPower": [10001, 10001, 10001],
            },
        },
    ],
    "ec_farm": {
        "layout": {"deviceID": [0, 1], "easting": [0, 10], "northing": [0, 10]},
        "number_devices": 2,
        "q_factor": 0.98,
        "aep": 100001,
        "captured_power": 100002,
        "captured_power_per_condition": {
            "siteConditionID": [0, 1, 2, 3],
            "capturedPower": [10001, 10001, 10001, 10001],
        },
        "resource_reduction": 0.97,
    },
    "et_results": [
        {
            "Dev_Captured_Power": {
                "description": "Characteristics of the capture system",
                "label": "Cpto / sigma_v",
                "unit": "-",
                "value": {
                    "C_pto": [[200], [200], [200]],
                    "sigma_v": [[25.0], [25.0], [25.0]],
                },
            },
            "Dev_Control_strat": {
                "description": "Control Strategy",
                "label": "Control Strategy",
                "unit": "-",
                "value": "Passive",
            },
            "Dev_E_Capt": {
                "description": "Annual Captured Energy",
                "label": "Annual Captured Energy",
                "unit": "kWh",
                "value": 3285000.0,
            },
            "Dev_E_Elect": {
                "description": "Annual Electrical Energy",
                "label": "Annual Electrical Energy",
                "unit": "kWh",
                "value": 63932.006120238046,
            },
            "Dev_E_Grid": {
                "description": "Annual Energy delivered at grid",
                "label": "Annual Grid Energy",
                "unit": "kWh",
                "value": 61681.67900997283,
            },
            "Dev_E_Mech": {
                "description": "Annual Captured Energy",
                "label": "Annual Mechanical Energy",
                "unit": "kWh",
                "value": 1466713.2930960355,
            },
            "Dev_Elect_type": {
                "description": "Type of Electrical Transformation",
                "label": "Electrical Transformation",
                "unit": "-",
                "value": "Elect_Simplified",
            },
            "Dev_Env_Conditions": {
                "description": "Environmental Conditions",
                "label": "Environmental Conditions",
                "unit": "-",
                "value": {"Hs": [1], "Occ": [1], "Tz": [1], "id": [1]},
            },
            "Dev_Grid_type": {
                "description": "Type of Grid Conditioning Transformation",
                "label": "Grid Conditioning Transformation",
                "unit": "-",
                "value": "Grid_Simplified",
            },
            "Dev_Mech_type": {
                "description": "Type of Mechanical Transformation",
                "label": "Mechanical Transformation",
                "unit": "-",
                "value": "Mech_Simplified",
            },
            "Dev_PTO_cost": {
                "description": "Cost of the PTO",
                "label": "PTO Cost",
                "unit": "Euros",
                "value": 810000.0,
            },
            "Dev_PTO_mass": {
                "description": "Mass of the PTO",
                "label": "PTO Mass",
                "unit": "kg",
                "value": 213000.0,
            },
            "Dev_P_Capt": {
                "description": "Captured power per sea state",
                "label": "Captured Power per sea state",
                "unit": "kW",
                "value": {"Power": [375.0], "id": [1]},
            },
            "Dev_P_Elect": {
                "description": "Electrical power per sea state",
                "label": "Electrical Power per sea state",
                "unit": "kW",
                "value": {"Power": [7.2981742146390465], "id": [1]},
            },
            "Dev_P_Mech": {
                "description": "Mechanical power per sea state",
                "label": "Mechanical Power per sea state",
                "unit": "kW",
                "value": {"Power": [167.43302432603144], "id": [1]},
            },
            "Dev_Pa_Grid": {
                "description": "Active power per sea state",
                "label": "Active Power per sea state",
                "unit": "kW",
                "value": {"Power": [7.0412875582160765], "id": [1]},
            },
            "Dev_Pr_Grid": {
                "description": "Reactive power per sea state",
                "label": "Reactive power per sea state",
                "unit": "kW",
                "value": {"Power": [0.0], "id": [1]},
            },
            "Dev_cut_in_out": {
                "description": "Cut-in / Cut-off",
                "label": "Cut-in / Cut-off",
                "unit": "rad/s",
                "value": [0.5, 5],
            },
            "Dev_dof_PTO": {
                "description": "Active degrees of freedom for each PTO",
                "label": "PTO dofs",
                "unit": "-",
                "value": 3,
            },
            "Dev_elect_mass": {
                "description": "Mass of the electrical subsystem",
                "label": "Electrical subsystem Mass",
                "unit": "kg",
                "value": 180000.0,
            },
            "Dev_grid_mass": {
                "description": "Mass of the grid conditioning subsystem",
                "label": "Grid conditioning Mass",
                "unit": "kg",
                "value": 3000.0,
            },
            "Dev_mech_mass": {
                "description": "Mass of the mechanical subsystem",
                "label": "MEchanical subsystem Mass",
                "unit": "kg",
                "value": 30000.0,
            },
            "Dev_par_PTOs": {
                "description": "Number of PTOs per device",
                "label": "PTOs per device",
                "unit": "-",
                "value": 1,
            },
            "Dev_rated_power": {
                "description": "Rated power of the device",
                "label": "Rated Power",
                "unit": "kW",
                "value": 300.0,
            },
            "Dev_shutdown_flag": {
                "description": "Shutdown Flag",
                "label": "Shutdown Flag",
                "unit": "-",
                "value": 1.0,
            },
            "Dev_tech": {
                "description": "Wave or Tidal technology",
                "label": "Technology",
                "unit": "-",
                "value": "Wave",
            },
            "id": {
                "description": "Identifier of the device",
                "label": "Device ID",
                "unit": "-",
                "value": "0",
            },
        },
        {
            "Dev_Captured_Power": {
                "description": "Characteristics of the capture system",
                "label": "Cpto / sigma_v",
                "unit": "-",
                "value": {
                    "C_pto": [[200], [200], [200]],
                    "sigma_v": [[25.0], [25.0], [25.0]],
                },
            },
            "Dev_Control_strat": {
                "description": "Control Strategy",
                "label": "Control Strategy",
                "unit": "-",
                "value": "Passive",
            },
            "Dev_E_Capt": {
                "description": "Annual Captured Energy",
                "label": "Annual Captured Energy",
                "unit": "kWh",
                "value": 3285000.0,
            },
            "Dev_E_Elect": {
                "description": "Annual Electrical Energy",
                "label": "Annual Electrical Energy",
                "unit": "kWh",
                "value": 63932.006120238046,
            },
            "Dev_E_Grid": {
                "description": "Annual Energy delivered at grid",
                "label": "Annual Grid Energy",
                "unit": "kWh",
                "value": 61681.67900997283,
            },
            "Dev_E_Mech": {
                "description": "Annual Captured Energy",
                "label": "Annual Mechanical Energy",
                "unit": "kWh",
                "value": 1466713.2930960355,
            },
            "Dev_Elect_type": {
                "description": "Type of Electrical Transformation",
                "label": "Electrical Transformation",
                "unit": "-",
                "value": "Elect_Simplified",
            },
            "Dev_Env_Conditions": {
                "description": "Environmental Conditions",
                "label": "Environmental Conditions",
                "unit": "-",
                "value": {"Hs": [1], "Occ": [1], "Tz": [1], "id": [1]},
            },
            "Dev_Grid_type": {
                "description": "Type of Grid Conditioning Transformation",
                "label": "Grid Conditioning Transformation",
                "unit": "-",
                "value": "Grid_Simplified",
            },
            "Dev_Mech_type": {
                "description": "Type of Mechanical Transformation",
                "label": "Mechanical Transformation",
                "unit": "-",
                "value": "Mech_Simplified",
            },
            "Dev_PTO_cost": {
                "description": "Cost of the PTO",
                "label": "PTO Cost",
                "unit": "Euros",
                "value": 810000.0,
            },
            "Dev_PTO_mass": {
                "description": "Mass of the PTO",
                "label": "PTO Mass",
                "unit": "kg",
                "value": 213000.0,
            },
            "Dev_P_Capt": {
                "description": "Captured power per sea state",
                "label": "Captured Power per sea state",
                "unit": "kW",
                "value": {"Power": [375.0], "id": [1]},
            },
            "Dev_P_Elect": {
                "description": "Electrical power per sea state",
                "label": "Electrical Power per sea state",
                "unit": "kW",
                "value": {"Power": [7.2981742146390465], "id": [1]},
            },
            "Dev_P_Mech": {
                "description": "Mechanical power per sea state",
                "label": "Mechanical Power per sea state",
                "unit": "kW",
                "value": {"Power": [167.43302432603144], "id": [1]},
            },
            "Dev_Pa_Grid": {
                "description": "Active power per sea state",
                "label": "Active Power per sea state",
                "unit": "kW",
                "value": {"Power": [7.0412875582160765], "id": [1]},
            },
            "Dev_Pr_Grid": {
                "description": "Reactive power per sea state",
                "label": "Reactive power per sea state",
                "unit": "kW",
                "value": {"Power": [0.0], "id": [1]},
            },
            "Dev_cut_in_out": {
                "description": "Cut-in / Cut-off",
                "label": "Cut-in / Cut-off",
                "unit": "rad/s",
                "value": [0.5, 5],
            },
            "Dev_dof_PTO": {
                "description": "Active degrees of freedom for each PTO",
                "label": "PTO dofs",
                "unit": "-",
                "value": 3,
            },
            "Dev_elect_mass": {
                "description": "Mass of the electrical subsystem",
                "label": "Electrical subsystem Mass",
                "unit": "kg",
                "value": 180000.0,
            },
            "Dev_grid_mass": {
                "description": "Mass of the grid conditioning subsystem",
                "label": "Grid conditioning Mass",
                "unit": "kg",
                "value": 3000.0,
            },
            "Dev_mech_mass": {
                "description": "Mass of the mechanical subsystem",
                "label": "MEchanical subsystem Mass",
                "unit": "kg",
                "value": 30000.0,
            },
            "Dev_par_PTOs": {
                "description": "Number of PTOs per device",
                "label": "PTOs per device",
                "unit": "-",
                "value": 1,
            },
            "Dev_rated_power": {
                "description": "Rated power of the device",
                "label": "Rated Power",
                "unit": "kW",
                "value": 300.0,
            },
            "Dev_shutdown_flag": {
                "description": "Shutdown Flag",
                "label": "Shutdown Flag",
                "unit": "-",
                "value": 1.0,
            },
            "Dev_tech": {
                "description": "Wave or Tidal technology",
                "label": "Technology",
                "unit": "-",
                "value": "Wave",
            },
            "id": {
                "description": "Identifier of the device",
                "label": "Device ID",
                "unit": "-",
                "value": "1",
            },
        },
    ],
    "et_results_array": {
        "Array_E_Capt": {
            "description": "Total Annual Captured Energy",
            "label": "Captured Energy",
            "unit": "Wh",
            "value": 6570000.0,
        },
        "Array_E_Elect": {
            "description": "Total Annual Electrical Energy",
            "label": "Electrical Energy",
            "unit": "Wh",
            "value": 127864.01224047609,
        },
        "Array_E_Grid": {
            "description": "Total Annual Grid Conditioned Active Energy",
            "label": "Grid conditioned Energy",
            "unit": "Wh",
            "value": 123363.35801994566,
        },
        "Array_E_Mech": {
            "description": "Total Annual Mechanical Energy",
            "label": "Mechanical Energy",
            "unit": "Wh",
            "value": 2933426.586192071,
        },
        "Array_Env_Conditions": {
            "description": "Environmental Conditions",
            "label": "Environmental Conditions",
            "unit": "-",
            "value": {"Hs": [1], "Occ": [1], "Tz": [1], "id": [1]},
        },
        "Array_P_Capt": {
            "description": "Captured Power per sea state",
            "label": "Captured Power",
            "unit": "W",
            "value": {"Power": [750.0], "id": [1]},
        },
        "Array_P_Elect": {
            "description": "Electrical Power per sea state",
            "label": "Electrical Power",
            "unit": "W",
            "value": {"Power": [14.596348429278093], "id": [1]},
        },
        "Array_P_Mech": {
            "description": "Mechanical Power per sea state",
            "label": "Mechanical Power",
            "unit": "W",
            "value": {"Power": [334.8660486520629], "id": [1]},
        },
        "Array_Pa_Grid": {
            "description": "Grid Active Power per sea state",
            "label": "Grid Active Power",
            "unit": "W",
            "value": {"Power": [14.082575116432153], "id": [1]},
        },
        "Array_Pr_Grid": {
            "description": "Grid Reactive Power per sea state",
            "label": "Grid Reactive Power",
            "unit": "W",
            "value": {"Power": [0.0], "id": [1]},
        },
        "Array_Tech": {
            "description": "Wave or Tidal Energy Array",
            "label": "Technology",
            "unit": "-",
            "value": "Wave",
        },
        "Array_cost": {
            "description": "Total cost of the components of the Energy Transformation subsystem",
            "label": "Cost of the components",
            "unit": "Euros",
            "value": 1620000.0,
        },
        "Array_mass": {
            "description": "Total weight of the components of the Energy Transformation subsystem",
            "label": "Weight of the components",
            "unit": "kg",
            "value": 426000.0,
        },
        "Array_number_devices": {
            "description": "Total number of devices in the array",
            "label": "Number of devices",
            "unit": "-",
            "value": 2,
        },
        "BoM": {
            "description": "Bill of materials of the Energy Trasformation subsystem",
            "label": "Bill of materials",
            "unit": "-",
            "value": {
                "id": ["Cat_ID_0", "Cat_ID_1", "Cat_ID_2"],
                "name": ["Elect_Simplified", "Grid_Simplified", "Mech_Simplified"],
                "qnt": [6, 6, 6],
                "total_cost": [60000.0, 60000.0, 1500000.0],
                "unit_cost": [10000.0, 10000.0, 250000.0],
                "uom": ["NA", "NA", "NA"],
            },
        },
        "Hierarchy": {
            "description": "Hierarchy of the Energy Trasformation subsystem ",
            "label": "Hierarchy",
            "unit": "-",
            "value": {
                "category": [
                    "Level 3",
                    "Level 2",
                    "Level 1",
                    "Level 0",
                    "Level 0",
                    "Level 0",
                    "Level 1",
                    "Level 0",
                    "Level 0",
                    "Level 0",
                    "Level 1",
                    "Level 0",
                    "Level 0",
                    "Level 0",
                    "Level 2",
                    "Level 1",
                    "Level 0",
                    "Level 0",
                    "Level 0",
                    "Level 1",
                    "Level 0",
                    "Level 0",
                    "Level 0",
                    "Level 1",
                    "Level 0",
                    "Level 0",
                    "Level 0",
                ],
                "child": [
                    ["ET0", "ET1"],
                    ["ET0_PTO_0_0", "ET0_PTO_1_0", "ET0_PTO_2_0"],
                    ["ET0_PTO_0_0_MechT", "ET0_PTO_0_0_ElectT", "ET0_PTO_0_0_GridC"],
                    "NA",
                    "NA",
                    "NA",
                    ["ET0_PTO_1_0_MechT", "ET0_PTO_1_0_ElectT", "ET0_PTO_1_0_GridC"],
                    "NA",
                    "NA",
                    "NA",
                    ["ET0_PTO_2_0_MechT", "ET0_PTO_2_0_ElectT", "ET0_PTO_2_0_GridC"],
                    "NA",
                    "NA",
                    "NA",
                    ["ET1_PTO_0_0", "ET1_PTO_1_0", "ET1_PTO_2_0"],
                    ["ET1_PTO_0_0_MechT", "ET1_PTO_0_0_ElectT", "ET1_PTO_0_0_GridC"],
                    "NA",
                    "NA",
                    "NA",
                    ["ET1_PTO_1_0_MechT", "ET1_PTO_1_0_ElectT", "ET1_PTO_1_0_GridC"],
                    "NA",
                    "NA",
                    "NA",
                    ["ET1_PTO_2_0_MechT", "ET1_PTO_2_0_ElectT", "ET1_PTO_2_0_GridC"],
                    "NA",
                    "NA",
                    "NA",
                ],
                "design_id": [
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                    "Array_01",
                ],
                "failure_rate_repair": [
                    "NA",
                    "NA",
                    "NA",
                    1.510959497491122,
                    4255.203822326559,
                    0.030213788359045195,
                    "NA",
                    1.510959497491122,
                    4255.203822326559,
                    0.030213788359045195,
                    "NA",
                    1.510959497491122,
                    4255.203822326559,
                    0.030213788359045195,
                    "NA",
                    "NA",
                    1.510959497491122,
                    4255.203822326559,
                    0.030213788359045195,
                    "NA",
                    1.510959497491122,
                    4255.203822326559,
                    0.030213788359045195,
                    "NA",
                    1.510959497491122,
                    4255.203822326559,
                    0.030213788359045195,
                ],
                "failure_rate_replacement": [
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                ],
                "gate_type": [
                    "AND",
                    "1/3",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "1/3",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                    "AND",
                ],
                "name_of_node": [
                    "Array_01",
                    "ET0",
                    "ET0_PTO_0_0",
                    "ET0_PTO_0_0_MechT",
                    "ET0_PTO_0_0_ElectT",
                    "ET0_PTO_0_0_GridC",
                    "ET0_PTO_1_0",
                    "ET0_PTO_1_0_MechT",
                    "ET0_PTO_1_0_ElectT",
                    "ET0_PTO_1_0_GridC",
                    "ET0_PTO_2_0",
                    "ET0_PTO_2_0_MechT",
                    "ET0_PTO_2_0_ElectT",
                    "ET0_PTO_2_0_GridC",
                    "ET1",
                    "ET1_PTO_0_0",
                    "ET1_PTO_0_0_MechT",
                    "ET1_PTO_0_0_ElectT",
                    "ET1_PTO_0_0_GridC",
                    "ET1_PTO_1_0",
                    "ET1_PTO_1_0_MechT",
                    "ET1_PTO_1_0_ElectT",
                    "ET1_PTO_1_0_GridC",
                    "ET1_PTO_2_0",
                    "ET1_PTO_2_0_MechT",
                    "ET1_PTO_2_0_ElectT",
                    "ET1_PTO_2_0_GridC",
                ],
                "node_subtype": [
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                    "NA",
                ],
                "node_type": [
                    "System",
                    "Device",
                    "PTO",
                    "Component",
                    "Component",
                    "Component",
                    "PTO",
                    "Component",
                    "Component",
                    "Component",
                    "PTO",
                    "Component",
                    "Component",
                    "Component",
                    "Device",
                    "PTO",
                    "Component",
                    "Component",
                    "Component",
                    "PTO",
                    "Component",
                    "Component",
                    "Component",
                    "PTO",
                    "Component",
                    "Component",
                    "Component",
                ],
                "parent": [
                    "NA",
                    "Array_01",
                    "ET0",
                    "ET0_PTO_0_0",
                    "ET0_PTO_0_0",
                    "ET0_PTO_0_0",
                    "ET0",
                    "ET0_PTO_1_0",
                    "ET0_PTO_1_0",
                    "ET0_PTO_1_0",
                    "ET0",
                    "ET0_PTO_2_0",
                    "ET0_PTO_2_0",
                    "ET0_PTO_2_0",
                    "Array_01",
                    "ET1",
                    "ET1_PTO_0_0",
                    "ET1_PTO_0_0",
                    "ET1_PTO_0_0",
                    "ET1",
                    "ET1_PTO_1_0",
                    "ET1_PTO_1_0",
                    "ET1_PTO_1_0",
                    "ET1",
                    "ET1_PTO_2_0",
                    "ET1_PTO_2_0",
                    "ET1_PTO_2_0",
                ],
                "system": [
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                    "ET",
                ],
            },
        },
        "id": {
            "description": "Identifier of the Array",
            "label": "Array ID",
            "unit": "-",
            "value": "Array_01",
        },
        "materials": {
            "description": "Materials for ESA assesment",
            "label": "Materials",
            "unit": "-",
            "value": [{"material_name": "steel", "material_quantity": 426000.0}],
        },
        "pto_subsystems": {
            "description": "Subsystems",
            "label": "Subsystems",
            "unit": "-",
            "value": ["MechT", "ElectT", "GridC"],
        },
    },
    "ed_results": {
        "study_id": 1,
        "network_id": 1,
        "array_cable_total_length": 1630.8326112068523,
        "export_cable_total_length": 1065.2400289945676,
        "power_histogram": [0.0, 0.0, 1.0],
        "array_power_output": [-0.1248018661436312],
        "array_reactive_output": [-0.09954507030499604],
        "annual_yield": 20427807398.67293,
    },
    "downtime_hours": {
        "project_life": 10,
        "n_devices": 2,
        "downtime": [
            {
                "device_id": "device11",
                "downtime_table": {
                    "year": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    "jan": [744, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    "feb": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    "mar": [0, 43, 0, 0, 0, 0, 432, 15, 0, 15],
                    "apr": [0, 0, 0, 0, 0, 564, 0, 0, 0, 0],
                    "may": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    "jun": [0, 0, 196, 0, 124, 0, 532, 0, 0, 0],
                    "jul": [0, 0, 0, 643, 0, 324, 0, 0, 0, 0],
                    "aug": [56, 0, 0, 0, 56, 0, 56, 0, 0, 0],
                    "sep": [0, 145, 0, 0, 0, 0, 0, 0, 0, 0],
                    "oct": [0, 0, 0, 88, 0, 0, 0, 0, 0, 0],
                    "nov": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    "dec": [0, 0, 0, 0, 0, 0, 159, 0, 0, 0],
                },
            },
            {
                "device_id": "device12",
                "downtime_table": {
                    "year": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                    "jan": [744, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    "feb": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    "mar": [0, 43, 0, 0, 0, 0, 0, 15, 0, 0],
                    "apr": [0, 0, 0, 0, 0, 532, 0, 0, 0, 180],
                    "may": [0, 0, 0, 0, 0, 42, 0, 0, 0, 0],
                    "jun": [0, 0, 0, 0, 124, 0, 0, 0, 0, 0],
                    "jul": [0, 140, 0, 643, 0, 324, 0, 0, 0, 0],
                    "aug": [56, 0, 0, 0, 0, 0, 0, 0, 0, 720],
                    "sep": [0, 0, 543, 0, 0, 0, 0, 0, 0, 720],
                    "oct": [0, 0, 0, 643, 0, 0, 120, 453, 0, 744],
                    "nov": [0, 0, 0, 0, 47, 0, 0, 0, 0, 720],
                    "dec": [0, 0, 0, 0, 96, 0, 0, 0, 0, 744],
                },
            },
        ],
    },
}


sys.stdout = sys.stderr = open("hooks-output.txt", "w", buffering=1)


def if_not_skipped(func):
    def wrapper(transaction):
        if not transaction["skip"]:
            func(transaction)

    return wrapper


def remove_all_projects(tr):
    protocol = tr["protocol"]
    host = tr["host"]
    port = tr["port"]
    projects_ids = get_project_ids(tr)
    for speyId in projects_ids:
        requests.delete(f"{protocol}//{host}:{port}/spey/{speyId}")


def get_project_ids(tr):
    existing_project = check_project(tr)
    ids = [pr["speyId"] for pr in existing_project]
    return list(set(ids))


def check_project(tr):
    protocol = tr["protocol"]
    host = tr["host"]
    port = tr["port"]
    existing_project = requests.get(f"{protocol}//{host}:{port}/spey").json()
    return existing_project


def post_a_project(tr):
    protocol = tr["protocol"]
    host = tr["host"]
    port = tr["port"]

    r = requests.post(f"{protocol}//{host}:{port}/spey", json=body_project)
    print(r.json())
    speyId = r.json()["ID"]
    tr["_spey_id"] = speyId
    request_input = requests.post(
        f"{protocol}//{host}:{port}/spey/{speyId}/inputs", json=body
    )

    print(tr)

    print("POST A PROJECT")
    print(body)
    print(f"{protocol}//{host}:{port}/spey")
    print(r.status_code)
    print(r)
    print(f"{protocol}//{host}:{port}/spey/{speyId}/inputs")
    print(request_input.status_code)
    print(request_input)


# def post_a_farm(tr, ecId):
#     protocol = tr['protocol']
#     host = tr['host']
#     port = tr['port']

#     r = requests.post(f'{protocol}//{host}:{port}/ec/{ecId}/farm',
#                       json=body_farm)
#     print('POST A FARM')
#     print(body_farm)
#     print(f'{protocol}//{host}:{port}/ec/{ecId}/farm')
#     print(r.status_code)
#     print(r)


# def post_a_device_list(tr, ecId):
#     protocol = tr['protocol']
#     host = tr['host']
#     port = tr['port']

#     r = requests.post(f'{protocol}//{host}:{port}/ec/{ecId}/devices',
#                       json=[body_device, body_device])
#     print('POST A LIST OF DEVICES')
#     print([body_device, body_device])
#     print(f'{protocol}//{host}:{port}/ec/{ecId}/devices')
#     print(r.status_code)
#     print(r)


def get_a_project(tr):
    print(tr)
    existing_project = check_project(tr)
    print(existing_project)
    if existing_project:  # if any project exist
        project = existing_project[0]
    else:
        post_a_project(tr)
        projects = check_project(tr)

        if projects:
            project = projects
        else:
            print("ERROR NO PROJECT AVAILABLE IN THE DB")
    print("project")

    return project


# def get_a_project_wo_farm(tr):
#     print('get_a_project_wo_farm')
#     existing_project = check_project(tr)
#     if existing_project:  # if any project exist
#         for project in existing_project:
#             if not project['farm']:
#                 print('Found a project without a farm')
#                 return project

#     print('No project without a farm')
#     print('posting a new project')
#     post_a_project(tr)
#     project = get_a_project_wo_farm(tr)
#     print(project)

#     return project


# def get_project_w_device(tr):
#     existing_project = check_project(tr)
#     if existing_project:  # if any project exist
#         for project in existing_project:
#             if project['device']:
#                 print('Found a project with a device')
#                 return project
#         else:
#             post_device()

#     post_a_project(tr)
