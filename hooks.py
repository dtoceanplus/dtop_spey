import os
import json
import requests
import sys

import traceback
from functools import partial

import dredd_hooks as hooks

from dredd_utils import *

import pickle


# @hooks.before("/spey > List SPEY assessments > 200 > application/json")
@hooks.before("/spey > List SPEY assessments > 404")
# @hooks.before("/spey > Create a new SPEY assessment > 201")
# @hooks.before("/spey > Create a new SPEY assessment > 400")
# @hooks.before("/spey/{speyId} > Returns the assessment > 200 > application/json")
# @hooks.before("/spey/{speyId} > Returns the assessment > 404")
@hooks.before("/spey/{speyId} > Returns the assessment > 500")
# @hooks.before("/spey/{speyId} > Delete the SPEY assessment > 200")
# @hooks.before("/spey/{speyId} > Delete the SPEY assessment > 404")
# @hooks.before("/spey/{speyId} > Update the SPEY assessment > 201")
# @hooks.before("/spey/{speyId} > Update the SPEY assessment > 400")
# @hooks.before("/spey/{speyId}/inputs > Collects inputs for a new SPEY assessment > 201")
# @hooks.before("/spey/{speyId}/inputs > Collects inputs for a new SPEY assessment > 400")
# @hooks.before("/spey/{speyId}/inputs > Returns the inputs > 200 > application/json")
# @hooks.before("/spey/{speyId}/inputs > Returns the inputs > 404")
@hooks.before("/spey/{speyId}/inputs > Returns the inputs > 500")
# @hooks.before(
#     "/spey/{speyId}/efficiency > Returns the efficiency of the array and the device for each subsystem > 200 > application/json"
# )
# @hooks.before(
#     "/spey/{speyId}/efficiency > Returns the efficiency of the array and the device for each subsystem > 404"
# )
@hooks.before(
    "/spey/{speyId}/efficiency > Returns the efficiency of the array and the device for each subsystem > 500"
)
# @hooks.before(
#     "/spey/{speyId}/energy-production > Returns the Energy Production and Losses of the array and for each device > 200 > application/json"
# )
# @hooks.before(
#     "/spey/{speyId}/energy-production > Returns the Energy Production and Losses of the array and for each device > 404"
# )
@hooks.before(
    "/spey/{speyId}/energy-production > Returns the Energy Production and Losses of the array and for each device > 500"
)
# @hooks.before(
#     "/spey/{speyId}/alternative-metrics > Returns a set of Alternative metrics of the system performance and energy yield of the array and the device > 200 > application/json"
# )
# @hooks.before(
#     "/spey/{speyId}/alternative-metrics > Returns a set of Alternative metrics of the system performance and energy yield of the array and the device > 404"
# )
@hooks.before(
    "/spey/{speyId}/alternative-metrics > Returns a set of Alternative metrics of the system performance and energy yield of the array and the device > 500"
)
# @hooks.before(
#     "/spey/{speyId}/power-quality > Returns an estimate of the Power Quality of the Transformation subsystem at array and device level and of the Delivery System at array level > 200 > application/json"
# )
# @hooks.before(
#     "/spey/{speyId}/power-quality > Returns an estimate of the Power Quality of the Transformation subsystem at array and device level and of the Delivery System at array level > 404"
# )
@hooks.before(
    "/spey/{speyId}/power-quality > Returns an estimate of the Power Quality of the Transformation subsystem at array and device level and of the Delivery System at array level > 500"
)
# @hooks.before(
#     "/representation/{speyId} > Export data representation > 200 > application/json"
# )
# @hooks.before("/representation/{speyId} > Export data representation > 404")
@hooks.before("/representation/{speyId} > Export data representation > 500")
@hooks.before(
    "/representation/filters > Filters for representation import > 200 > application/json"
)
def _skip(transaction):
    transaction["skip"] = True


@hooks.before_each
@if_not_skipped
def before_all(transaction):
    print("\n" + transaction["name"])
    remove_all_projects(transaction)


@hooks.before("/spey > Create a new SPEY assessment > 201")
@if_not_skipped
def post_project(transaction):
    # data = json.loads(transaction["request"]["body"])
    # data.pop("speyId", None)
    transaction["request"]["body"] = json.dumps(body_project)
    print(transaction["request"]["body"])


@hooks.before("/spey/{speyId}/inputs > Collects inputs for a new SPEY assessment > 201")
@if_not_skipped
def post_project(transaction):
    # data = json.loads(transaction["request"]["body"])
    # data.pop("speyId", None)
    protocol = transaction["protocol"]
    host = transaction["host"]
    port = transaction["port"]
    r = requests.post(f"{protocol}//{host}:{port}/spey", json=body_project)
    speyId = r.json()["ID"]
    transaction["_spey_id"] = speyId

    speyId = transaction["_spey_id"]
    transaction["id"] = transaction["id"].replace("123", str(speyId))
    transaction["request"]["uri"] = transaction["request"]["uri"].replace(
        "123", str(speyId)
    )
    transaction["fullPath"] = transaction["fullPath"].replace("123", str(speyId))
    transaction["request"]["body"] = json.dumps(body)


@hooks.before("/spey > Create a new SPEY assessment > 400")
@if_not_skipped
def post_assessment_400(transaction):
    """
    Remove one of required parameters when creating a project.
    """
    data = json.loads(transaction["request"]["body"])
    data.pop("description")
    transaction["request"]["body"] = json.dumps(data)


@hooks.before("/spey/{speyId}/inputs > Collects inputs for a new SPEY assessment > 400")
@if_not_skipped
def post_project_400(transaction):
    # data = json.loads(transaction["request"]["body"])
    # data.pop("speyId", None)
    protocol = transaction["protocol"]
    host = transaction["host"]
    port = transaction["port"]
    r = requests.post(f"{protocol}//{host}:{port}/spey", json=body_project)
    speyId = r.json()["ID"]
    transaction["_spey_id"] = speyId

    speyId = transaction["_spey_id"]
    transaction["id"] = transaction["id"].replace("123", str(speyId))
    transaction["request"]["uri"] = transaction["request"]["uri"].replace(
        "123", str(speyId)
    )
    transaction["fullPath"] = transaction["fullPath"].replace("123", str(speyId))
    data = json.loads(transaction["request"]["body"])
    data.pop("device_characteristics_general")

    transaction["request"]["body"] = json.dumps(data)


@hooks.before("/spey/{speyId} > Returns the assessment > 200 > application/json")
@hooks.before("/spey/{speyId}/inputs > Returns the inputs > 200 > application/json")
@hooks.before(
    "/spey/{speyId}/power-quality > Returns an estimate of the Power Quality of the Transformation subsystem at array and device level and of the Delivery System at array level > 200 > application/json"
)
@hooks.before(
    "/spey/{speyId}/efficiency > Returns the efficiency of the array and the device for each subsystem > 200 > application/json"
)
@hooks.before(
    "/spey/{speyId}/energy-production > Returns the Energy Production and Losses of the array and for each device > 200 > application/json"
)
@hooks.before(
    "/spey/{speyId}/alternative-metrics > Returns a set of Alternative metrics of the system performance and energy yield of the array and the device > 200 > application/json"
)
@hooks.before(
    "/representation/{speyId} > Export data representation > 200 > application/json"
)
@if_not_skipped
def get_project_id(transaction):
    """
    Add a correct project path
    """
    project = post_a_project(transaction)
    speyId = transaction["_spey_id"]
    transaction["id"] = transaction["id"].replace("123", str(speyId))
    transaction["request"]["uri"] = transaction["request"]["uri"].replace(
        "123", str(speyId)
    )
    transaction["fullPath"] = transaction["fullPath"].replace("123", str(speyId))


@hooks.before("/spey/{speyId} > Returns the assessment > 404")
@hooks.before("/spey/{speyId}/inputs > Returns the inputs > 404")
@hooks.before(
    "/spey/{speyId}/power-quality > Returns an estimate of the Power Quality of the Transformation subsystem at array and device level and of the Delivery System at array level > 404"
)
@hooks.before(
    "/spey/{speyId}/efficiency > Returns the efficiency of the array and the device for each subsystem > 404"
)
@hooks.before(
    "/spey/{speyId}/energy-production > Returns the Energy Production and Losses of the array and for each device > 404"
)
@hooks.before(
    "/spey/{speyId}/alternative-metrics > Returns a set of Alternative metrics of the system performance and energy yield of the array and the device > 404"
)
@hooks.before("/representation/{speyId} > Export data representation > 404")
@if_not_skipped
def get_project_id_404(transaction):
    """
    Add an incorrect project path
    """
    transaction["request"]["uri"] = transaction["request"]["uri"].replace("123", "-1")
    transaction["fullPath"] = transaction["fullPath"].replace("123", "-1")


# # @hooks.before("/spey/{speyId} > Returns the assessment > 500")
# # @if_not_skipped
# # def get_project_id_500(transaction):
# #     """
# #     Add an incorrect project path
# #     """
# #     transaction["request"]["uri"] + transaction["request"]["uri"].replace("123", 123)
# #     transaction["fullPath"] = transaction["fullPath"].replace("123", 123)


@hooks.before("/spey/{speyId} > Delete the SPEY assessment > 200")
@if_not_skipped
def delete_project_id(transaction):
    """
    Add an incorrect project path
    """
    project = post_a_project(transaction)
    speyId = transaction["_spey_id"]
    # transaction["id"] = transaction["id"].replace("123", str(speyId))
    transaction["request"]["uri"] = transaction["request"]["uri"].replace(
        "123", str(speyId)
    )
    transaction["fullPath"] = transaction["fullPath"].replace("123", str(speyId))


@hooks.before("/spey/{speyId} > Delete the SPEY assessment > 404")
@if_not_skipped
def delete_project_400(transaction):
    """
    Add an incorrect project path
    """
    project = post_a_project(transaction)
    # transaction["id"] = transaction["id"].replace("123", str(speyId))
    transaction["request"]["uri"] = transaction["request"]["uri"].replace("123", "-1")
    transaction["fullPath"] = transaction["fullPath"].replace("123", "-1")


@hooks.before("/spey/{speyId} > Update the SPEY assessment > 201")
@if_not_skipped
def put_assessment_id(transaction):
    """
    Update a project by ID
    """
    project = post_a_project(transaction)
    speyId = transaction["_spey_id"]
    # transaction["id"] = transaction["id"].replace("123", str(speyId))
    transaction["request"]["uri"] = transaction["request"]["uri"].replace(
        "123", str(speyId)
    )
    transaction["fullPath"] = transaction["fullPath"].replace("123", str(speyId))

    requested = json.loads(transaction["request"]["body"])
    requested.pop("speyId", None)

    transaction["request"]["body"] = json.dumps(requested)


@hooks.before("/spey/{speyId} > Update the SPEY assessment > 400")
@if_not_skipped
def put_assessment_400(transaction):
    """
    Update a project by ID
    """
    project = post_a_project(transaction)
    speyId = transaction["_spey_id"]
    # transaction["id"] = transaction["id"].replace("123", str(speyId))
    transaction["request"]["uri"] = transaction["request"]["uri"].replace("123", "-1")

    # Cambiare init di assessment metodo PUT con la funzione update
    # Cambiare hook put errore 400.
