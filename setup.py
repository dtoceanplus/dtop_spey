# This file is for creating Python package.

# Packaging Python Projects: https://packaging.python.org/tutorials/packaging-projects/
# Setuptools documentation: https://setuptools.readthedocs.io/en/latest/index.html

import setuptools

setuptools.setup(
    name="dtop_spey",
    version="0.0.1",
    packages=setuptools.find_packages(where="src"),
    # packages=setuptools.find_packages(),
    package_dir={"": "src"},
    install_requires=[
        "numpy",
        "matplotlib",
        "bson",
        "flask",
        "flask-babel",
        "flask-cors",
        "requests",
        "pandas",
        "dtop-shared-library",
    ],
    include_package_data=True,
    zip_safe=False,
    dependency_links=[
        "git+ssh://git@gitlab.com/dtoceanplus/dtop-shared-library.git#egg=dtop_shared_library-0.0.1"
    ],
)
