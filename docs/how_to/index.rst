.. _spey-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the System Performance and Energy Yield module. 
These guides are intended for users who have previously completed all the :ref:`System Performance and Energy Yield tutorials <spey-tutorials>` and have a good knowledge of the features and workings of the System Performance and Energy Yield module. 
While the tutorials give an introduction to the basic usage of the module, these *how-to guides* tackle slightly more advanced topics.

**MACHINE CHARACTERISATION DATA (MANDATORY)**

All the data needed for the Machine Characterisation data must be input via the GUI in standalone mode, or automatically fetched by the corresponding URI of the provider module in integrated mode:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _DATA-FROM-MACHINE-CHARACTERISATION

   * - Variable
     - Unit
     - Description
     - GUI in standalone mode
     - URI in integrated mode

   * - Technology
     - -
     - Wave or Tidal
     - Dropdown Menu (Wave/Tidal)
     - /mc/<mcId>
   
   * - Rated Power
     - kW
     - Rated power of the prime mover
     - Input number
     - /mc/<mcId>/general

   * - Mass
     - kg
     - Mass of the prime mover
     - Input number
     - /mc/<mcId>/dimensions

   * - Wet Area
     - m^2
     - Wet area of the prime mover
     - Input number
     - /mc/<mcId>/dimensions

   * - Characteristic Length
     - m
     - The characteristic length of the prime mover. In case of tidal turbine, this is represented by the rotor diameter
     - Input number
     - /mc/<mcId>/dimensions

**SITE CHARACTERISATION DATA (MANDATORY)**

All the data needed for the Site Characterisation data must be input partially directly via the GUI and by uploading a support file in standalone mode, or automatically fetched byt the corresponding URI of the provider module in integrated mode:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _DATA-FROM-SITE-CHARACTERISATION

   * - Variable
     - Unit
     - Description
     - GUI in standalone mode
     - URI in integrated mode

   * - Annual Average Energy Flux Tidal
     - kw/m2
     - Annual average flux of the site
     - input number
     - /sc/scId/point/statistics/currents/Basic/Flux

   * - Annual Average Energy Flux Wave
     - kw/m
     - Annual average flux of the site
     - Input number
     - /sc/scId/point/statistics/waves/Basic/CgE

   * - Lease area extension
     - km2
     - value of the extension of the lease area of the site
     - Input number
     - /sc/scId/farm/info

   * - Monthly Wave Scatter Diagram (Wave Case)/Current Monthly Scenario(Tidal case)
     - -
     - Occurrences of the sea states
     - Support Excel File
     - /sc/scId/point/statistics/waves/EJPD3v/hs_tp_dp_monthly_flattened / /sc/scId/farm/scenarii_matrices/currentsmonthly

The structure of the support Excel file  is the same both for tidal and wave cases.

The name of the rows are fixed: ‘id’, ‘January’, ‘February’, ‘March’, ‘April’, ‘May’, ‘June’, ‘July’, ‘August’, ‘September’, ‘October’, ‘November’, ‘December’. The user should add as many columns as the number of sea (wave or tidal) conditions he/she wants to examine. Each sea state is identified by an incremental integer. The values in the remaining cells corresponds to the monthly occurrence of each sea state.

**ENERGY CAPTURE DATA (OPTIONAL)**

The data required in terms of Energy Capture are optional. However, if the user decides to include the set of data corresponding to Energy Capture, the full stack of data is required (no partial input is permitted). When not in integrated mode, all the data needed for the Energy Capture data must be input partially directly via the GUI and by uploading a support file in standalone mode. In integrated mode it is automatically fetched by the corresponding URI of the provider module in integrated mode:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _DATA-FROM-ENERGY-CAPTURE

   * - Variable
     - Unit
     - Description
     - GUI in standalone mode
     - URI in integrated mode

   * - Number of devices
     - -
     - Number of devices
     - Inout Number
     - /ec/ecId/farm

   * - Array Annual Captured Energy Production
     - kWh
     - Annual Captured Energy Production at Array level
     - Input Number
     - /ec/ecId/farm

   * - Array q-factor
     - -
     - 1-facto of the array
     - Input Number
     - /ec/ecId/farm

   * - Device Captured Energy
     - -
     - Information of captured energy production at device level of aggregation
     - Excel File
     - /ec/ecId/devices

In the support Excel File, the names of the Columns are fixed: ‘id’, ‘Annual Captured energy [kWh]’ and ‘q-factor’. The user must include as many rows as the number of devices, identifying each of them by an increasing integer and adding the corresponding value for the energy production of the device and the q-factor.

**ENERGY TRANSFORMATION DATA** 

The data required in terms of Energy transformation are optional. However, if the user decides to include the set of data corresponding to Energy transformation, the full stack of data is required (no partial input is permitted). When not in integrated mode, all the data needed for the Energy transformation data must be input partially directly via the GUI and by uploading a support file in standalone mode. In integrated mode it is automatically fetched by the corresponding URI of the provider module in integrated mode:
 
The data consist in:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _DATA-FROM-ENERGY-TRANSFORMATION

   * - Variable
     - Unit
     - Description
     - GUI in standalone mode
     - URI in integrated mode

   * - Array Annual Transformed Energy Production
     - kWh
     - Annual Transformed Energy Production at Array level
     - Input Number
     - /energy_transf/etId/array

   * - Device Transformed Energy
     - -
     - Information of transformed energy production at device level of aggregation
     - Excel file
     - /energy_transf/etId/devices

In such a support Excel file, the names of the Columns are fixed: ‘id’, ‘Annual transformed energy [kWh]’, ‘Active power per Sea State [kW]’ and ‘Reactive power per Sea State [kW]’. The user must include as many rows as the number of devices, identifying each of them by an increasing integer an adding the corresponding value for the energy production of the device, and a list of the active/reactive power per sea state. The length of these lists is the same of the number of sea states considered.

**ENERGY DELIVERY DATA**

The data required in terms of Energy delivery are optional. However, if the user decides to include the set of data corresponding to Energy delivery, the full stack of data is required (no partial input is permitted). When not in integrated mode, all the data needed for the Energy delivery data must be input partially directly via the GUI and by uploading a support file in standalone mode. In integrated mode it is automatically fetched by the corresponding URI of the provider module in integrated mode:
 
The data consist in:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _DATA-FROM-ENERGY-TRANSFORMATION

   * - Variable
     - Unit
     - Description
     - GUI in standalone mode
     - URI in integrated mode

   * - Array Annual Delivered Energy Production
     - kWh
     - Annual Delivered Energy Production at Array level
     - Input Number
     - /api/energy-deliv-studies/edId/results

   * - Total Length of Cables
     - m
     - Total lenght of the electrical cables
     - Input Number
     - /api/energy-deliv-studies/edId/results

   * - Export Cable Length
     - m
     - Total length of the export cables
     - Input Number
     - /api/energy-deliv-studies/edId/results

   * - Power Delivery
     - -
     - Information of delivered energy production
     - Excel File
     - /api/energy-deliv-studies/edId/results

In the support Excel file, the name of the rows are fixed: ‘id’, ‘Active Power per Condition [kW]’, ‘Reactive Power per Condition [kVar]’. The user should add as many columns as the number of sea (wave or tidal) conditions input in Site condition data. Each sea state is identified by an incremental integer (first row). The values in the remaining cells corresponds to the values of active/reactive power per sea state.

**LOGISTICS AND MARINE OPERATION DATA**

The data required in terms of logistics and marine operation are optional. However, if the user decides to include the set of data corresponding to Logistics and Marine operation, the full stack of data is required (no partial input is permitted). When not in integrated mode, all the data needed for the Logistics and Marine operation data must be input partially directly via the GUI and by uploading a support file in standalone mode. In integrated mode it is automatically fetched by the corresponding URI of the provider module in integrated mode:
 
The data consist in:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _DATA-FROM-LOGISTICS

   * - Variable
     - Unit
     - Description
     - GUI in standalone mode
     - URI in integrated mode

   * - Project Life
     - years
     - Duration of the ocean renewable energy project
     - Input Number
     - /api/lmoId/phases/maintenance/downtime

   * - Downtime per device per year per month
     - hours
     - Downtime per device per year per month
     - JSON file
     - /api/lmoId/phases/maintenance/downtime


The JSON file should contain a list (introduced by ‘[‘, concluded by a ‘]’, and the elements are separated by a ‘,’) of objects (an object for each device is required), introduced by a ‘{‘ and concluded by a ‘}’, and the different fields are separated by a ‘,’.  Each field consists of a label, between quotation marks, followed by a colon and the value corresponding to this label.

The following fields should be included in the JSON file:

-	 “device_id” – the value should be a string (identified by quotation marks)

-	“downtime_table” – is an object (identified by ‘{}’) whose fields are:

   -- “year” – a sequential list from 0 to project life-1

   -- “jan”, “feb”, “mar”, “apr”, “may”, “jun”, “jul”, “aug”, “sep”, “oct”, “nov”, “dec” . They are lists containing the number of dowintime hours for each year of the project life. The length of these lists is the same of the field “year”.
 


