.. _create-new-spey-study-standalone-with-MM:

**********************************************
Create a new SPEY entity in Standalone from MM
**********************************************

The first step is to create a new study standalone from the Main Module, and select SPEY.

1.	Fill in an appropriate name and description to identify your entity.

2.	You will be automatically directed to the view where you can upload the inputs. 

