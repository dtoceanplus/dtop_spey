.. _exploring-results:

***********************
Exploring the Results
***********************

If SPEY has run successfully, the user can visualise and export the results. 

The user can visualise the outputs of one specific assessment by clicking on the dedicated tab (Efficiency, Alternative Metrics, Energy Production and Power Quality). The user can therefore visualise the outputs as well as the inputs required for a specific assessment by clicking on Outputs/Inputs tab. A friendly description of each parameter can be shown by approaching the cursor to the icon next to the parameter name. 

The different columns will show:

(1)	Parameter: the parameter name


(2)	Units: the units of the parameter


(3)	Value: the actual value of that parameter.

Additionally, all the Outputs have the following:

(4)	Level of Aggregation: it could be array or device, if the metrics has been calculated at array or device level. 

Finally, the outputs of Power quality, Alternative metrics and Efficiency have also the column:

(5)	View: to view a diagram of parameter with respect to the device number or the sea state number.

For each assessment, the inputs and the outputs could be exported in JSON format by clicking on the ‘Save data’ button.

The fields Parameter and Level of Aggregation are sortable when present. 

The fields Level of Aggregation and Value are filterable: the former, with respect to ‘array’ or ‘device’; the latter, based if an assessment has been calculated or not. By default, only the calculated assessments are visible.
