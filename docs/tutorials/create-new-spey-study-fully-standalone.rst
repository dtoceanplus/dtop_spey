.. _create-new-spey-study-fully-standalone:

*************************************************
Create a new SPEY entity in Fully Standalone Mode
*************************************************

The first step is to create a new entity within the SPEY module . After installing, the user should connect to 
SPEY module `http://spey.DOMAIN/#/spey <http://spey.DOMAIN/#/spey>`_ in his/her browser.


1.	In the left menu, select ``SPEY Studies`` and click ``Create SPEY study``. 

2.	Fill in an appropriate name and description to identify your entity.

3.	Click ``‘create`` to save these inputs and return to the list of studies.

4.	From the list of studies, click ``Open`` to start working on an entity by redirecting the user to inputs page, ``Edit`` to change the name or description, or ``Delete`` to permanently remove an entity. If the status of an entity is 100% (which means that the assessments where calculated and finalised), two other buttons are active: ``Results``, which leads directly to the outputs pages and ``Export DR``, in order to export a json file with the SPEY contribution to the Digital Representation of the project. 

The user can also click on ``Open`` for an entity that has been already completed and fully calculated. The user will be redirected to the inputs page, also in this case, in case he/she wants to change some oif the inputs.


