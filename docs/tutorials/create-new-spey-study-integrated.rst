.. create-new-spey-study-integrated:

*******************************************
Create a new SPEY entity integrated from MM
*******************************************

The first step is to create a new entity of SPEY in integrated mode from the Main Module.

1.	Fill in an appropriate name and description to identify your entity.

2.	You will be automatically directed to the view where you can upload the inputs. 

