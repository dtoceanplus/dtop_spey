.. _insert-inputs-run-study:

*************************************
Inserting Inputs and run a Spey Study
*************************************

The inputs, in the standalone mode, have been categorised in 6 groups, namely reproducing the modules that the user should have been running if he/she was working in integrated mode: Machine characterisation, Site Characterisation, Energy capture, Energy Transformation, Energy Delivery, Logistics and Marine Operation planning. 

When running in integrated fashion, then the data from the already run modules will be retrieved and fetched.

The only two sets of data that are mandatory are those pertinent to Machine Characterisation and Site Characterisation (in this order). All the other sets of data are optional. All these sets of data are optional; however, if the user must input all the data required for a specific set.

When an entity from another module cannot be retrieved within the same study, the corresponding green button is activated. By clicking to the green buttons, a form will appear to the user. For the description of the data requirements for each set, see the How-to-guide section.
 
Once that the user has filled the data required to calculate the assessments, he/she can click on the button Run if the study was not run anytime before; if this is not the case, the green button Run will be deactivated and the user can click on Update and Re-Run orange button.

