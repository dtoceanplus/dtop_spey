.. _spey-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the System Performance and Energy Yield tool.
They are intended for those who are new to the System Performance and Energy Yield tool.
It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others. 

The use of the SPEY tool is done following 3 steps: 

.. toctree::
   :maxdepth: 3

   create-new-spey-study-fully-standalone
   create-new-spey-study-standalone-with-MM
   create-new-spey-study-integrated
   insert-inputs-run-study
   exploring-results

