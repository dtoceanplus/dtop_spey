Complexity 3
============

Alternative metrics module
--------------------------

.. automodule:: dtop_spey.business.cpx3.AlternativeMetricsCpx3
   :members:
   :undoc-members:
   :show-inheritance:

Efficiency module
-----------------

.. automodule:: dtop_spey.business.cpx3.EfficiencyCpx3
   :members:
   :undoc-members:
   :show-inheritance:

Power Quality module
--------------------

.. automodule:: dtop_spey.business.cpx3.PowerQualityCpx3
   :members:
   :undoc-members:
   :show-inheritance:
