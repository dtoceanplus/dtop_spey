Example submodule
=================

You can use ``automodule`` to autogenerate docstrings from Python source code. 
For example:

.. code-block:: RST

   .. automodule:: dtop_spey.business
      :members:
      :undoc-members:
      :show-inheritance:

See the Stage Gate folder for further examples. 
