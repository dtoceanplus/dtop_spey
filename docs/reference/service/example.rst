Example submodule (service layer)
=================================

You can use ``automodule`` to autogenerate docstrings from Python source code. 
For example:

.. code-block:: RST

   .. automodule:: dtop_spey.service.assessment
      :members:
      :undoc-members:
      :show-inheritance:

See the Stage Gate folder for further examples. 
