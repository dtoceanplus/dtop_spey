.. _spey-explanation:

*********************
Background and theory
*********************

The System Performance and Energy Yield module assesses the performance of the system in terms of energy yield during all the stages of the resource-to-wire conversion, including the downtime of the system. SPEY also computes the efficiencies at the different stages of the transformation, assesses the power quality at the delivery point, and produces a set of alternative metrics against a set of technical parameters.
It is one of the four assessment modules of the DTOceanPlus design suite of tools.

Use Cases
=========
The Generic User Case can be generally summarised as following:

.. figure:: figures/use-cases.svg

      Generic Use Case for using the System Performance and Energy Yield Tools

The User can:

1)	Run SPEY within the framework of the Stage Gate (SG) or Structured Innovation (SI) Design tools.

2)	Run SPEY after running the set of Deployment Design tools of DTOceanPlus.

3)	Use in standalone mode.

By considering the three Use cases above, following is summarised the dependencies of SPEY from/to other modules in DTOceanPlus.

    - Modules that provide services that SPEY consumes
        Site Characterisation (SC), Machine Characterisation (MC), Energy Capture (EC), Energy Transformation (ET), Energy Delivery (ED), Logistics & Marine Operations (LMO)

    - Modules that are consuming services from SPEY
        Stage Gate (SG)


**Use Case within the framework of SG/SI Design Tools**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In this case, the SPEY tool will be run within the framework of the Stage Gate or Structured Innovation Design tools. 

The following steps are identified for this use case:

1)	The user runs the framework of the SI/SG Tools

2)	The SI/SG will require eventually some assessments from the SPEY module

3)	The SPEY Module will check if the needed information is available and in case it is not, it will request the user to input the information from the relevant Deployment Design Tools

4)	The User will complement the information and run the Deployment Design Tools

5)	SPEY will be run and perform the assessments

6)	SPEY will provide the assessments to SI/SG Tools to complete their framework 

7)	The outcome will be shown to the User.

.. figure:: figures/stagegate.svg

      Use Case for using the System Performance and Energy Yield toll with the Stage Gate Tool.


**Use Case after Deployment Design Tools**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The User will run one or more Deployment Design Tools and then he/she will run the SPEY module to carry out the assessments in terms of performance and energy yield. The numerical results as well as the graphs/diagrams will be exposed to the user.

.. figure:: figures/deployment.svg

**Standalone Mode**
^^^^^^^^^^^^^^^^^^^^^^^^^^
In this Case, the User wants only to run the SPEY module, to get some assessments in terms of performances and energy yield. 

The user, in this case, will provide all the required inputs and he/she will be exposed to the overall results of the assessment.

.. figure:: figures/standalone.svg
      
      Use Case for using the System Performance and Energy Yield in a standalone mode.

Functionalities
===============

The System Performance and Energy Yield module has 4 major functionalities:  

1. :ref:`Efficiency` - A set of dimensionless parameters expressing how well the overall system, as well as the different systems, perform  with respect to the available resource and the other subsystems, both at array and at device level of aggregation.

2. :ref:`Energy_production` - Estimates the gross and net energy production, during the lifetime, as well as the average annual and monthly production due to the downtime of the system.

3. :ref:`Power_quality` - An estimatation of the active power production with respect to the reactive one can be estimated for different subsystems and levels of aggregation.

4. :ref:`Alternative_metrics` - A set of dimensional parameters expressing how well the overall system, as well as the different sub-subsystems, perform with respect to the other parameters, as for example the lease area, the wetted surface of the prime mover, the mass, the rated power of the device, the characteristic length, and the length of the cabling, both at array and at device level of aggregation.


.. toctree::
   :hidden:
   :maxdepth: 2

   Efficiency
   Alternative_metrics
   Energy_production
   Power_quality

