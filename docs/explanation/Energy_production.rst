.. _Energy_production:

Energy Production
======================

This section estimates the gross and net energy production, during the lifetime, as well as the average annual and monthly production due to the downtime of the system.

The Energy Production assessments of the SPEY module aim at:

    -	Assess the gross energy that could be produced by the plant, for each device and at array level of aggregation, during the whole lifetime, for each year of life of the plant, and accounting for the monthly distribution of the resource.
    -	Estimating the losses of energy due to downtime of the plant for each device and at array level of aggregation, during the whole lifetime, for each year of life of the plant, and accounting for the monthly distribution of the resource, as well as the downtime hours for each month for each device.
    -	Assess the actual net energy delivered onshore for each month, for each device and at array level of aggregation, during the whole lifetime, for each year of life of the plant, and accounting for the monthly distribution of the resource, as well as the downtime hours for each month for each device.
    -	Assess the ratio between the net energy delivered onshore and the gross energy for each device and at array level of aggregation, during the whole lifetime, for each year of life of the plant, and accounting for the monthly distribution of the resource.
    -	Assess the ratio between the net energy delivered onshore and the gross energy for each device and at array level of aggregation, during the whole lifetime, for each year of life of the plant, and accounting for the monthly distribution of the resource.


**Inputs, Models And Outputs**

The inputs needed for carrying out the assessment in terms of Energy Production are shown in the following table:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _INPUTS FOR THE EVALUATION OF THE ENERGY PRODUCTION

   * - ID
     - Brief Description of the Input Quantity
     - Origin of the Data
     - Data Model in SPEY
     - Units
   * - CL
     - Level of Complexity
     - User
     - Number, Integer
     - -
   * - MRH
     - Monthly Resource Histogram is the Monthly probability of occurrence of the resource (Hs for Wave Energy, Vc for Tidal Energy) 
     - User/SC
     - Dictionary, whose keys are "bins" (the centroid of the bin) and "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December". The value for each key is a list with the same length of bins
     - -
   * - PDH
     - Power Deliver Histogram, It is the histogram of the power delivery per device.
     - user/ED
     - The bins must be the same as monthly_resource_histogram, and the value for power is a list with same length of bins
     - kW
   * - ND
     - Number of devices
     - User/EC
     - The bins must be the same as monthly_resource_histogram, and the value for power is a list with same length of bins
     - kW
   * - ND
     - Number of devices
     - User/EC
     - Number, Integer
     - -
   * - PL
     - Project Life
     - User/LMO
     - Number, Integer
     - -
   * - MDHD
     - Monthly Downtime Hours per Device
     - User/LMO
     - Dictionary of Pandas Tables. The keys are the device ids; the pandas tables have set as index the year of the project life (form from 1 to project life) and the columns are names as the month (first capital letter)
     - hours

The Energy Production functions can compute the gross energy that can be delivered onshore, the lost energy due to downtime and the net energy estimated to be delivered. 

It is important to notice that the downtime hours are supposed to be distributed uniformly through the sea states.

Gross Energy: the general definition of Gross Energy is the one in Eq. 11.

    .. math:: Gross Energy=Occurrence Histrogram\cdot Power Histogram\cdot Number Hours

As a function of the level of aggregation and the reference period, the following quantities can be calculated (Eq. 12 to Eq. 18):

	.. math:: Monthly Gross Energy per Device = MRH\cdot PDH\cdot 24\cdot (Number of Days in the Month)	

	.. math:: Annual Gross Energy per Device=MRH\cdot PDH\cdot 24\cdot 365.25

    .. math:: Lifetime Gross Energy per Device=MRH\cdot PDH\cdot 24\cdot 365.25\cdot PL

    .. math:: Lifetime Gross Energy per Device=MRH\cdot PDH\cdot 24\cdot 365.25\cdot PL

    .. math:: Array Monthly Gross Energy = \sum_{devices}^{}MRH\cdot PDH\cdot24\cdot (Number of Days in the Month)

    .. math:: Array Annual Gross Energy= \sum_{devices}^{}\cdot MRH\cdot PDH\cdot 24\cdot 365.25	

    .. math:: Array Lifetime Gross Energy=\sum_{devices}^{}\cdot MRH\cdot PDH\cdot 24\cdot 365.25\cdot PL

    .. math:: Lost Energy = Gross Energy\cdot \frac{Downtime Hours}{Number Hours}

The general definition of Lost Energy is the one in Eq. 18.

As a function of the level of aggregation and the reference period, the following quantities can be calculated: (Eq. 19 to Eq. 25)

    .. math:: Monthly Lost Energy per Device=Monthly Gross Energy per Device\cdot\frac{24\cdot MDHD}{Number of Days in the Month}

    .. math:: Annual Lost Energy per Device= \sum_{months}^{}Monthly Lost Energy per Device

    .. math:: Lifetime Lost Energy per Device= \sum_{years}^{}Annual Lost Energy per Device

    .. math:: Array Monthly Lost Energy = \sum_{devices}^{}Monthly Lost Energy per Device

    .. math:: Array Annual Lost Energy = \sum_{devices}Annual Lost Energy per Device

    .. math:: Array Lifetime Lost Energy = \sum_{devices} Lifetime Lost Energy per Device

    .. math:: Net Energy = Gross Energy - Lost Energy

The general definition of Net Energy is the one in Eq. 25.

As a function of the level of aggregation and the reference period, the following quantities can be calculated: (Eq. 26 to Eq. 32)

    .. math:: Monthly Net Energy per Device = Monthly Gross Energy per Device - Monthly Lost Energy per Device 

    .. math:: Annual Net Energy per Device = Annual Gross Energy per Device - Annual Lost Energy per Device

    .. math:: Lifetime Net Energy per Device = Lifetime Gross Energy per Device - Lifetime Lost Energy per Device

    .. math:: Array Monthly Net Energy = Array Monthly Gross Energy - Array Monthly Lost Energy

    .. math:: Array Annual Net Energy = Array Annual Gross Energy - Array Annual Lost Energy

    .. math:: Array Annual Net Energy  = Array Annual Gross Energy - Array Annual Lost Energy

    .. math:: Array Lifetime Net Energy = Array Lifetime Gross Energy - Array Lifetime Lost energy

    .. math:: Lost Energy ratio=\frac{Lost Energy}{Gross Energy}

In general, Lost Energy Ratio is the ratio between lost energy and gross energy.

As a function of the level of aggregation and the reference period, the following quantities can be calculated: (Eq. 33 to Eq. 39)

    .. math:: Monthly Lost Energy Ratio per Device=\frac{Monthly Lost Energy per Device}{Monthly Gross Energy per Device}
    
    .. math:: Annual Lost Energy Ratio per Device= \frac{Annual Lost Energy per Device}{Annual Gross Energy per Device }

    .. math:: Lifetime Lost Energy Ratio per Device= \frac{Lifetime Lost Energy per Device}{Lifetime Gross Energy per Device}

    ..math:: Array Monthly Lost Energy  Energy =\frac{Array Monthly Lost Energy}{Array Monthly Gross Energy}

    .. math:: Array Annual Lost Energy  Energy =\frac{Array Annual Lost Energy}{Array Annual Gross Energy}

    .. math:: Array Lifetime Lost Energy  Energy =\frac{Array Lifetime Lost Energy}{Array Lifetime Gross Energy}

    .. math:: Net Energy ratio=\frac{Net Energy}{Gross Energy}

Net Energy Ratio is the ratio between lost energy and gross energy.

As a function of the level of aggregation and the reference period, the following quantities can be calculated: (Eq. 40 to Eq.45)

    ..math:: Monthly Net Energy Ratio per Device=\frac{Monthly Net Energy per Device}{Monthly Gross Energy per Device}

    .. math:: Annual Net Energy Ratio per Device= \frac{Annual Net Energy per Device}{Annual Gross Energy per Device}

    .. math:: Lifetime Net Energy Ratio per Device=\frac{Lifetime Net Energy per Device}{Lifetime Gross Energy per Device}

    .. math:: Array Monthly Net Energy  Ratio =\frac{Array Monthly Net Energy}{rray Monthly Gross Energy}

    .. math:: Array Annual Net Energy  Ratio =\frac{Array Annual Net Energy}{Array Annual Gross Energy}

    .. math:: Array Lifetime Net Energy  Ratio =\frac{Array Lifetime Net Energy}{Array Lifetime Gross Energy


**IMPACT**

The outputs of the Energy Production assessment tool in SPEY will inform the user about:

    - The capacity of the plant to deliver energy onshore in case of no downtime (gross energy).

    - The estimated energy lost each month, each year and during the whole lifetime of the plant accounting for downtime (net energy).

    - The estimated energy delivered each month, each year and during the whole lifetime of the plant accounting for downtime (net energy).

    - The impact of the lost energy due to downtime with respect to the potential energy to be delivered (lost energy ratios).

    - The effectiveness of the operations and the estimation of the net energy with respect to the potential energy to be delivered (net energy ratios).
