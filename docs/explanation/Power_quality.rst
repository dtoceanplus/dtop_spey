.. _Power_quality:

Power Quality
======================

An estimatation of the active power production with respect to the reactive one can be estimated for different subsystems and levels of aggregation.

The Power Quality assessments of the SPEY module aim at providing the user with an estimation of the power quality, expressed in terms of phase between active and reactive power at the generator (for device and at array level) and at the onshore landing point.

**INPUTS, MODELS AND OUTPUTS**

The inputs needed for carrying out the assessment in terms of Power Quality are the table below.

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _INPUTS FOR THE EVALUATION OF THE POWER QUALITY

   * - ID
     - Brief Description of the Input Quantity
     - Origin of the Data
     - Data Model in SPEY
     - Units
   * - DATP
     - Active Transformed Power per device: the amount of active power at energy transformation phase per device per sea state
     - User/ET
     - Pandas table, columns are the devices and rows are the sea states (ordered by Sea State)
     - kW
   * - DRTP
     - Reactive Transformed Power per device: the amount of reactive power at energy transformation phase per device per sea state
     - User/ET
     - Pandas table, columns are the devices and rows are the sea states (ordered by Sea State)
     - kW
   * - ADP
     - Active Delivered Power of the array: the amount of active power at energy delivery phase of the array per sea state
     - User/ED
     - Pandas table, just one column (the total array) and rows are the sea states
     - kW
   * - RDP
     - Reactive Delivered Power of the array: the amount of active power at energy delivery phase of the array per sea state
     - User/ED
     - Pandas table, just one column (the total array) and rows are the sea states
     - kW

The Power Quality functions can compute the phase between active and reactive power at the generator (transformation level) for device and array level and at the onshore landing point at array level of aggregation for different sea states. 

The following quantities will be estimated: (Eq.71 to Eq.73):

	.. math:: Phase at generator per Device=\frac{DATP}{√(DATP^2+DRTP^2 )}

    .. math:: Phase at generator per Array Level=\frac{\sum_{devices}^{}DATP}{√(\sum_{devices}^{}DATP^2+\sum_{devices}^{}DRTP^2)}

    .. math:: Phase at onshore landing point per Array Level=\frac{ADP}{√(ADP^2+RDP^2 )}

**IMPACT**

The information about the power quality levels per sea state at generator and/or at the onshore landing point could be used to assess of the quality of the delivered energy at different stages of the generation of energy and identify eventually compliances with grid codes and eventually improve the design.

