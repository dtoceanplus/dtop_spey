.. _Alternative_metrics:

Alternative metrics
======================

 A set of dimensional parameters expressing how well the overall system, as well as the different sub-subsystems, perform with respect to the other parameters, as for example the lease area, the wetted surface of the prime mover, the mass, the rated power of the device, the characteristic length, and the length of the cabling, both at array and at device level of aggregation.

The Alternative Metrics assessments of the SPEY module aim at:

    - Provide the user with a certain number of dimensional parameters.
    - Assess the performances of the plant (both at array and at device level) with respect to main design characteristics of the plant (lease area extension, rated power, mass of the prime mover, wetted surface of the prime mover, cable length). 

**INPUTS, MODELS AND OUTPUTS**

The inputs needed for carrying out the assessment in terms of Alternative Metrics are presented in the following table:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _INPUTS FOR THE EVALUATION OF THE ENERGY PRODUCTION

   * - ID
     - Brief Description of the Input Quantity
     - Origin of the Data
     - Data Model in SPEY
     - Units
   * - CL
     - Level of Complexity
     - User
     - Number, Integer
     - -
   * - AEF
     - Average Annual Energy Flux Resource available in the site
     - SC
     - Number, Float
     - kWh/(m) WAVE; kWh/(m2) TIDAL
   * - TT
     - Technology Type, Tidal or Wave Energy Device
     - User/MC
     - String
     - “Wave” or “Tidal”
   * - CD
     - Characteristic dimension of the Ocean Energy Absorber: in case of Tidal energy device, the rotor diameter has been considered
     - User/MC
     - Number, float
     - m
   * - ND
     - Number of devices
     - User/EC
     - Number, Integer
     - -
   * - RP
     - Rated Power of the Device
     - User/MC
     - Number, Float
     - kW
   * - WS
     - Wetted Surface of the Ocean Energy converter
     - User/MC
     - Number, float
     - m2
   * - PMM
     - Prime Mover Mass
     - User/MC
     - Number, float
     - kg
   * - ECL
     - Export Cable Length
     - User/ED
     - Number, float
     - m
   * - IACL
     - Total Length of the intra-array cable system
     - User/ED
     - Number, float
     - m
   * - LA
     - Lease Area excluding No-Go Areas
     - User/SC
     - Number, float
     - km2
   * - ACE
     - Annual Captured Energy: the total amount of energy captured in one year
     - User/EC
     - Number, Float
     - kWh
   * - DACE
     - Annual Captured Energy per device: the total amount of energy captured in one year per device
     - User/EC
     - List of Numbers, Float
     - kWh
   * - ATE
     - Annual Transformed Energy: the total amount of energy transformed in one year
     - User/ET
     - Number, Float
     - kWh
   * - DATE
     - Annual Transformed Energy per device: the total amount of energy transformed in one year per device
     - User/ET
     - List of Numbers, Float
     - kWh
   * - ADE
     - Annual Delivered Energy: the total amount of energy delivered in one year.
     - user/ED
     - Number, Float
     - kWh

The functions computing Alternative Metrics in the SPEY module can be grouped as a function of the normalising factors. 

Therefore, they can be classified in five groups: 

1) Wetted Area Parameters: the energy production at different stages of the production chain (captured energy, transformed energy, delivered energy) and at different levels of aggregation (array, device) is calculated with respect to the wetted surface of the prime mover. The following metrics could be assessed (Eq.46 to Eq.50):
	
    .. math:: DACE Wetted Area Parameter=\frac{DACE}{WS}

    .. math:: ACE Wetted Area Parameter=\frac{ACE}{ND\cdot WS}

    .. math:: DATE Wetted Area Parameter=\frac{DATE}{WS}

    .. math:: ATE Wetted Area Parameter= \frac{ATE/}{ND*WS}

    .. math:: ADE Wetted Area Parameter=\frac{ADE}{ND\cdot WS}


2) Mass Parameters: the energy production at different stages of the production chain (captured energy, transformed energy, delivered energy) and at different levels of aggregation (array, device) is calculated with respect to the mass of the prime mover. Moreover, also the ratio between the rated power and the mass of the prime mover is calculated. The following metrics could be assessed (Eq.51 to Eq.56):
	
    .. math:: DACE Mass Parameter=\frac{DACE}{PMM}

    .. math:: ACE Mass Parameter=\frac{ACE}{ND\cdot PMM}

    .. math:: DATE Mass Parameter=\frac{DATE}{PMM}

    .. math:: ATE Mass Parameter=\frac{ATE}{ND\cdot PMM}

    .. math:: ADE Mass Parameter=\frac{ADE}{ND\cdot PMM}

    .. math:: Power to Mass Ratio=\frac{RP}{PMM}


3) Capture Width and Capture Width parameters: the capture length is calculated for wave energy devices and a set of associated parameters could be estimated as well. Moreover, a definition of capture length for tidal energy converters has been introduced, as the equivalent diameter of the rotor, given the captured energy the average energy flux in the site. The following metrics could be assessed (Eq.57 to Eq.64):

Eq. 57,58 WAVE; Eq. 59,60 TIDAL

    .. math:: CW (Device)=\frac{DACE}{AEF*365.25*24}

    .. math:: CW (Array)=\frac{ACE}{AEF*365.25*24}

    .. math:: CW(Device)=√(\frac{4\cdot DACE}{π\cdot AEF\cdot 365.25\cdot 24})

    .. math:: CW(array)=√(\frac{4\cdot ACE}{π\cdot AEF\cdot 365.25\cdot 24})

    .. math:: CW Ratio (Device)=\frac{CW (device)}{CD}

    .. math:: CWRatio (array)=\frac{CW (array)}{ND\cdot CD}

    .. math:: CW Ratio Rated Power (device)=\frac{CW (device)}{RP}

    .. math:: CW Ratio (array)=\frac{CW (array)}{ND\cdot RP}

4) Cable Length Parameters: the length of the export cable (ECL), of the intra-array cables (IACL) and of the whole cable system is calculated with respect to the rated power. The following metrics could be assessed (Eq.65 to Eq.67):
	
    .. math:: Intra Array Cable Ratio=\frac{IACL}{RP\cdot ND}

    .. math:: Export Cable Ratio=\frac{ECL}{RP\cdot ND}

    .. math:: Total Cable Ratio=\frac{IACL+ECL}{RP\cdot ND}

5) Lease Area Parameters: the energy production at different stages of the production chain (captured energy, transformed energy, delivered energy) and at array level of aggregation is calculated with respect to the extension of the lease area excluding no-go areas. The following metrics could be assessed (Eq.68 to Eq.70):
	
    .. math:: ACE Lease Area Parameter=\frac{ACE}{LA}

    .. math:: ATE Lease Area Parameter=\frac{ATE}{LA}

    .. math:: ADE Lease Area Parameter=\frac{ADE}{LA}

**IMPACT**

The outputs of the Alternative Metrics assessment tool in SPEY will inform the user about how the system (captured energy, length of cables, etc…) performs with respect to the mass, wetted surface, rated power, characteristic dimension. Moreover, users can take advantage of a novel definition for capture width applied to tidal energy converters. 

All this set of metrics are useful indicators to quickly compare different projects and scenarios and they can capture quickly the adequacy of the performances of a certain subsystem with respect to some characteristic of the design.
