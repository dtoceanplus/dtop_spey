.. _Efficiency:

Efficiency
===========

A set of dimensionless parameters expressing how well the overall system, as well as the different systems, perform  with respect to the available resource and the other subsystems, both at array and at device level of aggregation.

The assessments in terms of Efficiency that the SPEY module can compute aim at providing to the user with a set of dimensionless quantifiable parameters that show how:

- the overall designed system performs with respect the available resource;
- each subsystem (energy capture, transformation, delivery) performs, at array and device level, with respect to the available resource and with respect other subsystems.

It is important to notice that differences in efficiency per devices are expected due to the hydrodynamic interactions among devices themselves. In the Efficiency assessments no operations or downtime are included in the computations, supposing that the subsystems are working during all the time. This approach was chosen to point out the efficiency of the technology, without any bias due to downtime or reliability.

**Inputs, Models And Outputs**

The inputs needed for carrying out the assessment of the efficiency are in following table:

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _INPUTS FOR THE EVALUATION OF THE EFFICIENCY

   * - ID
     - Brief Description of the Input Quantity
     - Origin of the Data
     - Data Model in SPEY
     - Units
   * - CL
     - Level of Complexity
     - User
     - Number, Integer
     - -
   * - AEF
     - Average Annual Energy Flux Resource available in the site
     - User/SC
     - Number, Float
     - kW/(m) WAVE; kWh/(m2) TIDAL
   * - TT
     - Technology Type, Tidal or Wave Energy Device
     - User/MC
     - String
     - “Wave” or “Tidal”
   * - CD
     - Characteristic dimension of the Ocean Energy Absorber: in case of Tidal energy device, the rotor diameter has been considered
     - User/MC
     - Number, Float
     - m
   * - ND
     - Number of devices
     - User/EC
     - Number, Integer
     - -
   * - RP
     - Rated Power of the Device
     - User/MC
     - Number, Float
     - kW
   * - ACE
     - Annual Captured Energy: the total amount of energy captured in one year
     - User/EC
     - Number, Float
     - kWh
   * - DACE
     - Annual Captured Energy per device: the total amount of energy captured in one year per device
     - EC
     - List of Numbers, Float
     - kWh
   * - ATE
     - Annual Transformed Energy: the total amount of energy transformed in one year
     - User/ET
     - Number, Float
     - kWh
   * - DATE
     - Annual Transformed Energy per device: the total amount of energy transformed in one year per device
     - User/ET
     - List of Numbers, Float
     - kWh
   * - ADE
     - Annual Delivered Energy: the total amount of energy delivered in one year.
     - User/ED
     - Number, Float
     - kWh

The assessments that the efficiency class in SPEY can compute are the following:

Rated Flux The ratio between the capacity of the machine (rated power) and the available resource. 
    
In order to keep the metrics dimensionless, Eq .1 refers to wave energy converters and Eq. 2 to Tidal energy Converter.
    
    .. math:: RatedFlux=\frac{RP}{AEF\cdot CD}
    
    .. math:: RatedFlux=\frac{RP}{AEF\cdot π\frac{CD^2}{4}}

This could be expressed as: (Eq.3) (Eq.4). Array Captured Efficiency and Device Captured Efficiency are respectively the ratio between Annual Array Captured Energy and Rated Power of the Array and the ratio between Annual Device Captured Energy and Rated Power of the Device.

    .. math:: Captured Efficiency (Array)=  \frac{ACE}{ND*RP*365*24}

    .. math:: Captured Efficiency (Device)= \frac{DACE}{RP*365*24}

Absolute Array and Device Transformation Efficiency, defined respectively as the ratio between Annual Array Transformed Energy and Rated Power of the Array and the ratio between Annual Device Transformed Energy and Rated Power of the Device (Eq.5) (Eq.6).
    
    .. math:: Absolute Transformed Efficiency (Array) =  \frac{ATE}{ND*RP*365*24}

    .. math:: Absolute Transformed Efficiency (Device) =  \frac{DATE}{ND*RP*365*24}

Relative Array and Device Transformation Efficiency, defined respectively as the ratio between Annual Array Transformed Energy and Annual Array Captured Energy and the ratio between Annual Device Transformed Energy and Annual Device Captured Energy (Eq.7) (Eq.8).

    .. math:: Relative Transformed  Efficiency (Array)=  \frac{ATE}{ACE}

    .. math:: Relative Transformed  Efficiency (Device)=  \frac{DATE}{DACE}

Absolute Array Delivery Efficiency, defined as the ratio between Annual Array Delivered Energy and Rated Power of the Array (Eq. 9)

	.. math:: Absolute Delivered  Efficiency (Array)=  \frac{ADE}{ND*RP*365*24}

Relative Array Delivery Efficiency, defined respectively as the ratio between Annual Array Delivered Energy and Annual Array Transformed Energy (Eq. 10)

	.. math:: Relative Delivered  Efficiency (Array) =  \frac{ADE}{ATE}


**IMPACT**

The Efficiency assessments can be used to:

    - Identify if the machine is well dimensioned for the available resource in the site (Rated Flux)

    - Identify which stages of the transformation of energy are less efficient and may require a re-design of the subsystem (relative efficiencies).

    - Identify how each subsystem relate to the available resource (absolute efficiencies).





