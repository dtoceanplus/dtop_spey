.. _spey-home:

System Performance and Energy Yield
===================================

Introduction 
------------

The System Performance and Energy Yield SPEY tools belong to the suite of tools “DTOceanPlus” developed within the EU-funded project DTOceanPlus.

It assesses  the performance of the system in terms of energy yield during all the stages of the resource-to-wire conversion, including the downtime of the system. SPEY also computes the efficiencies at the different stages of the transformation, assesses the power quality at the delivery point, and produces a set of alternative metrics against a set of technical parameters.


In particular, SPEY can be used to:

   - Compute several dimensionless (efficiency) and dimensional (alternative metrics) parameters, given the technical design of the ocean energy plant and the power production of the different subsystems, at different level of aggregation (array and device level) and facilitate the visualisation of these outputs to the user.

   - Estimate the Energy Production at different level of aggregation (array and device level) accounting for the probabilistic distribution of the downtime throughout the life of the project, within different timescales (lifetime of the plant, annual and monthly energy production) and facilitate the visualisation of these outputs to the user.

   - Show results in terms of Power quality (Reactive vs Active power to the grid and as outputs per device) obtained by technical modules.


The main outputs of this module (computed both at array and device level) are:

   - A set of dimensionless metrics (efficiencies).

   - A set of dimensional metrics (Alternative Metrics) as a function of cable lengths, mass, rated power, and other characteristic dimensions.

   - Estimation of the power quality delivery per sea state.

   - Estimation of the net (monthly, yearly, lifetime) Energy Production, accounting for the downtime of the system.


Structure
---------

This module's documentation is divided into five main sections:

- :ref:`spey-tutorials` to give step-by-step instructions on using SPEY for new users. 

- :ref:`spey-how-to` that show how to achieve specific outcomes using SPEY; 

- A section on :ref:`background, theory and calculation methods <spey-explanation>` that describes how SPEY works and aims to give confidence in the tools;

- The :ref:`API reference <spey-reference>` section documents the code of modules, classes, API, and GUI. 


.. toctree::
   :hidden:
   :maxdepth: 2

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Workflow for using the SPEY module
----------------------------------

The workflow for using the System Performance and energy Yield Module can be summarised as 
1) create a study, 2) provide inputs, 2) run the assessment, and 3) view the results, as shown next. 

PICTURE HERE OF WORKFLOW (to be done 25/03/2021)

Overview of SPEY data requirements
----------------------------------
This section summarises the types of input data required to run the System Performance and Energy Yield module. Full details and data specifications are given in the how to guide on preparing data.

The required and optional inputs to run the module are summarised in the table below.  Note that in integrated mode, all the required inputs will all come from other modules. The inputs have been grouped as for the modules they should come from in integrated mode. Of course, in standalone mode, they all come from the user.

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: _SUMMARY OF INPUTS

   * - Group of Inputs
     - Parameters
   * - Machine Characterisation
     - Technology, Rated Power, Mass, Wet area, Characteristic Length 
   * - Site Characterisation
     - Annual Average Energy Flux Tidal [(for tidal energy devices), Annual Average Energy Flux Wave (for wave energy devices), Lease area extension, Monthly Wave Scatter Diagram (for wave energy devices), Current Monthly Scenario (for tidal energy devices).
   * - Energy Capture
     - Number of devices, Array Annual Captured Energy Production, Array q-factor, Device Annual Captured Energy Production, Device q-factor
   * - Energy Transformation
     - Array Annual Transformed Energy Production, Device Annual Transformed Energy Production, Device Active Transformed Power, Device Reactive Transformed Power
   * - Energy Delivery
     - Array Annual Delivered Energy Production, Total Length of Cables, Export Cable Length, Onshore active Power per sea state, Onshore reactive Power per sea state
   * - Logistics and Marine Operation
     - Project Life, Downtime hours per device, per month, per year


