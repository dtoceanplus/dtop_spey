from dtop_spey import business
import pytest


def test_metrics_cpx():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 10.0, 500.0, 50.0, 50.0, lease_area=10.0
    )
    assert metrics.cpx == "Level of Complexity 1"


def test_metrics_inputs():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 10.0, 500.0, 50.0, 50.0, lease_area=10.0
    )

    inputs = metrics.get_inputs()
    assert inputs["Level of Complexity"] == "Level of Complexity 1"


def test_wetted_captured():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        50.0,
        50.0,
        25.0,
        [10.0, 13.0],
        lease_area=10.0,
    )
    metrics.wetted_area_parameters()

    assert metrics.array_captured_wetted == 0.1


def test_wetted_captured_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 50.0, 50.0, lease_area=10.0
    )
    metrics.wetted_area_parameters()

    assert metrics.array_captured_wetted == None


def test_wetted_captured_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 0.0, 50.0, 25.0, lease_area=10.0
    )
    with pytest.raises(ZeroDivisionError):
        metrics.wetted_area_parameters()


def test_wetted_captured_device_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        0.0,
        50.0,
        device_annual_captured_energy=[10.0, 13.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.wetted_area_parameters()


# def test_wetted_captured_none():
#     metrics = business.AlternativeMetrics.get_cpx(
#         "1", 100.0, "Wave", 1.0, 5.0, 0.5,10., 50.0, 25.0
#     )
#     with pytest.raises(TypeError):
#         metrics.wetted_area_parameters()


def test_wetted_captured_device():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        50.0,
        50.0,
        25.0,
        [10.0, 13.0],
        lease_area=10.0,
    )
    metrics.wetted_area_parameters()

    assert metrics.device_captured_wetted[0] == 0.2


def test_wetted_captured_device_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 50.0, 50.0, 25.0, lease_area=10.0
    )
    metrics.wetted_area_parameters()

    assert metrics.device_captured_wetted == None


def test_wetted_transformed_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        50.0,
        50.0,
        25.0,
        [10.0, 13.0],
        lease_area=10.0,
    )
    metrics.wetted_area_parameters()

    assert metrics.array_transformed_wetted == None


def test_wetted_transformed_device_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        0.0,
        50.0,
        device_annual_transformed_energy=[10.0, 13.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.wetted_area_parameters()


def test_wetted_transformed_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        0.0,
        50.0,
        annual_transformed_energy=10.0,
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.wetted_area_parameters()


def test_wetted_delivered_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        0.0,
        50.0,
        annual_delivered_energy=10.0,
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.wetted_area_parameters()


def test_mass_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 5.0, 0.0, 50.0, lease_area=10.0
    )
    with pytest.raises(ZeroDivisionError):
        metrics.mass_parameters()


def test_mass_device_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        5.0,
        0.0,
        device_annual_captured_energy=[10.0, 10.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.mass_parameters()


def test_mass_device_transformed_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        5.0,
        0.0,
        device_annual_transformed_energy=[10.0, 10.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.mass_parameters()


def test_mass_transf_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        5.0,
        0.0,
        annual_transformed_energy=10.0,
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.mass_parameters()


def test_mass_deliv_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        5.0,
        0.0,
        annual_delivered_energy=10.0,
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.mass_parameters()


def test_mass_delivered_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 5.0, 10.0, 50.0, lease_area=10.0
    )
    metrics.mass_parameters()
    assert metrics.array_delivered_mass == None


def test_PWR():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Tidal", 1.0, 5.0, 0.5, 5.0, 10.0, 50.0, lease_area=10.0
    )
    metrics.calculate_PWR()
    assert metrics.PWR == 0.05


def test_CL_tidal():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Tidal", 1.0, 5.0, 0.5, 5.0, 10.0, 50.0, lease_area=10.0
    )
    metrics.CL_parameters()
    assert metrics.array_CL == 0.008521956800134095


def test_CL():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 5.0, 10.0, 50.0, lease_area=10.0
    )
    metrics.CL_parameters()
    assert metrics.array_CL == 5.703855806525211e-05


def test_CL_captured_wave_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 5.0, 10.0, lease_area=10.0
    )
    metrics.CL_parameters()
    assert metrics.array_CL == None


def test_CL_captured_tidal_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Tidal", 1.0, 5.0, 0.5, 5.0, 10.0, lease_area=10.0
    )
    metrics.CL_parameters()
    assert metrics.array_CL == None


def test_CL_ratio():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 5.0, 10.0, 50.0, lease_area=10.0
    )
    metrics.CL_parameters()
    assert metrics.array_CL_ratio == 1.1407711613050422e-05


def test_CL_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 0.0, "Wave", 1.0, 5.0, 0.5, 5.0, 10.0, 50.0, lease_area=10.0
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_tidal_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 0.0, "Tidal", 1.0, 5.0, 0.5, 5.0, 10.0, 50.0, lease_area=10.0
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_zero_dim():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 10.0, "Wave", 0.0, 5.0, 0.5, 5.0, 10.0, 50.0, lease_area=10.0
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_tidal_zero_dim():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 10.0, "Tidal", 0.0, 5.0, 0.5, 5.0, 10.0, 50.0, lease_area=10.0
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_power_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 10.0, "Wave", 1.0, 5.0, 0.0, 5.0, 10.0, 50.0, lease_area=10.0
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_tidal_power_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 10.0, "Tidal", 1.0, 5.0, 0.0, 5.0, 10.0, 50.0, lease_area=10.0
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_zero_wave_device():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        0.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        5.0,
        10.0,
        device_annual_captured_energy=[10.0, 10.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_zero_tidal_device():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        0.0,
        "Tidal",
        1.0,
        5.0,
        0.5,
        5.0,
        10.0,
        device_annual_captured_energy=[10.0, 10.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_zero_wave_device_dim():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        10.0,
        "Wave",
        0.0,
        5.0,
        0.5,
        5.0,
        10.0,
        device_annual_captured_energy=[10.0, 10.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_zero_tidal_device_dim():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        10.0,
        "Tidal",
        0.0,
        5.0,
        0.5,
        5.0,
        10.0,
        device_annual_captured_energy=[10.0, 10.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_zero_wave_device_power():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        10.0,
        "Wave",
        1.0,
        5.0,
        0.0,
        5.0,
        10.0,
        device_annual_captured_energy=[10.0, 10.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_CL_zero_tidal_device_power():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        10.0,
        "Tidal",
        1.0,
        0.5,
        0.0,
        5.0,
        10.0,
        device_annual_captured_energy=[10.0, 10.0],
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.CL_parameters()


def test_cable_length_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.0,
        5.0,
        10.0,
        50.0,
        intra_array_cable_length=100.0,
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.cable_length_parameters()


def test_cable_length_export_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.0,
        5.0,
        10.0,
        50.0,
        export_cable_length=100.0,
        lease_area=10.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.cable_length_parameters()


def test_cable_length_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 10.0, 5.0, 10.0, 50.0, lease_area=10.0
    )
    metrics.cable_length_parameters()
    assert metrics.IA_ratio == None


def test_cable_length():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        5.0,
        10.0,
        50.0,
        intra_array_cable_length=100.0,
        lease_area=10.0,
    )
    metrics.cable_length_parameters()
    assert metrics.IA_ratio == 40.0


def test_lease_captured():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 50.0, 50.0, 25.0, [10.0, 13.0], lease_area=10
    )
    metrics.lease_area_parameters()

    assert metrics.array_captured_lease == 2.5


def test_lease_captured_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 50.0, 50.0, lease_area=10.0
    )
    metrics.lease_area_parameters()

    assert metrics.array_captured_lease == None


def test_lease_captured_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 10.0, 50.0, 25.0, lease_area=0.0
    )
    with pytest.raises(ZeroDivisionError):
        metrics.lease_area_parameters()


def test_lease_transformed():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        50.0,
        50.0,
        annual_transformed_energy=25.0,
        lease_area=10,
    )
    metrics.lease_area_parameters()

    assert metrics.array_transformed_lease == 2.5


def test_lease_transformed_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 50.0, 50.0, lease_area=10.0
    )
    metrics.lease_area_parameters()

    assert metrics.array_transformed_lease == None


def test_lease_transformed_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        10.0,
        50.0,
        annual_transformed_energy=25.0,
        lease_area=0.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.lease_area_parameters()


def test_lease_delivered():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        50.0,
        50.0,
        annual_delivered_energy=25.0,
        lease_area=10.0,
    )
    metrics.lease_area_parameters()

    assert metrics.array_delivered_lease == 2.5


def test_lease_delivered_none():
    metrics = business.AlternativeMetrics.get_cpx(
        "1", 100.0, "Wave", 1.0, 5.0, 0.5, 50.0, 50.0, lease_area=10.0
    )
    metrics.lease_area_parameters()

    assert metrics.array_delivered_lease == None


def test_lease_delivered_zero():
    metrics = business.AlternativeMetrics.get_cpx(
        "1",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        10.0,
        50.0,
        annual_delivered_energy=25.0,
        lease_area=0.0,
    )
    with pytest.raises(ZeroDivisionError):
        metrics.lease_area_parameters()
