from dtop_spey import business
import pytest


def test_efficiency_cpx():
    EFFICIENCY = business.Efficiency.get_cpx("3", 100.0, "Tidal", 1.0, 5.0, 0.5)
    assert EFFICIENCY.cpx == "Level of Complexity 3"


def test_efficiency_inputs():
    EFFICIENCY = business.Efficiency.get_cpx("3", 100.0, "Tidal", 1.0, 5.0, 0.5)
    a = EFFICIENCY.get_inputs()
    assert a["Level of Complexity"] == "Level of Complexity 3"


def test_efficiency_capt_eff_OK():
    EFFICIENCY = business.Efficiency.get_cpx("3", 100.0, "Wave", 1.0, 5.0, 0.5, 0.8)
    EFFICIENCY.captured_efficiency()
    assert EFFICIENCY.captured_eff == 3.650467716176135e-05


def test_efficiency_capt_eff_none():
    EFFICIENCY = business.Efficiency.get_cpx("3", 100.0, "Wave", 1.0, 5.0, 0.5)
    EFFICIENCY.captured_efficiency()
    assert EFFICIENCY.captured_eff == None


def test_efficiency_capt_eff_zero():
    EFFICIENCY = business.Efficiency.get_cpx("3", 100.0, "Wave", 1.0, 5.0, 0.0, 0.8)
    with pytest.raises(ZeroDivisionError):
        EFFICIENCY.captured_efficiency()


def test_efficiency_capt_eff_zero_device():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3", 100.0, "Wave", 1.0, 5.0, 0.0, device_annual_captured_energy=[10.0, 10.0]
    )
    with pytest.raises(ZeroDivisionError):
        EFFICIENCY.captured_efficiency()


def test_transformation_eff_none():
    EFFICIENCY = business.Efficiency.get_cpx("3", 100.0, "Wave", 1.0, 5.0, 0.5, 0.8)
    EFFICIENCY.transformed_efficiency()
    assert EFFICIENCY.transformed_rel_eff == None


def test_transformation_eff_zero():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3", 100.0, "Wave", 1.0, 5.0, 0.5, 0.0, annual_transformed_energy=10.0
    )
    with pytest.raises(ZeroDivisionError):
        EFFICIENCY.transformed_efficiency()


def test_efficiency_transf_eff_zero_device():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3", 100.0, "Wave", 1.0, 5.0, 0.0, device_annual_transformed_energy=[10.0, 10.0]
    )
    with pytest.raises(ZeroDivisionError):
        EFFICIENCY.transformed_efficiency()


def test_delivery_eff_none():
    EFFICIENCY = business.Efficiency.get_cpx("3", 100.0, "Wave", 1.0, 5.0, 0.5, 0.8)
    EFFICIENCY.delivered_efficiency()
    assert EFFICIENCY.delivered_abs_eff == None


def test_delivery_eff():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3", 100.0, "Wave", 1.0, 5.0, 0.5, annual_delivered_energy=0.4
    )
    EFFICIENCY.delivered_efficiency()
    assert EFFICIENCY.delivered_abs_eff == 1.8252338580880676e-05


def test_rated_flux():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3", 100.0, "Wave", 1.0, 5.0, 0.5, annual_delivered_energy=0.4
    )
    EFFICIENCY.estimate_rated_flux()
    assert EFFICIENCY.rated_flux == 0.005


def test_rated_flux_tidal():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3", 100.0, "Tidal", 1.0, 5.0, 0.5, annual_delivered_energy=0.4
    )
    EFFICIENCY.estimate_rated_flux()
    assert EFFICIENCY.rated_flux == 0.0063661977236758134307553505349


def test_device_captured_eff_OK():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        annual_captured_energy=0.4,
        device_annual_captured_energy=[0.1, 0.2, 0.1],
    )
    a = [0.2, 4.563084645220169e-05, 0.2]
    EFFICIENCY.captured_efficiency()
    assert EFFICIENCY.device_captured_eff[1] == a[1]


def test_device_transformed_eff_rel():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3",
        100.0,
        "Wave",
        1.0,
        5.0,
        0.5,
        annual_captured_energy=0.4,
        device_annual_captured_energy=[0.1, 0.2, 0.1],
        device_annual_transformed_energy=[0.05, 0.1, 0.05],
    )
    a = [0.5, 0.5, 0.5]
    EFFICIENCY.transformed_efficiency()
    assert EFFICIENCY.device_transformed_rel_eff[1] == a[1]


def test_device_transformed_eff_abs():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3",
        100.0,
        "Tidal",
        1.0,
        5.0,
        0.5,
        annual_captured_energy=0.4,
        device_annual_captured_energy=[0.1, 0.2, 0.1],
        device_annual_transformed_energy=[0.05, 0.1, 0.05],
    )
    a = [0.1, 0.2, 1.1407711613050422e-05]
    EFFICIENCY.transformed_efficiency()
    assert EFFICIENCY.device_transformed_abs_eff[2] == a[2]


def test_efficiency_delivered_eff_zero():
    EFFICIENCY = business.Efficiency.get_cpx(
        "3", 100.0, "Wave", 1.0, 5.0, 0.0, 0.8, annual_delivered_energy=10.0
    )
    with pytest.raises(ZeroDivisionError):
        EFFICIENCY.delivered_efficiency()
