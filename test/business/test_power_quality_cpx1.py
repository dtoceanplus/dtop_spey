from dtop_spey import business
import pytest
import pandas as pd
import numpy as np


def test_quality_cpx():
    quality = business.PowerQuality.get_cpx("1")
    assert quality.cpx == "Level of Complexity 1"


def test_quality_inputs():
    quality = business.PowerQuality.get_cpx("1")
    inputs = quality.get_inputs()
    assert inputs["Level of Complexity"] == "Level of Complexity 1"


def test_quality_transformed_none():
    quality = business.PowerQuality.get_cpx("1")
    quality.transformed_phase()

    assert quality.device_transformed_phase == None


def test_quality_delivered_none():
    quality = business.PowerQuality.get_cpx("1")
    quality.delivered_phase()
    assert quality.array_delivered_phase == None


def test_quality_transformed():
    transformed_active_power = {"Device1": [100.0, 0.0], "Device2": [200.0, 100.0]}
    transformed_reactive_power = {"Device1": [0.0, 50.0], "Device2": [100.0, 100.0]}
    SS = ["SS1", "SS2"]
    transformed_active_power_pd = pd.DataFrame(data=transformed_active_power, index=SS)
    transformed_reactive_power_pd = pd.DataFrame(
        data=transformed_reactive_power, index=SS
    )

    quality = business.PowerQuality.get_cpx(
        "1", transformed_active_power_pd, transformed_reactive_power_pd
    )
    quality.get_inputs()
    quality.transformed_phase()
    assert quality.device_transformed_phase["Device1"]["SS1"] == 1.0


def test_quality_transformed_array():
    transformed_active_power = {"Device1": [100.0, 0.0], "Device2": [200.0, 100.0]}
    transformed_reactive_power = {"Device1": [0.0, 50.0], "Device2": [100.0, 100.0]}
    SS = ["SS1", "SS2"]
    transformed_active_power_pd = pd.DataFrame(transformed_active_power, SS)
    transformed_reactive_power_pd = pd.DataFrame(
        transformed_reactive_power, SS
    )

    quality = business.PowerQuality.get_cpx(
        "1", transformed_active_power_pd, transformed_reactive_power_pd
    )
    quality.get_inputs()
    quality.transformed_phase()
    assert quality.array_transformed_phase["SS2"] == 0.5547001962252291


def test_quality_delivered():
    delivered_active_power = {"Array": [200.0, 100.0]}
    delivered_reactive_power = {"Array": [100.0, 100.0]}
    SS = ["SS1", "SS2"]
    delivered_active_power_pd = pd.DataFrame(data=delivered_active_power, index=SS)
    delivered_reactive_power_pd = pd.DataFrame(data=delivered_reactive_power, index=SS)

    quality = business.PowerQuality.get_cpx(
        "1",
        delivered_active_power=delivered_active_power_pd,
        delivered_reactive_power=delivered_reactive_power_pd,
    )
    quality.get_inputs()
    quality.delivered_phase()
    assert quality.array_delivered_phase["Array"]["SS1"] == 0.8944271909999159
