from dtop_spey import business
import pandas as pd
import pytest


def test_quality_not_found():
    delivered_active_power = {"Array": [200.0, 100.0]}
    delivered_reactive_power = {"Array": [100.0, 100.0]}
    SS = ["SS1", "SS2"]
    delivered_active_power_pd = pd.DataFrame(data=delivered_active_power, index=SS)
    delivered_reactive_power_pd = pd.DataFrame(data=delivered_reactive_power, index=SS)

    with pytest.raises(AssertionError):
        business.PowerQuality.get_cpx(
            "4",
            delivered_active_power=delivered_active_power_pd,
            delivered_reactive_power=delivered_reactive_power_pd,
        )


def test_alternative_metrics_not_found():
    with pytest.raises(AssertionError):
        business.AlternativeMetrics.get_cpx(
            "4", 100.0, "Wave", 1.0, 5.0, 0.5, 50.0, 50.0, 25.0, [10.0, 13.0]
        )


def test_efficiency_not_found():
    with pytest.raises(AssertionError):
        business.Efficiency.get_cpx("4", 100.0, "Tidal", 1.0, 5.0, 0.5)


def test_energy_prod_not_found():
    occurrence = [0.2, 0.2, 0.3, 0.1, 0.2]
    bins = [1.0, 2.0, 3.0, 4.0, 5.0]
    power = [30.0, 40.0, 20.0, 50.0, 10.0]
    monthly_resource_histogram = {
        "bins": bins,
        "January": occurrence,
        "February": occurrence,
        "March": occurrence,
        "April": occurrence,
        "May": occurrence,
        "June": occurrence,
        "July": occurrence,
        "August": occurrence,
        "September": occurrence,
        "October": occurrence,
        "November": occurrence,
        "December": occurrence,
    }

    power_delivery_histogram = {"bins": bins, "power": power}

    number_devices = 3
    project_life = 15

    with pytest.raises(AssertionError):
        business.EnergyProduction.get_cpx(
            "4",
            monthly_resource_histogram,
            power_delivery_histogram,
            number_devices,
            project_life,
        )
