import atexit

import pytest
from pact import Consumer, Like, Provider, Term, EachLike

from dtop_spey.service import create_app


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


pact = Consumer("spey").has_pact_with(Provider("et"), port=1235)
pact.start_service()
atexit.register(pact.stop_service)

# @pytest.mark.skip()
def test_et_devices(client):
    pact.given("et 1 exists and has devices").upon_receiving(
        "a request for devices"
    ).with_request(
        "GET", Term(r"/energy_transf/\d+/devices", "/energy_transf/1/devices")
    ).will_respond_with(
        200,
        body=EachLike(
            {
                "Dev_E_Grid": {"value": 1.5},
                "Dev_Pa_Grid": {"value": {"Power": [1.5]}},
                "Dev_Pr_Grid": {"value": {"Power": [1.5]}},
            }
        ),
    )

    with pact:
        response = client.post(
            "/et-devices", json={"et_devices": f"{pact.uri}/energy_transf/1/devices"}
        )
        assert response.status_code == 201


# @pytest.mark.skip()
def test_et_farm(client):
    pact.given("et 1 exists and has farm").upon_receiving(
        "a request for farm"
    ).with_request(
        "GET", Term(r"/energy_transf/\d+/array", "/energy_transf/1/array")
    ).will_respond_with(
        200, body=Like({"Array_E_Grid": {"value": 1.5}})
    )

    with pact:

        response = client.post(
            "/et-farm", json={"et_farm": f"{pact.uri}/energy_transf/1/array"}
        )
        assert response.status_code == 201
