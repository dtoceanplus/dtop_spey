import atexit

import pytest
from pact import Consumer, Like, Provider, Term, EachLike

from dtop_spey.service import create_app


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


pact = Consumer("spey").has_pact_with(Provider("mc"), port=1237)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_mc_machine(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("mc 1 exists ").upon_receiving("a request for machine").with_request(
        "GET", Term(r"/mc/\d", "/mc/1")
    ).will_respond_with(200, body=Like({"type": "WEC"}))

    with pact:
        response = client.post("/mc-machine", json={"mc_machine": f"{pact.uri}/mc/1"})
        assert response.status_code == 201


# @pytest.mark.skip()
def test_mc_general(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("mc 1 exists and it has general").upon_receiving(
        "a request for general"
    ).with_request("GET", Term(r"/mc/\d+/general", "/mc/1/general")).will_respond_with(
        200, body=Like({"rated_capacity": 2.5})
    )

    with pact:
        response = client.post(
            "/mc-general", json={"mc_general": f"{pact.uri}/mc/1/general"}
        )
        assert response.status_code == 201


# @pytest.mark.skip()
def test_mc_dimensions(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("mc 1 exists and it has dimensions").upon_receiving(
        "a request for dimensions"
    ).with_request(
        "GET", Term(r"/mc/\d+/dimensions", "/mc/1/dimensions")
    ).will_respond_with(
        200, body=Like({"mass": 2.5, "wet_area": 2.5, "characteristic_dimension": 2.5,})
    )

    with pact:
        response = client.post(
            "/mc-dimensions", json={"mc_dimensions": f"{pact.uri}/mc/1/dimensions"}
        )
        assert response.status_code == 201
