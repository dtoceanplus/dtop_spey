import atexit

import pytest
from pact import Consumer, Like, Provider, Term, EachLike

from dtop_spey.service import create_app


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


pact = Consumer("spey").has_pact_with(Provider("ed"), port=1236)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_ed_results_cpx3(client):
    pact.given("ed 1 exists at cpx 3").upon_receiving(
        "a request for outputs"
    ).with_request(
        "GET",
        Term(
            r"/api/energy-deliv-studies/\d+/results",
            "/api/energy-deliv-studies/1/results",
        ),
    ).will_respond_with(
        200,
        body=Like(
            {
                "export_cable_total_length": 1.5,
                "array_cable_total_length": 1.5,
                "array_power_output": EachLike(1.5),
                "array_reactive_output": EachLike(1.5),
                "annual_yield": 1.5,
            }
        ),
    )

    with pact:

        response = client.post(
            "/ed-outputs",
            json={"ed_outputs": f"{pact.uri}/api/energy-deliv-studies/1/results"},
        )
        assert response.status_code == 201


# @pytest.mark.skip()
def test_ed_results_cpx1(client):
    pact.given("ed 2 exists at cpx 1").upon_receiving(
        "a request for outputs"
    ).with_request(
        "GET",
        Term(
            r"/api/energy-deliv-studies/\d+/results",
            "/api/energy-deliv-studies/1/results",
        ),
    ).will_respond_with(
        200,
        body=Like(
            {
                "export_cable_total_length": 1.5,
                "array_cable_total_length": 1.5,
                "array_power_output": EachLike(1.5),
                "array_reactive_output": EachLike(1.5),
                "annual_yield": 1.5,
            }
        ),
    )

    with pact:

        response = client.post(
            "/ed-outputs",
            json={"ed_outputs": f"{pact.uri}/api/energy-deliv-studies/1/results"},
        )
        assert response.status_code == 201
