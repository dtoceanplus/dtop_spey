import atexit

import pytest
from pact import Consumer, Like, Provider, Term, EachLike

from dtop_spey.service import create_app


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


pact = Consumer("spey").has_pact_with(Provider("ec"), port=1234)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_ec_devices(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("ec 1 exists and has device 2").upon_receiving(
        "a request for devices"
    ).with_request("GET", Term(r"/ec/\d+/devices", "/ec/1/devices")).will_respond_with(
        200, body=EachLike({"aep": 1.11, "q_factor": 0.86})
    )

    with pact:
        response = client.post(
            "/ec-devices", json={"ec_devices": f"{pact.uri}/ec/1/devices"}
        )
        assert response.status_code == 201


# @pytest.mark.skip()
def test_ec_farm(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1234)
    pact.given("ec 1 exists and has farm").upon_receiving(
        "a request for farm"
    ).with_request("GET", Term(r"/ec/\d+/farm", "/ec/1/farm")).will_respond_with(
        200, body=Like({"aep": 1.11, "q_factor": 0.86, "number_devices": 2})
    )

    with pact:

        response = client.post("/ec-farm", json={"ec_farm": f"{pact.uri}/ec/1/farm"})
        assert response.status_code == 201
