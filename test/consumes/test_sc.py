import atexit

import pytest
from pact import Consumer, Like, Provider, Term, EachLike

from dtop_spey.service import create_app


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


pact = Consumer("spey").has_pact_with(Provider("sc"), port=1239)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_sc_info(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("sc 1 exists and it has info").upon_receiving(
        "a request for sc"
    ).with_request(
        "GET", Term(r"/sc/\d+/point/info", "/sc/1/point/info")
    ).will_respond_with(
        200, body=Like({"surface": 1.5})
    )

    with pact:
        response = client.post(
            "/sc-info", json={"sc_info": f"{pact.uri}/sc/1/point/info"}
        )
        assert response.status_code == 201


# @pytest.mark.skip()
def test_sc_info(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("sc 1 exists and it has info").upon_receiving(
        "a request for sc"
    ).with_request(
        "GET", Term(r"/sc/\d+/point/info", "/sc/1/point/info")
    ).will_respond_with(
        200, body=Like({"surface": 1.5})
    )

    with pact:
        response = client.post(
            "/sc-info", json={"sc_info": f"{pact.uri}/sc/1/point/info"}
        )
        assert response.status_code == 201


# @pytest.mark.skip()
def test_flux_wave(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("sc 1 exists and it has wave flux").upon_receiving(
        "a request for sc"
    ).with_request(
        "GET",
        Term(
            r"/sc/\d+/point/statistics/waves/Basic/CgE",
            "/sc/1/point/statistics/waves/Basic/CgE",
        ),
    ).will_respond_with(
        200, body=Like({"mean": 1.5})
    )

    with pact:
        response = client.post(
            "/sc-flux-wave",
            json={"sc_flux_wave": f"{pact.uri}/sc/1/point/statistics/waves/Basic/CgE"},
        )
        assert response.status_code == 201


# @pytest.mark.skip()
def test_flux_current(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("sc 1 exists and it has current flux").upon_receiving(
        "a request for sc"
    ).with_request(
        "GET",
        Term(
            r"/sc/\d+/point/statistics/currents/Basic/Flux",
            "/sc/1/point/statistics/currents/Basic/Flux",
        ),
    ).will_respond_with(
        200, body=Like({"mean": 1.5})
    )

    with pact:
        response = client.post(
            "/sc-flux-current",
            json={
                "sc_flux_current": f"{pact.uri}/sc/1/point/statistics/currents/Basic/Flux"
            },
        )
        assert response.status_code == 201


# @pytest.mark.skip()
def test_occurrence_wave(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("sc 1 exists and it has monthly wave EJPD").upon_receiving(
        "a request for sc"
    ).with_request(
        "GET",
        Term(
            r"/sc/\d+/point/statistics/waves/EJPD3v/hs_tp_dp_monthly_flattened",
            "/sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_monthly_flattened",
        ),
    ).will_respond_with(
        200,
        body=Like(
            {
                "pdf": [
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                ],
                "id": [
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                    EachLike(0.5),
                ],
            }
        ),
    )

    with pact:
        response = client.post(
            "/sc-occurrence-wave",
            json={
                "sc_occurrence_wave": f"{pact.uri}/sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_monthly_flattened"
            },
        )
        assert response.status_code == 201


def test_occurrence_currents(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("sc 1 exists and it has monthly current probability").upon_receiving(
        "a request for sc"
    ).with_request(
        "GET",
        Term(
            r"/sc/\d+/farm/scenarii_matrices/currentsmonthly",
            "/sc/1/farm/scenarii_matrices/currentsmonthly",
        ),
    ).will_respond_with(
        200,
        body=Like(
            {
                "p": Like(
                    {
                        "January": EachLike(0.5),
                        "February": EachLike(0.5),
                        "March": EachLike(0.5),
                        "April": EachLike(0.5),
                        "May": EachLike(0.5),
                        "June": EachLike(0.5),
                        "July": EachLike(0.5),
                        "August": EachLike(0.5),
                        "September": EachLike(0.5),
                        "October": EachLike(0.5),
                        "November": EachLike(0.5),
                        "December": EachLike(0.5),
                    }
                ),
                "id": Like(
                    {
                        "January": EachLike(0.5),
                        "February": EachLike(0.5),
                        "March": EachLike(0.5),
                        "April": EachLike(0.5),
                        "May": EachLike(0.5),
                        "June": EachLike(0.5),
                        "July": EachLike(0.5),
                        "August": EachLike(0.5),
                        "September": EachLike(0.5),
                        "October": EachLike(0.5),
                        "November": EachLike(0.5),
                        "December": EachLike(0.5),
                    }
                ),
            }
        ),
    )

    with pact:
        response = client.post(
            "/sc-occurrence-current",
            json={
                "sc_occurrence_current": f"{pact.uri}/sc/1/farm/scenarii_matrices/currentsmonthly"
            },
        )
        assert response.status_code == 201
