import atexit

import pytest
from pact import Consumer, Like, Provider, Term, EachLike

from dtop_spey.service import create_app


@pytest.fixture
def app():
    app = create_app({"TESTING": True})
    yield app


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


downtime_hours = {
    "project_life": Like(10),
    "n_devices": Like(2),
    "downtime": EachLike(
        {
            "device_id": Like("device11"),
            "downtime_table": Like(
                {
                    "year": EachLike(1),
                    "jan": EachLike(1.5),
                    "feb": EachLike(1.5),
                    "mar": EachLike(1.5),
                    "apr": EachLike(1.5),
                    "may": EachLike(1.5),
                    "jun": EachLike(1.5),
                    "jul": EachLike(1.5),
                    "aug": EachLike(1.5),
                    "sep": EachLike(1.5),
                    "oct": EachLike(1.5),
                    "nov": EachLike(1.5),
                    "dec": EachLike(1.5),
                }
            ),
        }
    ),
}


pact = Consumer("spey").has_pact_with(Provider("lmo"), port=1238)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_lmo(client):
    # pact = Consumer("spey").has_pact_with(Provider("ec"), port=1235)
    pact.given("lmo 1 exists and it has downtime").upon_receiving(
        "a request for lmo"
    ).with_request(
        "GET",
        Term(
            r"/api/\d+/phases/maintenance/downtime",
            "/api/1/phases/maintenance/downtime",
        ),
    ).will_respond_with(
        200, body=Like(downtime_hours)
    )

    with pact:
        response = client.post(
            "/lmo-downtime",
            json={"lmo_downtime": f"{pact.uri}/api/1/phases/maintenance/downtime"},
        )
        assert response.status_code == 201
