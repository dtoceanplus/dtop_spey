import pytest
import json

from dtop_spey import service
from dtop_spey.storage.models import *


def test_projects(client):
    clean_db(client)

    response_core = client.get("/api")

    response = client.get("/spey")
    projects = json.loads(response.data)
    assert type(projects) is list

    # def test_add_project(client):
    post_project = client.post("/spey", json=body)
    data_post_project = json.loads(post_project.data)
    assert data_post_project["message"] == "Assessment added"

    post_project_wave = client.post("/spey/1/inputs", json=project_body)
    data_post_project = json.loads(post_project_wave.data)
    assert data_post_project["message"] == "Assessment added"

    post_project_tid = client.post("/spey/1/inputs", json=project_body_tidal)
    assert post_project_tid.status_code == 400

    # def test_add_project(client):
    get_project = client.get("/spey/1")
    project_data = json.loads(get_project.data)
    assert project_data["title"] == "Trial"

    get_inputs = client.get("/spey/1/inputs")
    assert get_inputs.status_code == 200

    get_efficiency = client.get("/spey/1/efficiency")
    assert get_efficiency.status_code == 200

    get_pq = client.get("/spey/1/power-quality")
    assert get_pq.status_code == 200

    get_am = client.get("/spey/1/alternative-metrics")
    assert get_am.status_code == 200

    get_ep = client.get("/spey/1/energy-production")
    assert get_ep.status_code == 200

    put_project = client.put("/spey/1", json=body)
    data_put_project = json.loads(put_project.data)
    assert data_put_project["message"] == "project updated"

    # put_project_tid = client.put("/spey/1", json=project_body_tidal)
    # assert put_project_tid.status_code == 400

    put_project = client.put("/spey/-1", json=project_body)
    assert put_project.status_code == 400

    get_inputs_ko = client.get("/spey/-1/inputs")
    assert get_inputs_ko.status_code == 404

    get_efficiency_ko = client.get("/spey/-1/efficiency")
    assert get_efficiency_ko.status_code == 404

    get_pq_ko = client.get("/spey/-1/power-quality")
    assert get_pq_ko.status_code == 404

    get_am_ko = client.get("/spey/-1/alternative-metrics")
    assert get_am_ko.status_code == 404

    get_ep_ko = client.get("/spey/-1/energy-production")
    assert get_ep_ko.status_code == 404

    get_project_ko = client.get("/spey/-1")
    assert get_project_ko.status_code == 404

    delete_project_ko = client.delete("/spey/-1")
    assert delete_project_ko.status_code == 404

    project_body["device_characteristics_machine"] = project_body_tidal[
        "device_characteristics_machine"
    ]

    post_project = client.post("/spey", json=body)
    data_post_project = json.loads(post_project.data)
    assert data_post_project["message"] == "Assessment added"

    post_project_tidal = client.post("/spey/2/inputs", json=project_body)
    data_post_project = json.loads(post_project_tidal.data)
    assert data_post_project["message"] == "Assessment added"

    project_body["downtime_hours"] = {"url": "/api/2/results/downtime"}

    post_project = client.post("/spey", json=body)
    data_post_project = json.loads(post_project.data)
    assert data_post_project["message"] == "Assessment added"

    post_project_noDT = client.post("/spey/3/inputs", json=project_body)
    data_post_project = json.loads(post_project_noDT.data)
    assert data_post_project["message"] == "Assessment added"

    project_body.pop("ec_farm")
    project_body.pop("ec_devices")
    project_body.pop("et_results")
    project_body.pop("et_results_array")
    project_body.pop("ed_results")
    project_body.pop("downtime_hours")

    post_project = client.post("/spey", json=body)
    data_post_project = json.loads(post_project.data)
    assert data_post_project["message"] == "Assessment added"

    post_project_optional = client.post("/spey/4/inputs", json=project_body)
    data_post_project = json.loads(post_project_optional.data)
    assert data_post_project["message"] == "Assessment added"

    get_representation = client.get("/representation/1")
    assert get_representation.status_code == 200

    get_representation_ko = client.get("/representation/-1")
    assert get_representation_ko.status_code == 404


def clean_db(client):
    response = client.get("/spey")
    if response.status_code == 404:
        response = client.post("/spey", json=project_body)
        response = client.get("/spey")

    for project in response.json:
        response = client.delete(f'spey/{project["speyId"]}')
        assert response.status_code == 200


body = {"title": "Trial", "description": "Trying to make pytest working"}


project_body = {
    "scId": "User-Defined",
    "mcId": "User-Defined",
    "ecId": "User-Defined",
    "etId": "User-Defined",
    "edId": "User-Defined",
    "lmoId": "User-Defined",
    "device_characteristics_general": {"url": "/mc/1/general"},
    # "device_characteristics_general": {
    #     "floating": True,
    #     "connector_type": "Dry",
    #     "constant_power_factor": 0.32,
    #     "machine_cost": 1000000,
    #     "max_installation_water_depth": 100,
    #     "min_installation_water_depth": 30,
    #     "min_interdistance_x": 120,
    #     "min_interdistance_y": 20,
    #     "preferred_fundation_type": "Pile",
    #     "rated_capacity": 1000000,
    #     "rated_voltage": 1000,
    #     "materials": [
    #         {"material_name": "undefined", "material_quantity": 760839.76},
    #         {"material_name": "concrete", "material_quantity": 882211.2033092708},
    #     ],
    # },
    "device_characteristics_machine": {"url": "/mc/1"},
    # "device_characteristics_machine": {
    #     "complexity": 1,
    #     "date": "27-12-2019",
    #     "desc": "Simple project to test potential of Wave Energy Converter in the North Sea",
    #     "dimensions": 1,
    #     "general": 1,
    #     "id": 2,
    #     "model": 1,
    #     "status": 20,
    #     "tags": "wave",
    #     "title": "Test array",
    #     "type": "WEC",
    # },
    "device_characteristics_dimensions": {"url": "/mc/1/dimensions"},
    # "device_characteristics_dimensions": {
    #     "characteristic_dimension": 8,
    #     "draft": 3,
    #     "height": 10,
    #     "width": 10,
    #     "length": 70,
    #     "submerged_volume": 700,
    #     "wet_area": 1001,
    #     "wet_frontal_area": 10,
    #     "dry_frontal_area": 100,
    #     "beam_wet_area": 10,
    #     "mass": 700000,
    #     "footprint_radius": 50,
    # },
    "sc_farm": {"url": "/sc/1/point/info"},
    # "sc_farm": {
    #     "name": ["aaa", ["bbb"]],
    #     "latitude": [0.0],
    #     "longitude": [0.0],
    #     "bins": [1, 2, 3],
    #     "surface": 10.0,
    #     "description": "bbb",
    # },
    "wave_EJPD": {
        "url": "/sc/1/point/statistics/waves/EJPD3v/hs_tp_dp_monthly_flattened"
    },
    # "wave_EJPD": {
    #     "id": [[1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1]],
    #     "pdf": [[1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1], [1]],
    # },
    "current_scenarii": {"url": "/sc/1/farm/scenarii_matrices/currentsmonthly"},
    # "current_scenarii": {
    #     "id": {
    #         "January": [1, 2, 3],
    #         "February": [1, 2, 3],
    #         "March": [1, 2, 3],
    #         "April": [1, 2, 3],
    #         "May": [1, 2, 3],
    #         "June": [1, 2, 3],
    #         "July": [1, 2, 3],
    #         "August": [1, 2, 3],
    #         "September": [1, 2, 3],
    #         "October": [1, 2, 3],
    #         "November": [1, 2, 3],
    #         "December": [1, 2, 3],
    #     },
    #     "p": {
    #         "January": [0.5, 0.2, 0.3],
    #         "February": [0.5, 0.2, 0.3],
    #         "March": [0.5, 0.2, 0.3],
    #         "April": [0.5, 0.2, 0.3],
    #         "May": [0.5, 0.2, 0.3],
    #         "June": [0.5, 0.2, 0.3],
    #         "July": [0.5, 0.2, 0.3],
    #         "August": [0.5, 0.2, 0.3],
    #         "September": [0.5, 0.2, 0.3],
    #         "October": [0.5, 0.2, 0.3],
    #         "November": [0.5, 0.2, 0.3],
    #         "December": [0.5, 0.2, 0.3],
    #     },
    #     "name": "aaa",
    #     "description": "bbb",
    # },
    "wave_flux": {"url": "/sc/1/point/statistics/waves/Basic/CgE"},
    # "wave_flux": {
    #     "latitude": [0.0],
    #     "longitude": [0.0],
    #     "mean": 5.0,
    #     "min": [5.0],
    #     "max": [5.0],
    #     "median": [5.0],
    #     "std": [5.0],
    #     "name": "aaa",
    #     "description": "bbb",
    # },
    "current_flux": {"url": "/sc/1/point/statistics/currents/Basic/Flux"},
    # "current_flux": {
    #     "latitude": [0.0],
    #     "longitude": [0.0],
    #     "mean": 5.0,
    #     "min": [5.0],
    #     "max": [5.0],
    #     "median": [5.0],
    #     "std": [5.0],
    #     "name": "aaa",
    #     "description": "bbb",
    # },
    "ec_devices": {"url": "/ec/1/devices"},
    # "ec_devices": [
    #     {
    #         "easting": 0,
    #         "northing": 0,
    #         "q_factor": 0.98,
    #         "aep": 100001,
    #         "captured_power": 100002,
    #         "captured_power_per_condition": {
    #             "siteConditionID": [0, 1, 2],
    #             "capturedPower": [10001, 10001, 10001],
    #         },
    #     },
    #     {
    #         "easting": 1000,
    #         "northing": 0,
    #         "q_factor": 0.98,
    #         "aep": 100001,
    #         "captured_power": 100002,
    #         "captured_power_per_condition": {
    #             "siteConditionID": [0, 1, 2],
    #             "capturedPower": [10001, 10001, 10001],
    #         },
    #     },
    # ],
    "ec_farm": {"url": "/ec/1/farm"},
    # "ec_farm": {
    #     "layout": {"deviceID": [0, 1], "easting": [0, 10], "northing": [0, 10]},
    #     "number_devices": 2,
    #     "q_factor": 0.98,
    #     "aep": 100001,
    #     "captured_power": 100002,
    #     "captured_power_per_condition": {
    #         "siteConditionID": [0, 1, 2, 3],
    #         "capturedPower": [10001, 10001, 10001, 10001],
    #     },
    #     "resource_reduction": 0.97,
    # },
    "et_results": {"url": "/energy_transf/1/devices"},
    # "et_results": [
    #     {
    #         "ET id": 1,
    #         "Device id": 2,
    #         "PTOs per device": 2,
    #         "Control strategy": "Passive",
    #         "Mech transformation type": "Air Turbine",
    #         "Elect tranformation type": "SCIG",
    #         "Grid transformation type": "B2B2Level",
    #         "Shutdown flag": True,
    #         "Annual Transformed Energy": 500.0,
    #         "Environmental Conditions": {
    #             "Hs[m]": [1.2, 5.1, 0.8],
    #             "Tp[s]": [9.2, 12.5, 5.4],
    #         },
    #         "Device Grid Conditioned Power": {
    #             "grid_conditioning_P_act [W]": [97000.0, 117000.0, 10000.0],
    #             "grid_conditioning_P_react [W]": [1000.0, 100.0, 5000.0],
    #         },
    #     },
    #     {
    #         "ET id": 2,
    #         "Device id": 1,
    #         "PTOs per device": 2,
    #         "Control strategy": "Passive",
    #         "Mech transformation type": "Air Turbine",
    #         "Elect tranformation type": "SCIG",
    #         "Grid transformation type": "B2B2Level",
    #         "Shutdown flag": True,
    #         "Annual Transformed Energy": 500.0,
    #         "Environmental Conditions": {
    #             "Hs[m]": [1.2, 5.1, 0.8],
    #             "Tp[s]": [9.2, 12.5, 5.4],
    #         },
    #         "Device Grid Conditioned Power": {
    #             "grid_conditioning_P_act [W]": [97000.0, 117000.0, 10000.0],
    #             "grid_conditioning_P_react [W]": [1000.0, 100.0, 5000.0],
    #         },
    #     },
    # ],
    "et_results_array": {"url": "/energy_transf/1/array"},
    # "et_results_array": {
    #     "ET id": 2,
    #     "Array Transformed Energy": 50000,
    #     "Stage": "mid",
    #     "Technology": "Wave",
    #     "Number of devices": 25,
    # },
    "ed_results": {"url": "/api/energy-deliv-studies/1/results/1"},
    # "ed_results": {
    #     "study_id": 1,
    #     "network_id": 1,
    #     "array_cable_total_length": 1630.8326112068523,
    #     "export_cable_total_length": 1065.2400289945676,
    #     "power_histogram": [0.0, 0.0, 1.0],
    #     "array_power_output": [
    #         -0.1248018661436312,
    #         -0.37325971538757496,
    #         -0.620212160040956,
    #         -0.8656849243352749,
    #         -1.10970440107363,
    #         -1.3603471006583545,
    #         -1.6046126087239765,
    #         -1.8479600724539358,
    #         -2.090399697855183,
    #         -2.331941483866773,
    #     ],
    #     "array_reactive_output": [
    #         -0.09954507030499604,
    #         -0.2860063586435535,
    #         -0.4714328811318724,
    #         -0.6558422926283058,
    #         -0.8392527646978287,
    #         0.004074025863753938,
    #         0.008001690193204514,
    #         0.012560392944804821,
    #         0.017743118964597127,
    #         0.02354299520030967,
    #     ],
    #     "annual_yield": 20427807398.67293,
    # },
    "downtime_hours": {"url": "/api/1/phases/maintenance/downtime"}
    # "downtime_hours": {
    #     "project_life": 10,
    #     "n_devices": 2,
    #     "downtime": {
    #         "device11": {
    #             "year": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    #             "jan": [744.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #             "feb": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #             "mar": [0.0, 43.0, 0.0, 0.0, 0.0, 0.0, 432.0, 15.0, 0.0, 15.0],
    #             "apr": [0.0, 0.0, 0.0, 0.0, 0.0, 564.0, 0.0, 0.0, 0.0, 0.0],
    #             "may": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #             "jun": [0.0, 0.0, 196.0, 0.0, 124.0, 0.0, 532.0, 0.0, 0.0, 0.0],
    #             "jul": [0.0, 0.0, 0.0, 643.0, 0.0, 324.0, 0.0, 0.0, 0.0, 0.0],
    #             "aug": [56.0, 0.0, 0.0, 0.0, 56.0, 0.0, 56.0, 0.0, 0.0, 0.0],
    #             "sep": [0.0, 145.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #             "oct": [0.0, 0.0, 0.0, 88.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #             "nov": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #             "dec": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 159.0, 0.0, 0.0, 0.0],
    #         },
    #         "device12": {
    #             "year": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    #             "jan": [744.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #             "feb": [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #             "mar": [0.0, 43.0, 0.0, 0.0, 0.0, 0.0, 0.0, 15.0, 0.0, 0.0],
    #             "apr": [0.0, 0.0, 0.0, 0.0, 0.0, 532.0, 0.0, 0.0, 0.0, 180.0],
    #             "may": [0.0, 0.0, 0.0, 0.0, 0.0, 42.0, 0.0, 0.0, 0.0, 0.0],
    #             "jun": [0.0, 0.0, 0.0, 0.0, 124.0, 0.0, 0.0, 0.0, 0.0, 0.0],
    #             "jul": [0.0, 140.0, 0.0, 643.0, 0.0, 324.0, 0.0, 0.0, 0.0, 0.0],
    #             "aug": [56.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 720.0],
    #             "sep": [0.0, 0.0, 543.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 720.0],
    #             "oct": [0.0, 0.0, 0.0, 643.0, 0.0, 0.0, 120.0, 453.0, 0.0, 744.0],
    #             "nov": [0.0, 0.0, 0.0, 0.0, 47.0, 0.0, 0.0, 0.0, 0.0, 720.0],
    #             "dec": [0.0, 0.0, 0.0, 0.0, 96.0, 0.0, 0.0, 0.0, 0.0, 744.0],
    #         },
    #     },
    # },
}

project_body_tidal = {
    "device_characteristics_machine": {"url": "/mc/2"}
    # "device_characteristics_machine": {
    #     "complexity": 1,
    #     "date": "27-12-2019",
    #     "desc": "Simple project to test potential of Wave Energy Converter in the North Sea",
    #     "dimensions": 1,
    #     "general": 1,
    #     "id": 2,
    #     "model": 1,
    #     "status": 20,
    #     "tags": "wave",
    #     "title": "Test array",
    #     "type": "TEC",
    # }
}
