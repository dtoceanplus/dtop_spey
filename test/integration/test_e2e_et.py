"""
This is a simple test that checks that all necessary services are available
After some real tests involving communication with MC and SC will be written, this won't be needed
"""

import requests


def test_et_array():
    resp = requests.get("http://et.dto.test/energy_transf/1/array")
    assert resp.status_code == 200, resp.text


def test_et_devices():
    resp = requests.get("http://et.dto.test/energy_transf/1/devices")
    assert resp.status_code == 200, resp.text
