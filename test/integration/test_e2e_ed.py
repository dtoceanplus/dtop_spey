"""
This is a simple test that checks that all necessary services are available
After some real tests involving communication with MC and SC will be written, this won't be needed
"""

import requests


def test_ed_results():
    resp = requests.get("http://ed.dto.test/api/energy-deliv-studies/1/results/1")
    assert resp.status_code == 200, resp.text
