"""
This is a simple test that checks that all necessary services are available
After some real tests involving communication with MC and SC will be written, this won't be needed
"""

import requests


def test_lmo_downtime():
    resp = requests.get("http://lmo.dto.test/api/1/phases/maintenance/downtime")
    assert resp.status_code == 200, resp.text
