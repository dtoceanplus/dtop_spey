import os
import pytest

from dtop_spey.service import create_app
from dtop_spey.storage import db


@pytest.fixture
def app():
    app = create_app(
        {
            "TESTING": True,
            "SQLALCHEMY_DATABASE_URI": os.environ["DATABASE_URL"],
            "SERVER_NAME": "localhost.dev",
        }
    )

    with app.app_context():
        db.init_db()

    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()
