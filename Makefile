# # Minimal makefile for Sphinx documentation
# #

# # You can set these variables from the command line, and also
# # from the environment for the first two.
# SPHINXOPTS    ?=
# SPHINXBUILD   ?= sphinx-build
# SOURCEDIR     = .
# BUILDDIR      = _build

# # Put it first so that "make" without argument is like "make help".
# help:
# 	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# .PHONY: help Makefile

# # Catch-all target: route all unknown targets to Sphinx using the new
# # "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
# %: Makefile
# 	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# #--------------------------------------------

# ifeq ($(OS),Windows_NT)
# 	SHELL:=bash
# endif

impose:
	make -f ci.makefile spey-deploy-docker
	docker-compose run --rm contracts

impose-local:
	make -f ci.makefile spey-deploy
	docker-compose run --rm contracts

verify:
	make -f ci.makefile spey-deploy-docker
	docker-compose --project-name spey -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name spey -f verifiable.docker-compose stop spey
	docker cp `docker-compose --project-name spey -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify
