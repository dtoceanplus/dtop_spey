# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import requests
import copy
from flask import (
    Blueprint,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
    make_response,
)
import os

import json

import pandas as pd

from dtop_spey import business

from datetime import datetime

# from dtop_spey.storage.models.models import "class Assessment_Table(db.Model)"
from dtop_spey.storage.models.models import (
    db,
    Assessment_Table,
    Inputs_Table,
    Energy_Production,
    Alternative_Metrics,
    Efficiency,
    Power_Quality,
    Representation,
)

# from dtop_spey.storage.schemas.schemas import "schemas"
from dtop_spey.storage.schemas.schemas import (
    AssessmentSchema,
    InputsSchema,
    EnergyProductionSchema,
    AlternativeMetricsSchema,
    EfficiencySchema,
    PowerQualitySchema,
    RepresentationSchema,
)
from dtop_spey.storage import config

headers = {"Access-Control-Allow-Headers": "Content-Type"}


bp_spey = Blueprint("representation_bp", __name__)


@bp_spey.route("/representation/<speyId>", methods=["GET"])
def get_representation(speyId):
    """
        Get the DR of a Project
    """

    try:

        assessment = Assessment_Table.query.get(speyId)
        inputs = assessment.inputs_table
        assessment_representation = inputs.representation
        representation_schema = RepresentationSchema()
        assessment_representation_dict = representation_schema.dump(
            assessment_representation
        )

        if assessment:
            return make_response(assessment_representation_dict, 200, headers)

    except:
        return jsonify({"status": "fail", "message": "Unknown Reason"}), 404


# @bp_spey.route("/<int:project_id>", methods=("GET",))
# def get_representation(project_id: int):
#     try:
#         data = Representation.query.get(speyId)
#     return jsonify(projects[project_id])

#         if assessment:
#             return make_response(assessment_dict, 200, headers)
#         else:
#             return jsonify({"status": "fail", "message": assessment_dict}), 404
#         raise IndexError("The specified ID does not exist in the DB")
#     except IndexError as e:
#         return jsonify({"status": "fail", "message": str(e)}), 404
#     except:
#         return jsonify({"status": "fail", "message": "Unknown Reason"}), 404


# @bp_spey.route("/filters", methods=("GET",))
# def representation_filters():
#     # return make_response(jsonify({"paths": ["Device", "Project"]}), 200, headers)
#     return make_response(jsonify(["Device", "Project"]), 200, headers)


# @bp_spey.route("", methods=("POST",))
# def post_representation():
#     # project_id = max(projects.keys()) + 1
#     try:
#         data = request.json
#         projects[data.id] = data
#         resp = make_response(
#             jsonify({"message": "SPEY Representation added", "ID": data.id}), 201
#         )
#     except:
#         resp = make_response("NO Representation Added", 400))
