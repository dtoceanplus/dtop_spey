# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import requests
from flask import Blueprint, request

# TODO: to be updated for SPEY

from dtop_spey.service.api.errors import bad_request

bp = Blueprint("api_main", __name__)

# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/ec-farm", methods=["POST"])
def ec_farm():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/farm'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["ec_farm"]
    try:
        aep = requests.get(url).json()["aep"]
        print(aep)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to EC failed")

    return str(aep), 201


# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/ec-devices", methods=["POST"])
def ec_devices():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["ec_devices"]
    try:
        aep = requests.get(url).json()[0]["aep"]
        print(aep)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to EC failed")

    return str(aep), 201


# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/et-devices", methods=["POST"])
def et_devices():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["et_devices"]
    try:
        device_aep = requests.get(url).json()[0]["Dev_E_Grid"]
        print(device_aep)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to ET failed")

    return str(device_aep), 201


# TODO: Add route to API/DREDD (can probably skip this transaction until integration phase)
@bp.route("/et-farm", methods=["POST"])
def et_farm():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["et_farm"]
    try:
        array_aep = requests.get(url).json()["Array_E_Grid"]["value"]
        print(array_aep)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to ET failed")

    return str(array_aep), 201


@bp.route("/ed-outputs", methods=["POST"])
def ed_outputs():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["ed_outputs"]
    try:
        export_cable_length = requests.get(url).json()["export_cable_total_length"]
        print(export_cable_length)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to ET failed")

    return str(export_cable_length), 201


@bp.route("/mc-general", methods=["POST"])
def mc_general():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_general"]
    try:
        rated_capacity = requests.get(url).json()["rated_capacity"]
        print(rated_capacity)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to ET failed")

    return str(rated_capacity), 201


@bp.route("/mc-dimensions", methods=["POST"])
def mc_dimensions():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_dimensions"]
    try:
        mass = requests.get(url).json()["mass"]
        print(mass)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to ET failed")

    return str(mass), 201


@bp.route("/mc-machine", methods=["POST"])
def mc_machine():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["mc_machine"]
    try:
        type_mc = requests.get(url).json()["type"]
        print(type_mc)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to ET failed")

    return str(type_mc), 201


@bp.route("/lmo-downtime", methods=["POST"])
def lmo_downtime():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["lmo_downtime"]
    try:
        n_devices = requests.get(url).json()["n_devices"]
        print(n_devices)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to LMO failed")

    return str(n_devices), 201


@bp.route("/sc-info", methods=["POST"])
def sc_info():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["sc_info"]
    try:
        area = requests.get(url).json()["surface"]
        print(area)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to ET failed")

    return str(area), 201


@bp.route("/sc-flux-wave", methods=["POST"])
def sc_flux_wave():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["sc_flux_wave"]
    try:
        flux = requests.get(url).json()["mean"]
        print(flux)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to SC failed")

    return str(flux), 201


@bp.route("/sc-flux-current", methods=["POST"])
def sc_flux_current():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["sc_flux_current"]
    try:
        flux = requests.get(url).json()["mean"]
        print(flux)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to SC failed")

    return str(flux), 201


@bp.route("/sc-occurrence-wave", methods=["POST"])
def sc_occurrence_wave():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["sc_occurrence_wave"]
    try:
        prob = requests.get(url).json()["pdf"][0][0]
        print(prob)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to SC failed")

    return str(prob), 201


@bp.route("/sc-occurrence-current", methods=["POST"])
def sc_occurrence_current():
    """
    Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

    To be used in PACT testing example.

    Example request body for EC:

    {
        'farm': 'http://localhost:{port}/ec/{ecId}/devices'
    }

    :return: the annual energy produced (aep) metric from EC module (for the specified farm URL)
    :rtype: float
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    url = request_body["sc_occurrence_current"]
    try:
        prob = requests.get(url).json()["p"]["January"][0]
        print(prob)
    except requests.exceptions.RequestException as err:
        print(err)
        return bad_request("HTTP request to SC failed")

    return str(prob), 201
