# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import requests
import copy
from flask import (
    Blueprint,
    jsonify,
    redirect,
    render_template,
    request,
    url_for,
    make_response,
)
import os

import json

import pandas as pd

from dtop_spey import business

from datetime import datetime

# from dtop_spey.storage.models.models import "class Assessment_Table(db.Model)"
from dtop_spey.storage.models.models import (
    db,
    Assessment_Table,
    Inputs_Table,
    Energy_Production,
    Alternative_Metrics,
    Efficiency,
    Power_Quality,
    Representation,
)

# from dtop_spey.storage.schemas.schemas import "schemas"
from dtop_spey.storage.schemas.schemas import (
    AssessmentSchema,
    InputsSchema,
    EnergyProductionSchema,
    AlternativeMetricsSchema,
    EfficiencySchema,
    PowerQualitySchema,
    RepresentationSchema,
)
from dtop_spey.storage import config

bp_spey = Blueprint("assessment_bp", __name__)

headers = {"Access-Control-Allow-Headers": "Content-Type"}


def get_url_and_headers(module):
    """
    Common function provided by OCC to get DTOP basic module URL and headers

    :param str module: the DTOP module nickname (e.g. 'mm')
    :return: the module API url and the headers instance
    :rtype: tuple
    """
    protocol = os.getenv("DTOP_PROTOCOL", "http")
    domain = os.environ["DTOP_DOMAIN"]
    auth = os.getenv("DTOP_AUTH", "")

    print()
    print("Authorization header value :")
    header_auth = f"{auth}"
    print(header_auth)

    print()
    print("Headers - auth :")
    headers = {"Authorization": header_auth}
    print(headers)

    print()
    print("Host header value :")
    header_host = f'{module + "." + domain}'
    print(header_host)

    server_url = f'{protocol + "://" + header_host}'

    print()
    print("Check if DTOP is deployed on DNS resolved server or on workstation :")

    try:
        response = requests.get(server_url, headers=headers)
        print("DTOP appears to be deployed on DNS resolved server")
        module_api_url = f"{server_url}"
    except requests.ConnectionError as exception:
        print("DTOP appears to be deployed on workstation")
        docker_ws_ip = "http://172.17.0.1:80"
        module_api_url = f"{docker_ws_ip}"
        print()
        print("Headers - auth and localhost :")
        headers["Host"] = header_host
        print(headers)

    print()
    print("Basic Module Open API URL :")
    print(module_api_url)
    print()

    return module_api_url, headers


def call_intermodule_service(module, uri):
    """
    Generic function for communicating directly with the BE of other modules.

    Uses the *get_url_and_headers* function.
    Leads to a 400 error if the module server is not available.
    If the request is unsuccessful, a HTTP error is returned.

    :param str module: the nickname for the module that is being communicated with
    :param str uri: the URI specifying the appropriate route to call for the metric
    :return: the request response if successful, tuple of error and status code otherwise
    :rtype: requests.Response or tuple
    """
    # TODO: docstrings
    # TODO: update print statements
    print()
    print("Test example - request from Catalog BE to Main Module BE to get users' list")

    print()
    api_url, headers = get_url_and_headers(module)
    print("MM Open API URL to Get users :")
    users_url = f"{api_url + uri}"
    print(users_url)

    print()
    print("CM BE to MM BE request and response :")
    print()

    try:
        resp = requests.get(users_url, headers=headers)
        resp.raise_for_status()
        print()
        return resp
    except requests.exceptions.ConnectionError as errc:
        print("Error Connecting:", errc)
        print("The module server is not available")
        return errc, 400
    except requests.exceptions.HTTPError as errh:
        print("HTTP Error:", errh)
        return errh, errh.response.status_code


@bp_spey.route("", methods=["GET"])
def get_all_spey_assessments():
    """
        Returns the list of all the projects
    """
    try:
        all_projects = Assessment_Table.query.all()
        assessments_schema = AssessmentSchema(many=True)
        return make_response(
            jsonify(assessments_schema.dump(all_projects)), 200, headers
        )

    except:
        return jsonify({"error_message": "error while parsing the projects"}), 400


@bp_spey.route("", methods=["POST"])
def add_spey_assessment():
    """
        Post a project
    """
    try:
        assmnt_data = request.get_json()
        # assessment_schema = AssessmentSchema()
        # assmnt = assessment_schema.load(assmnt_data)

        now = datetime.now()

        # dd/mm/YY H:M:S
        dt_string = now.strftime("%B %d, %Y %H:%M:%S")

        assmnt_data["date"] = dt_string

        assmnt_data["status"] = 50

        assmnt = Assessment_Table(**assmnt_data)

        db.session.add(assmnt)

        db.session.commit()

        resp = make_response(
            jsonify({"message": "Assessment added", "ID": str(assmnt.speyId)}), 201
        )
    except Exception as e:
        resp = make_response(jsonify({"message": str(e)}), 400)

    return resp


@bp_spey.route("/<speyId>/inputs", methods=["POST"])
def spey_inputs(speyId):
    """
        Inputs of SPEY
    """
    try:
        inputs_data = request.get_json()
        # assessment_schema = AssessmentSchema()
        # assmnt = assessment_schema.load(inputs_data)

        assmnt = Assessment_Table.query.get(speyId)

        update_dictionary = {
            "status": 100,
            "scId": str(inputs_data.get("scId")),
            "mcId": str(inputs_data.get("mcId")),
            "ecId": str(inputs_data.get("ecId")),
            "etId": str(inputs_data.get("etId")),
            "edId": str(inputs_data.get("edId")),
            "lmoId": str(inputs_data.get("lmoId")),
        }

        assmnt.update(**update_dictionary)

        inputs_data["scId"] = str(inputs_data.get("scId"))
        inputs_data["mcId"] = str(inputs_data.get("mcId"))
        inputs_data["ecId"] = str(inputs_data.get("ecId"))
        inputs_data["etId"] = str(inputs_data.get("etId"))
        inputs_data["edId"] = str(inputs_data.get("edId"))
        inputs_data["lmoId"] = str(inputs_data.get("lmoId"))

        inputs_table = Inputs_Table(**inputs_data)

        assmnt.inputs_table = inputs_table

        dict_tech = {"TEC": "Tidal", "WEC": "Wave"}
        device_characteristics_machine = inputs_data.get(
            "device_characteristics_machine"
        )

        resp_mc_1 = {}
        if "url" in device_characteristics_machine:
            resp_mc_1 = call_intermodule_service(
                "mc", device_characteristics_machine["url"]
            ).json()

        device_characteristics_machine = {
            **device_characteristics_machine,
            **resp_mc_1,
        }

        techn = dict_tech[device_characteristics_machine["type"]]

        device_characteristics_general = inputs_data.get(
            "device_characteristics_general"
        )

        resp_mc_2 = {}
        if "url" in device_characteristics_general:
            resp_mc_2 = call_intermodule_service(
                "mc", device_characteristics_general["url"]
            ).json()

        device_characteristics_general = {
            **device_characteristics_general,
            **resp_mc_2,
        }

        rated_capacity = device_characteristics_general["rated_capacity"]

        device_characteristics_dimensions = inputs_data.get(
            "device_characteristics_dimensions"
        )

        resp_mc_3 = {}
        if "url" in device_characteristics_dimensions:
            resp_mc_3 = call_intermodule_service(
                "mc", device_characteristics_dimensions["url"]
            ).json()

        device_characteristics_dimensions = {
            **device_characteristics_dimensions,
            **resp_mc_3,
        }

        mass = device_characteristics_dimensions["mass"]

        wet_area = device_characteristics_dimensions["wet_area"]

        char_dim = device_characteristics_dimensions["characteristic_dimension"]

        info_sc = inputs_data.get("sc_farm")

        resp_sc1 = {}
        if "url" in info_sc:
            resp_sc1 = call_intermodule_service("sc", info_sc["url"]).json()

        info_sc = {
            **info_sc,
            **resp_sc1,
        }

        lease_area = info_sc["surface"]

        if techn == "Wave":
            resp_sc_2 = {}
            wave_flux = inputs_data.get("wave_flux")
            if "url" in wave_flux:
                resp_sc_2 = call_intermodule_service("sc", wave_flux["url"]).json()

            wave_flux = {
                **wave_flux,
                **resp_sc_2,
            }
            average_flux = wave_flux["mean"]
        elif techn == "Tidal":
            resp_sc_3 = {}
            current_flux = inputs_data.get("current_flux")
            if "url" in current_flux:
                resp_sc_3 = call_intermodule_service("sc", current_flux["url"]).json()

            current_flux = {
                **current_flux,
                **resp_sc_3,
            }
            average_flux = current_flux["mean"] / 1000.0  # SC will provide this in W

        month_array = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ]
        length = len(month_array)

        if techn == "Wave":
            resp_sc_4 = {}
            wave_EJPD = inputs_data.get("wave_EJPD")
            if "url" in wave_EJPD:
                resp_sc_4 = call_intermodule_service("sc", wave_EJPD["url"]).json()

            wave_EJPD = {
                **wave_EJPD,
                **resp_sc_4,
            }

            monthly_resource_histogram = {}
            for k in range(length):
                number_occurrences = sum(wave_EJPD["pdf"][k])
                monthly_resource_histogram[month_array[k]] = [
                    j / number_occurrences for j in wave_EJPD["pdf"][k]
                ]
            monthly_resource_histogram["bins"] = wave_EJPD["id"][0]
        elif techn == "Tidal":
            resp_sc_5 = {}
            current_scenarii = inputs_data.get("current_scenarii")
            if "url" in current_scenarii:
                resp_sc_5 = call_intermodule_service(
                    "sc", current_scenarii["url"]
                ).json()

            current_scenarii = {
                **current_scenarii,
                **resp_sc_5,
            }

            monthly_resource_histogram = current_scenarii["p"]

            for k in range(length):
                number_occurrences = sum(current_scenarii["p"][month_array[k]])
                monthly_resource_histogram[month_array[k]] = [
                    j / number_occurrences
                    for j in current_scenarii["p"][month_array[k]]
                ]
            monthly_resource_histogram["bins"] = current_scenarii["id"]["January"]

        if "ec_farm" in inputs_data:

            ec_farm = inputs_data.get("ec_farm")

            resp_ec_1 = {}
            if "url" in ec_farm:
                resp_ec_1 = call_intermodule_service("ec", ec_farm["url"]).json()

            ec_farm = {
                **ec_farm,
                **resp_ec_1,
            }

            array_aep = ec_farm["aep"]
            array_q_f = ec_farm["q_factor"]
            number_dev = ec_farm["number_devices"]
        else:
            array_aep = None
            array_q_f = None
            number_dev = 1

        if "ec_devices" in inputs_data:

            ec_devices = inputs_data.get("ec_devices")

            if "url" in ec_devices:
                ec_devices = call_intermodule_service("ec", ec_devices["url"]).json()

            device_aep = [k["aep"] for k in ec_devices]
            device_q_f = [k["q_factor"] for k in ec_devices]
        else:
            device_aep = None
            device_q_f = None

        if "et_results_array" in inputs_data:

            et_results_array = inputs_data.get("et_results_array")

            resp_et_1 = {}
            if "url" in et_results_array:
                resp_et_1 = call_intermodule_service(
                    "et", et_results_array["url"]
                ).json()

            et_results_array = {
                **et_results_array,
                **resp_et_1,
            }

            array_transf_aep = et_results_array["Array_E_Grid"]["value"]

        else:
            array_transf_aep = None

        if "et_results" in inputs_data:

            et_results = inputs_data.get("et_results")

            if "url" in et_results:
                et_results = call_intermodule_service("et", et_results["url"]).json()

            device_transf_aep = [k["Dev_E_Grid"]["value"] for k in et_results]

            device_active_transf = [
                k["Dev_Pa_Grid"]["value"]["Power"] for k in et_results
            ]

            device_active_transf_pd = pd.DataFrame(device_active_transf).transpose()

            device_reactive_transf = [
                k["Dev_Pr_Grid"]["value"]["Power"] for k in et_results
            ]

            device_reactive_transf_pd = pd.DataFrame(device_reactive_transf).transpose()

            if "ec_devices" in inputs_data:
                if number_dev != len(device_transf_aep):
                    raise ValueError(
                        "The number of devices in Energy Capture is not consistent with the file in Energy Transformation"
                    )

        else:
            device_transf_aep = None
            device_active_transf_pd = None
            device_reactive_transf_pd = None

        if "ed_results" in inputs_data:

            ed_results = inputs_data.get("ed_results")

            resp_ed = {}
            if "url" in ed_results:
                resp_ed = call_intermodule_service("ed", ed_results["url"]).json()

            ed_results = {
                **ed_results,
                **resp_ed,
            }

            export_cable_total_length = ed_results["export_cable_total_length"]
            intra_array_cable_length = ed_results["array_cable_total_length"]
            array_cable_total_length = (
                export_cable_total_length + intra_array_cable_length
            )

            power_delivery_histogram = {}
            power_histogram = ed_results["array_power_output"]
            power_delivery_histogram["power"] = power_histogram

            if len(power_histogram) == 1:
                monthly_resource_histogram = {
                    x: [1] for x in monthly_resource_histogram
                }

            power_delivery_histogram["bins"] = monthly_resource_histogram["bins"]

            array_power_output = ed_results["array_power_output"]

            array_power_output_pd = pd.DataFrame(array_power_output)

            array_reactive_output = ed_results["array_reactive_output"]

            array_reactive_output_pd = pd.DataFrame(array_reactive_output)

            delivered_aep = ed_results["annual_yield"]

            for k in month_array:
                len_power = len(array_power_output)
                len_occur = len(monthly_resource_histogram[k])
                if len_power != len_occur:
                    raise ValueError(
                        "The number of sea state considered ",
                        str(len_occur),
                        " in Site Characterisation in ",
                        k,
                        " is not consistent with the file in Energy Delivery ",
                        str(len_power),
                        ".  For example, ",
                        str(monthly_resource_histogram[k]),
                    )

            if "et_results" in inputs_data:
                for k in range(len(device_active_transf)):
                    if len(array_power_output) != len(device_active_transf[k]):
                        raise ValueError(
                            "The number of sea state considered in Energy Transformation (active power) is not consistent with the ones considered in Energy Delivery. Check if MC/EC complexity is greater than ED complexity and either run MC at complexity 1 or ED at complexity 2."
                        )
                for k in range(len(device_reactive_transf)):
                    if len(array_reactive_output) != len(device_reactive_transf[k]):
                        raise ValueError(
                            "The number of sea state considered in Energy Transformation (reactive power) is not consistent with the file in Energy Delivery"
                        )

        else:
            delivered_aep = None
            array_power_output_pd = None
            array_reactive_output_pd = None
            power_delivery_histogram = {"power": [0], "bins": [1]}
            array_cable_total_length = None
            intra_array_cable_length = None
            export_cable_total_length = None

        if "downtime_hours" in inputs_data:

            downtime_hours_resp = inputs_data.get("downtime_hours")

            resp_lmo = {}
            if "url" in downtime_hours_resp:
                resp_lmo = call_intermodule_service(
                    "lmo", downtime_hours_resp["url"]
                ).json()

            downtime_hours_resp = {
                **downtime_hours_resp,
                **resp_lmo,
            }

            project_life = downtime_hours_resp["project_life"]

            if "downtime" in downtime_hours_resp:
                downtime_hours_array = downtime_hours_resp["downtime"]
                downtime_hours = {
                    i["device_id"]: i["downtime_table"] for i in downtime_hours_array
                }

                downtime_hours_dict = {}
                month_conv = {
                    "jan": "January",
                    "feb": "February",
                    "mar": "March",
                    "apr": "April",
                    "may": "May",
                    "jun": "June",
                    "jul": "July",
                    "aug": "August",
                    "sep": "September",
                    "oct": "October",
                    "nov": "November",
                    "dec": "December",
                    "year": "Years",
                }

                for k in downtime_hours.keys():
                    # del downtime_hours[k]["year"]
                    if downtime_hours[k]["year"][0] == 0:

                        downtime_hours[k]["year"] = [
                            x + 1 for x in downtime_hours[k]["year"]
                        ]
                    downtime_hours_dict[k] = pd.DataFrame(downtime_hours[k])
                    # downtime_hours_dict[k].set_index("year")
                    downtime_hours_dict[k].rename(columns=month_conv, inplace=True)

                    downtime_hours_dict[k].set_index("Years", inplace=True)

                if "ec_devices" in inputs_data:
                    if number_dev != len(downtime_hours):
                        raise ValueError(
                            "The number of devices in Energy Capture is not consistent with the file in Logistics and Marine Operations"
                        )

            else:
                downtime_hours_dict = None

        else:
            downtime_hours_dict = None
            project_life = 1

        # Calculate Efficiency Metrics
        results_eff = business.Efficiency.get_cpx(
            "1",
            average_flux,
            techn,
            char_dim,
            number_dev,
            rated_capacity,
            array_aep,
            device_aep,
            array_q_f,
            device_q_f,
            array_transf_aep,
            device_transf_aep,
            delivered_aep,
        )

        results_eff.get_inputs()
        results_eff.estimate_rated_flux()
        results_eff.captured_efficiency()
        results_eff.transformed_efficiency()
        results_eff.delivered_efficiency()

        results_eff.outputs["array_q_f"] = {
            "value": array_q_f,
            "Aggregation-Level": "array",
            "Unit": "-",
            "Description": "q-factor of the array",
            "Tag": "q-factor",
            "Label": "Array q-factor",
        }
        results_eff.outputs["device_q_f"] = {
            "value": device_q_f,
            "Aggregation-Level": "device",
            "Unit": "-",
            "Description": "q-factor of the devices",
            "Tag": "q-factor",
            "Label": "Device q-factor",
        }

        results_eff_dict = {**results_eff.inputs, **results_eff.outputs}

        assmnt_eff = Efficiency(**results_eff_dict)
        assmnt_eff.inputs_table = inputs_table

        # Calculate Alternative Metrics

        results_am = business.AlternativeMetrics.get_cpx(
            "1",
            average_flux,
            techn,
            char_dim,
            number_dev,
            rated_capacity,
            wet_area,
            mass,
            annual_captured_energy=array_aep,
            device_annual_captured_energy=device_aep,
            annual_transformed_energy=array_transf_aep,
            device_annual_transformed_energy=device_transf_aep,
            PTO_force=None,
            annual_delivered_energy=delivered_aep,
            export_cable_length=export_cable_total_length,
            intra_array_cable_length=intra_array_cable_length,
            lease_area=lease_area,
        )

        results_am.get_inputs()
        results_am.wetted_area_parameters()
        results_am.mass_parameters()
        results_am.calculate_PWR()
        results_am.CL_parameters()
        results_am.cable_length_parameters()
        results_am.lease_area_parameters()

        results_am_dict = {**results_am.inputs, **results_am.outputs}

        assmnt_am = Alternative_Metrics(**results_am_dict)
        assmnt_am.inputs_table = inputs_table

        # Calculate Power Quality

        results_pq = business.PowerQuality.get_cpx(
            "1",
            device_active_transf_pd,
            device_reactive_transf_pd,
            array_power_output_pd,
            array_reactive_output_pd,
        )

        results_pq.get_inputs()
        results_pq.transformed_phase()
        results_pq.delivered_phase()

        results_pq_dict = {**results_pq.inputs, **results_pq.outputs}

        assmnt_pq = Power_Quality(**results_pq_dict)
        assmnt_pq.inputs_table = inputs_table

        # Calculate Energy Production

        results_ep = business.EnergyProduction.get_cpx(
            "1",
            monthly_resource_histogram=monthly_resource_histogram,
            power_delivery_histogram=power_delivery_histogram,
            number_devices=number_dev,
            project_life=project_life,
            downtime_hours_per_device=downtime_hours_dict,
        )

        results_ep.get_inputs()
        results_ep.downtime_net_energy()

        for key, value in results_ep.inputs["downtime_hours_per_device"][
            "value"
        ].items():
            results_ep.inputs["downtime_hours_per_device"]["value"][
                key
            ] = value.to_json()

        for key, value in results_ep.outputs.items():

            if isinstance(value["value"], pd.DataFrame):
                value["value"] = value["value"].fillna(0)
                results_ep.outputs[key]["value"] = value["value"].to_json()

            elif isinstance(value["value"], pd.Series):
                value["value"] = value["value"].fillna(0)
                results_ep.outputs[key]["value"] = value["value"].to_json()

            elif isinstance(value["value"], dict):
                for k, v in value["value"].items():
                    if isinstance(v, pd.DataFrame):
                        v = v.fillna(0)
                        results_ep.outputs[key]["value"][k] = v.to_json()
                    #
                    elif isinstance(v, pd.Series):
                        v = v.fillna(0)
                        results_ep.outputs[key]["value"][k] = v.to_json()

                    elif not isinstance(v, list):
                        if (
                            results_ep.outputs[key]["value"][k]
                            != results_ep.outputs[key]["value"][k]
                        ):
                            results_ep.outputs[key]["value"][k] = 0

            elif not isinstance(value["value"], list):
                if results_ep.outputs[key]["value"] != results_ep.outputs[key]["value"]:
                    results_ep.outputs[key]["value"] = 0

        results_ep_dict = {**results_ep.inputs, **results_ep.outputs}

        assmnt_ep = Energy_Production(**results_ep_dict)
        assmnt_ep.inputs_table = inputs_table

        outputs_eff = copy.deepcopy(results_eff.outputs)
        outputs_am = copy.deepcopy(results_am.outputs)
        outputs_ep = copy.deepcopy(results_ep.outputs)
        outputs_pq = copy.deepcopy(results_pq.outputs)

        array = [
            {
                "id": "1",
                "assessment": {
                    "array_captured_eff": outputs_eff["array_capt_eff"],
                    "array_absolute_transformed_eff": outputs_eff[
                        "array_abs_transf_eff"
                    ],
                    "array_relative_transformed_eff": outputs_eff[
                        "array_rel_transf_eff"
                    ],
                    "array_absolute_delivered_eff": outputs_eff["array_abs_deliv_eff"],
                    "array_relative_delivered_eff": outputs_eff["array_rel_deliv_eff"],
                    "ace_wetted_area_parameter": outputs_am["array_captured_wetted"],
                    "ate_wetted_area_parameter": outputs_am["array_transformed_wetted"],
                    "ade_wetted_area_parameter": outputs_am["array_delivered_wetted"],
                    "ace_mass_parameter": outputs_am["array_captured_mass"],
                    "ate_mass_parameter": outputs_am["array_transformed_mass"],
                    "ade_mass_parameter": outputs_am["array_delivered_mass"],
                    "ace_lease_area_parameter": outputs_am["array_captured_lease"],
                    "ate_lease_area_parameter": outputs_am["array_transformed_lease"],
                    "ade_lease_area_parameter": outputs_am["array_delivered_lease"],
                    "cw_array": outputs_am["array_CL"],
                    "cw_ratio_array": outputs_am["array_CL_ratio"],
                    "cw_ratio_power_array": outputs_am["array_CL_rated_power"],
                    "phase_at_generator_per_array_level": outputs_pq[
                        "array_transformed_phase"
                    ],
                    "array_monthly_gross_energy": outputs_ep[
                        "array_monthly_gross_energy_pd"
                    ],
                    "array_annual_gross_energy": outputs_ep[
                        "array_annual_gross_energy_pd"
                    ],
                    "array_lifetime_gross_energy": outputs_ep[
                        "array_lifetime_gross_energy_pd"
                    ],
                    "array_monthly_lost_energy": outputs_ep[
                        "array_monthly_lost_energy_pd"
                    ],
                    "array_annual_lost_energy": outputs_ep[
                        "array_annual_lost_energy_pd"
                    ],
                    "array_lifetime_lost_energy": outputs_ep[
                        "array_lifetime_lost_energy_pd"
                    ],
                    "array_monthly_net_energy": outputs_ep[
                        "array_monthly_net_energy_pd"
                    ],
                    "array_annual_net_energy": outputs_ep["array_annual_net_energy_pd"],
                    "array_lifetime_net_energy": outputs_ep[
                        "array_lifetime_net_energy_pd"
                    ],
                    "array_monthly_lost_energy_ratio": outputs_ep[
                        "array_monthly_lost_ratio_pd"
                    ],
                    "array_annual_lost_energy_ratio": outputs_ep[
                        "array_annual_lost_ratio_pd"
                    ],
                    "array_lifetime_lost_energy_ratio": outputs_ep[
                        "array_lifetime_lost_ratio_pd"
                    ],
                    "array_monthly_net_energy_ratio": outputs_ep[
                        "array_monthly_net_ratio_pd"
                    ],
                    "array_annual_net_energy_ratio": outputs_ep[
                        "array_annual_net_ratio_pd"
                    ],
                    "array_lifetime_net_energy_ratio": outputs_ep[
                        "array_lifetime_net_ratio_pd"
                    ],
                },
            }
        ]

        array[0]["id"] = "1"
        for k in array[0]["assessment"]:
            array[0]["assessment"][k]["description"] = array[0]["assessment"][k].pop(
                "Description"
            )
            array[0]["assessment"][k]["unit"] = array[0]["assessment"][k].pop("Unit")
            array[0]["assessment"][k]["origin"] = "SPEY"
            array[0]["assessment"][k].pop("Aggregation-Level")
            array[0]["assessment"][k].pop("Tag")
            array[0]["assessment"][k].pop("Label")

        device = []
        for i in range(number_dev):
            device.append({})
            device[i]["id"] = "1_" + str(i + 1)
            device[i]["assessment"] = {}
            device[i]["assessment"]["device_captured_eff"] = copy.deepcopy(
                outputs_eff["device_capt_eff"]
            )
            device[i]["assessment"]["device_absolute_transformed_eff"] = copy.deepcopy(
                outputs_eff["device_abs_transf_eff"]
            )
            device[i]["assessment"]["device_relative_transformed_eff"] = copy.deepcopy(
                outputs_eff["device_rel_transf_eff"]
            )
            device[i]["assessment"]["rated_flux"] = copy.deepcopy(
                outputs_eff["rated_power_flux"]
            )
            device[i]["assessment"]["dace_wetted_area_parameter"] = copy.deepcopy(
                outputs_am["device_captured_wetted"]
            )
            device[i]["assessment"]["date_wetted_area_parameter"] = copy.deepcopy(
                outputs_am["device_transformed_wetted"]
            )
            device[i]["assessment"]["dace_mass_parameter"] = copy.deepcopy(
                outputs_am["device_captured_mass"]
            )
            device[i]["assessment"]["date_mass_parameter"] = copy.deepcopy(
                outputs_am["device_transformed_mass"]
            )
            device[i]["assessment"]["power_to_mass_ratio"] = copy.deepcopy(
                outputs_am["PWR"]
            )
            device[i]["assessment"]["cw_device"] = copy.deepcopy(
                outputs_am["device_CL"]
            )
            device[i]["assessment"]["cw_ratio_device"] = copy.deepcopy(
                outputs_am["device_CL_ratio"]
            )
            device[i]["assessment"]["cw_ratio_power_device"] = copy.deepcopy(
                outputs_am["device_CL_rated_power"]
            )
            device[i]["assessment"][
                "phase_at_generator_per_device_level"
            ] = copy.deepcopy(outputs_pq["device_transformed_phase"])
            device[i]["assessment"]["device_monthly_gross_energy"] = copy.deepcopy(
                outputs_ep["device_monthly_gross_energy_pd"]
            )
            device[i]["assessment"]["device_annual_gross_energy"] = copy.deepcopy(
                outputs_ep["device_annual_gross_energy_pd"]
            )
            device[i]["assessment"]["device_lifetime_gross_energy"] = copy.deepcopy(
                outputs_ep["device_lifetime_gross_energy_pd"]
            )
            device[i]["assessment"]["device_monthly_lost_energy"] = copy.deepcopy(
                outputs_ep["device_monthly_lost_energy_pd"]
            )
            device[i]["assessment"]["device_annual_lost_energy"] = copy.deepcopy(
                outputs_ep["device_annual_lost_energy_pd"]
            )
            device[i]["assessment"]["device_lifetime_lost_energy"] = copy.deepcopy(
                outputs_ep["device_lifetime_lost_energy_pd"]
            )
            device[i]["assessment"]["device_monthly_net_energy"] = copy.deepcopy(
                outputs_ep["device_monthly_net_energy_pd"]
            )
            device[i]["assessment"]["device_annual_net_energy"] = copy.deepcopy(
                outputs_ep["device_annual_net_energy_pd"]
            )
            device[i]["assessment"]["device_lifetime_net_energy"] = copy.deepcopy(
                outputs_ep["device_lifetime_net_energy_pd"]
            )
            device[i]["assessment"]["device_monthly_lost_energy_ratio"] = copy.deepcopy(
                outputs_ep["device_monthly_lost_ratio_pd"]
            )
            device[i]["assessment"]["device_annual_lost_energy_ratio"] = copy.deepcopy(
                outputs_ep["device_annual_lost_ratio_pd"]
            )
            device[i]["assessment"][
                "device_lifetime_lost_energy_ratio"
            ] = copy.deepcopy(outputs_ep["device_lifetime_lost_ratio_pd"])
            device[i]["assessment"]["device_monthly_net_energy_ratio"] = copy.deepcopy(
                outputs_ep["device_monthly_net_ratio_pd"]
            )
            device[i]["assessment"]["device_annual_net_energy_ratio"] = copy.deepcopy(
                outputs_ep["device_annual_net_ratio_pd"]
            )
            device[i]["assessment"]["device_lifetime_net_energy_ratio"] = copy.deepcopy(
                outputs_ep["device_lifetime_net_ratio_pd"]
            )

            for key in device[i]["assessment"]:
                device[i]["assessment"][key]["description"] = device[i]["assessment"][
                    key
                ].pop("Description")
                device[i]["assessment"][key]["unit"] = device[i]["assessment"][key].pop(
                    "Unit"
                )
                device[i]["assessment"][key]["origin"] = "SPEY"
                device[i]["assessment"][key].pop("Aggregation-Level")
                device[i]["assessment"][key].pop("Tag")
                device[i]["assessment"][key].pop("Label")
                if isinstance(device[i]["assessment"][key]["value"], list):
                    device[i]["assessment"][key]["value"] = copy.deepcopy(
                        device[i]["assessment"][key]["value"][i]
                    )

        power_transmission = [
            {
                "id": "1_1",
                "assessment": {"total_cable_ratio": outputs_am["Cable_ratio"]},
            }
        ]

        for k in power_transmission[0]["assessment"]:
            power_transmission[0]["assessment"][k]["description"] = power_transmission[
                0
            ]["assessment"][k].pop("Description")
            power_transmission[0]["assessment"][k]["unit"] = power_transmission[0][
                "assessment"
            ][k].pop("Unit")
            power_transmission[0]["assessment"][k]["origin"] = "SPEY"
            power_transmission[0]["assessment"][k].pop("Aggregation-Level")
            power_transmission[0]["assessment"][k].pop("Tag")
            power_transmission[0]["assessment"][k].pop("Label")

        transmission_to_shore = [
            {
                "id": "1_1_1",
                "assessment": {
                    "export_cable_ratio": outputs_am["Export_ratio"],
                    "phase_at_onshore_landing_point_per_array_level": outputs_pq[
                        "array_delivered_phase"
                    ],
                },
            }
        ]

        for k in transmission_to_shore[0]["assessment"]:
            transmission_to_shore[0]["assessment"][k][
                "description"
            ] = transmission_to_shore[0]["assessment"][k].pop("Description")
            transmission_to_shore[0]["assessment"][k]["unit"] = transmission_to_shore[
                0
            ]["assessment"][k].pop("Unit")
            transmission_to_shore[0]["assessment"][k]["origin"] = "SPEY"
            transmission_to_shore[0]["assessment"][k].pop("Aggregation-Level")
            transmission_to_shore[0]["assessment"][k].pop("Tag")
            transmission_to_shore[0]["assessment"][k].pop("Label")

        array_network = [
            {
                "id": "1_1_1",
                "assessment": {"intra_array_cable_ratio": outputs_am["IA_ratio"]},
            }
        ]

        for k in array_network[0]["assessment"]:
            array_network[0]["assessment"][k]["description"] = array_network[0][
                "assessment"
            ][k].pop("Description")
            array_network[0]["assessment"][k]["unit"] = array_network[0]["assessment"][
                k
            ].pop("Unit")
            array_network[0]["assessment"][k]["origin"] = "SPEY"
            array_network[0]["assessment"][k].pop("Aggregation-Level")
            array_network[0]["assessment"][k].pop("Tag")
            array_network[0]["assessment"][k].pop("Label")

        representation = {
            "array": array,
            "device": device,
            "power_transmission": power_transmission,
            "array_network": array_network,
            "transmission_to_shore": transmission_to_shore,
        }

        assmnt_dr = Representation(**representation)
        assmnt_dr.inputs_table = inputs_table

        db.session.add(assmnt)
        db.session.add(inputs_table)
        db.session.add(assmnt_eff)
        db.session.add(assmnt_am)
        db.session.add(assmnt_pq)
        db.session.add(assmnt_ep)
        db.session.add(assmnt_dr)

        db.session.commit()

        resp = make_response(
            jsonify({"message": "Assessment added", "ID": str(inputs_table.inputsId)}),
            201,
        )
    except ValueError as e:
        resp = make_response(jsonify({"message": str(e)}), 400)
    except Exception as e:
        resp = make_response(jsonify({"message": str(e)}), 400)
    except ZeroDivisionError as e:
        resp = make_response(jsonify({"message": str(e)}), 400)

    return resp


@bp_spey.route("/<speyId>", methods=["GET"])
def get_spey_project(speyId):
    """
        Get a project
    """

    try:

        assessment = Assessment_Table.query.get(speyId)

        assessment_schema = AssessmentSchema()

        assessment_dict = assessment_schema.dump(assessment)

        if assessment:
            return make_response(assessment_dict, 200, headers)
        else:
            return jsonify({"status": "fail", "message": "Failure"}), 404
        # raise IndexError("The specified ID does not exist in the DB")
    # except IndexError as e:
    #     return jsonify({"status": "fail", "message": str(e)}), 404
    except:
        return jsonify({"status": "fail", "message": "Unknown Reason"}), 500


@bp_spey.route("/<speyId>", methods=["DELETE"])
def delete_spey_project(speyId):
    """
        Delete a project
    """

    try:
        project = Assessment_Table.query.get(speyId)
        db.session.delete(project)
        db.session.commit()

        return make_response(jsonify({"message": "Assessment deleted"}), 200, headers)
    except:
        return make_response(
            jsonify({"message": "Assessment deleted failed"}), 404, headers
        )


@bp_spey.route("/<speyId>", methods=["PUT"])
def update_spey_project(speyId):
    """
        Update a project
    """

    try:
        assmnt = Assessment_Table.query.get(speyId)
        inputs_data = request.get_json()
        assmnt.update(**inputs_data)

        db.session.add(assmnt)

        db.session.commit()

        return make_response(jsonify({"message": "project updated"}), 201, headers)
    except Exception as e:
        return make_response(jsonify({"message": str(e)}), 400, headers)


@bp_spey.route("/<speyId>/power-quality", methods=["GET"])
def get_power_quality(speyId):
    """
        Get the Power Quality of a Project
    """

    try:

        assessment = Assessment_Table.query.get(speyId)
        inputs = assessment.inputs_table
        assessment_pq = inputs.power_quality
        pq_schema = PowerQualitySchema()
        assessment_pq_dict = pq_schema.dump(assessment_pq)

        pq_get = {
            "Inputs_PQ": {
                "transformed_active_power": assessment_pq_dict[
                    "transformed_active_power"
                ],
                "transformed_reactive_power": assessment_pq_dict[
                    "transformed_reactive_power"
                ],
                "delivered_active_power": assessment_pq_dict["delivered_active_power"],
                "delivered_reactive_power": assessment_pq_dict[
                    "delivered_reactive_power"
                ],
            },
            "Outputs_PQ": {
                "device_transformed_phase": assessment_pq_dict[
                    "device_transformed_phase"
                ],
                "array_transformed_phase": assessment_pq_dict[
                    "array_transformed_phase"
                ],
                "array_delivered_phase": assessment_pq_dict["array_delivered_phase"],
            },
        }

        if assessment:
            return make_response(pq_get, 200, headers)
        # else:
        #     return (jsonify({"status": "fail", "message": pq_get}), 404)
        # raise IndexError("The specified ID does not exist in the DB")
    # except IndexError as e:
    #     return jsonify({"status": "fail", "message": str(e)}), 404
    except:
        return jsonify({"status": "fail", "message": "Unknown Reason"}), 404


@bp_spey.route("/<speyId>/inputs", methods=["GET"])
def get_inputs(speyId):
    """
        Get the Inputs of a Study
    """
    try:

        assmnt = Assessment_Table.query.get(speyId)

        inputs = assmnt.inputs_table

        inputs_schema = InputsSchema()

        inputs_dict = inputs_schema.dump(inputs)

        if inputs:
            return make_response(inputs_dict, 200, headers)
        # else:
        #     return (jsonify({"status": "fail", "message": assessment_am_dict}), 404)
        # raise IndexError("The specified ID does not exist in the DB")
    # except IndexError as e:
    #     return jsonify({"status": "fail", "message": str(e)}), 404
    except:
        return jsonify({"status": "fail", "message": "Unknown Reason"}), 404


@bp_spey.route("/<speyId>/efficiency", methods=["GET"])
def get_efficiency(speyId):
    """
        Get the Efficiency of a Project
    """

    try:

        assessment = Assessment_Table.query.get(speyId)
        inputs = assessment.inputs_table
        assessment_eff = inputs.efficiency
        eff_schema = EfficiencySchema()
        assessment_eff_dict = eff_schema.dump(assessment_eff)
        Inputs_EFF = {
            "average_flux": assessment_eff_dict["average_flux"],
            "tech_type": assessment_eff_dict["tech_type"],
            "number_devices": assessment_eff_dict["number_devices"],
            "rated_power": assessment_eff_dict["rated_power"],
            "charact_dimension": assessment_eff_dict["charact_dimension"],
            "annual_captured_energy": assessment_eff_dict["annual_captured_energy"],
            "annual_transformed_energy": assessment_eff_dict[
                "annual_transformed_energy"
            ],
            "annual_delivered_energy": assessment_eff_dict["annual_delivered_energy"],
            "q_factor": assessment_eff_dict["q_factor"],
            "device_annual_captured_energy": assessment_eff_dict[
                "device_annual_captured_energy"
            ],
            "device_annual_transformed_energy": assessment_eff_dict[
                "device_annual_transformed_energy"
            ],
            "device_q_factor": assessment_eff_dict["device_q_factor"],
        }

        Outputs_EFF = {
            "rated_power_flux": assessment_eff_dict["rated_power_flux"],
            "array_capt_eff": assessment_eff_dict["array_capt_eff"],
            "device_capt_eff": assessment_eff_dict["device_capt_eff"],
            "array_abs_transf_eff": assessment_eff_dict["array_abs_transf_eff"],
            "device_abs_transf_eff": assessment_eff_dict["device_abs_transf_eff"],
            "array_rel_transf_eff": assessment_eff_dict["array_rel_transf_eff"],
            "device_rel_transf_eff": assessment_eff_dict["device_rel_transf_eff"],
            "array_abs_deliv_eff": assessment_eff_dict["array_abs_deliv_eff"],
            "array_rel_deliv_eff": assessment_eff_dict["array_rel_deliv_eff"],
            "array_q_factor": assessment_eff_dict["array_q_f"],
            "device_q_factor": assessment_eff_dict["device_q_f"],
        }

        efficiency_get = {"Inputs_EFF": Inputs_EFF, "Outputs_EFF": Outputs_EFF}

        if assessment:
            return make_response(efficiency_get, 200, headers)
        # else:
        #     return (jsonify({"status": "fail", "message": assessment_eff_dict}), 404)
        # raise IndexError("The specified ID does not exist in the DB")
    # except IndexError as e:
    #     return jsonify({"status": "fail", "message": str(e)}), 404
    except:
        return jsonify({"status": "fail", "message": "Unknown Reason"}), 404


@bp_spey.route("/<speyId>/alternative-metrics", methods=["GET"])
def get_alternative_metrics(speyId):
    """
        Get the Alternative Metrics of a Project
    """

    try:

        assessment = Assessment_Table.query.get(speyId)
        inputs = assessment.inputs_table
        assessment_am = inputs.alternative_metrics
        am_schema = AlternativeMetricsSchema()
        assessment_am_dict = am_schema.dump(assessment_am)

        Inputs_AM = {
            "average_flux": assessment_am_dict["average_flux"],
            "tech_type": assessment_am_dict["tech_type"],
            "number_devices": assessment_am_dict["number_devices"],
            "charact_dimension": assessment_am_dict["charact_dimension"],
            "rated_power": assessment_am_dict["rated_power"],
            "wetted_area_device": assessment_am_dict["wetted_area_device"],
            "device_mass": assessment_am_dict["device_mass"],
            "lease_area": assessment_am_dict["lease_area"],
            "annual_captured_energy": assessment_am_dict["annual_captured_energy"],
            "annual_transformed_energy": assessment_am_dict[
                "annual_transformed_energy"
            ],
            "annual_delivered_energy": assessment_am_dict["annual_delivered_energy"],
            "device_annual_captured_energy": assessment_am_dict[
                "device_annual_captured_energy"
            ],
            "device_annual_transformed_energy": assessment_am_dict[
                "device_annual_transformed_energy"
            ],
            "export_cable_length": assessment_am_dict["export_cable_length"],
            "intra_array_cable_length": assessment_am_dict["intra_array_cable_length"],
        }

        Outputs_AM = {
            "array_captured_wetted": assessment_am_dict["array_captured_wetted"],
            "device_captured_wetted": assessment_am_dict["device_captured_wetted"],
            "array_transformed_wetted": assessment_am_dict["array_transformed_wetted"],
            "device_transformed_wetted": assessment_am_dict[
                "device_transformed_wetted"
            ],
            "array_delivered_wetted": assessment_am_dict["array_delivered_wetted"],
            "array_captured_mass": assessment_am_dict["array_captured_mass"],
            "device_captured_mass": assessment_am_dict["device_captured_mass"],
            "array_transformed_mass": assessment_am_dict["array_transformed_mass"],
            "device_transformed_mass": assessment_am_dict["device_transformed_mass"],
            "array_delivered_mass": assessment_am_dict["array_delivered_mass"],
            "PWR": assessment_am_dict["PWR"],
            "array_CL": assessment_am_dict["array_CL"],
            "device_CL": assessment_am_dict["device_CL"],
            "array_CL_rated_power": assessment_am_dict["array_CL_rated_power"],
            "device_CL_rated_power": assessment_am_dict["device_CL_rated_power"],
            "array_CL_ratio": assessment_am_dict["array_CL_ratio"],
            "device_CL_ratio": assessment_am_dict["device_CL_ratio"],
            "IA_ratio": assessment_am_dict["IA_ratio"],
            "Export_ratio": assessment_am_dict["Export_ratio"],
            "Cable_ratio": assessment_am_dict["Cable_ratio"],
            "array_captured_lease": assessment_am_dict["array_captured_lease"],
            "array_transformed_lease": assessment_am_dict["array_transformed_lease"],
            "array_delivered_lease": assessment_am_dict["array_delivered_lease"],
        }

        am_get = {"Inputs_AM": Inputs_AM, "Outputs_AM": Outputs_AM}

        if assessment:
            return make_response(am_get, 200, headers)
        # else:
        #     return (jsonify({"status": "fail", "message": assessment_am_dict}), 404)
        # raise IndexError("The specified ID does not exist in the DB")
    # except IndexError as e:
    #     return jsonify({"status": "fail", "message": str(e)}), 404
    except:
        return jsonify({"status": "fail", "message": "Unknown Reason"}), 404


@bp_spey.route("/<speyId>/energy-production", methods=["GET"])
def get_energy_production(speyId):
    """
        Get the Energy Production of a Project
    """

    try:

        assessment = Assessment_Table.query.get(speyId)
        inputs = assessment.inputs_table
        assessment_ep = inputs.energy_production
        ep_schema = EnergyProductionSchema()
        assessment_ep_dict = ep_schema.dump(assessment_ep)

        Inputs_EP = {
            "monthly_resource_histogram": assessment_ep_dict[
                "monthly_resource_histogram"
            ],
            "power_delivery_histogram": assessment_ep_dict["power_delivery_histogram"],
            "number_devices": assessment_ep_dict["number_devices"],
            "project_life": assessment_ep_dict["project_life"],
            "downtime_hours_per_device": assessment_ep_dict[
                "downtime_hours_per_device"
            ],
        }

        Outputs_EP = {
            "array_lifetime_gross_energy_pd": assessment_ep_dict[
                "array_lifetime_gross_energy_pd"
            ],
            "array_annual_gross_energy_pd": assessment_ep_dict[
                "array_annual_gross_energy_pd"
            ],
            "array_monthly_gross_energy_pd": assessment_ep_dict[
                "array_monthly_gross_energy_pd"
            ],
            "device_lifetime_gross_energy_pd": assessment_ep_dict[
                "device_lifetime_gross_energy_pd"
            ],
            "device_annual_gross_energy_pd": assessment_ep_dict[
                "device_annual_gross_energy_pd"
            ],
            "device_monthly_gross_energy_pd": assessment_ep_dict[
                "device_monthly_gross_energy_pd"
            ],
            "array_lifetime_net_energy_pd": assessment_ep_dict[
                "array_lifetime_net_energy_pd"
            ],
            "array_annual_net_energy_pd": assessment_ep_dict[
                "array_annual_net_energy_pd"
            ],
            "array_monthly_net_energy_pd": assessment_ep_dict[
                "array_monthly_net_energy_pd"
            ],
            "device_lifetime_net_energy_pd": assessment_ep_dict[
                "device_lifetime_net_energy_pd"
            ],
            "device_annual_net_energy_pd": assessment_ep_dict[
                "device_annual_net_energy_pd"
            ],
            "device_monthly_net_energy_pd": assessment_ep_dict[
                "device_monthly_net_energy_pd"
            ],
            "array_lifetime_lost_energy_pd": assessment_ep_dict[
                "array_lifetime_lost_energy_pd"
            ],
            "array_annual_lost_energy_pd": assessment_ep_dict[
                "array_annual_lost_energy_pd"
            ],
            "array_monthly_lost_energy_pd": assessment_ep_dict[
                "array_monthly_lost_energy_pd"
            ],
            "device_lifetime_lost_energy_pd": assessment_ep_dict[
                "device_lifetime_lost_energy_pd"
            ],
            "device_annual_lost_energy_pd": assessment_ep_dict[
                "device_annual_lost_energy_pd"
            ],
            "device_monthly_lost_energy_pd": assessment_ep_dict[
                "device_monthly_lost_energy_pd"
            ],
            "array_lifetime_net_ratio_pd": assessment_ep_dict[
                "array_lifetime_net_ratio_pd"
            ],
            "array_annual_net_ratio_pd": assessment_ep_dict[
                "array_annual_net_ratio_pd"
            ],
            "array_monthly_net_ratio_pd": assessment_ep_dict[
                "array_monthly_net_ratio_pd"
            ],
            "device_lifetime_net_ratio_pd": assessment_ep_dict[
                "device_lifetime_net_ratio_pd"
            ],
            "device_annual_net_ratio_pd": assessment_ep_dict[
                "device_annual_net_ratio_pd"
            ],
            "device_monthly_net_ratio_pd": assessment_ep_dict[
                "device_monthly_net_ratio_pd"
            ],
            "array_lifetime_lost_ratio_pd": assessment_ep_dict[
                "array_lifetime_lost_ratio_pd"
            ],
            "array_annual_lost_ratio_pd": assessment_ep_dict[
                "array_annual_lost_ratio_pd"
            ],
            "array_monthly_lost_ratio_pd": assessment_ep_dict[
                "array_monthly_lost_ratio_pd"
            ],
            "device_lifetime_lost_ratio_pd": assessment_ep_dict[
                "device_lifetime_lost_ratio_pd"
            ],
            "device_annual_lost_ratio_pd": assessment_ep_dict[
                "device_annual_lost_ratio_pd"
            ],
            "device_monthly_lost_ratio_pd": assessment_ep_dict[
                "device_monthly_lost_ratio_pd"
            ],
        }

        ep_get = {"Inputs_EP": Inputs_EP, "Outputs_EP": Outputs_EP}

        if assessment:
            return make_response(ep_get, 200, headers)
        # else:
        # return (jsonify({"status": "fail", "message": ep_get}), 404)
        # raise IndexError("The specified ID does not exist in the DB")
    # except IndexError as e:
    #     return jsonify({"status": "fail", "message": str(e)}), 404
    except:
        return jsonify({"status": "fail", "message": "Unknown Reason"}), 404
