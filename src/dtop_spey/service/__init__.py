# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# This file should contain create_app() function.
# This function is used by Flask.

import os

from flask import Flask, request, jsonify, render_template
from flask_babel import Babel
from flask_cors import CORS
from werkzeug.middleware.proxy_fix import ProxyFix
from dtop_spey.storage.config import Config


babel = Babel()


def create_app(test_config=None):
    # create and configure the app
    app = Flask(
        __name__,
        instance_relative_config=False,
        # static_folder="../gui/frontend/dist/static",
        # template_folder="../gui/frontend/dist",
    )
    app.config.from_object(Config)
    cors = CORS(app, resources={r"/*": {"origins": "*"}})

    # secret key is needed for session
    # app.secret_key = "dljsaklqk24e21cjn!Ew@@dsa5"

    babel.init_app(app)

    # if test_config is None:
    #     # load the instance config, if it exists, when not testing
    #     app.config.from_pyfile("config.py", silent=True)
    # else:
    #     # load the test config if passed in
    #     app.config.from_mapping(test_config)
    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from ..storage import db

    db.init_app(app)

    from ..storage.schemas.schemas import ma

    ma.init_app(app)

    # Registering Blueprints
    # from .api import core as core_api

    # app.register_blueprint(core_api.bp, url_prefix="/core")

    from .api import bp, core, assessment, representation

    app.register_blueprint(bp)

    app.register_blueprint(core.bp)

    app.register_blueprint(assessment.bp_spey, url_prefix="/spey")

    app.register_blueprint(representation.bp_spey)

    app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_host=1, x_prefix=1)

    if os.environ.get("FLASK_ENV") == "development":
        from .api.integration import provider_states

        app.register_blueprint(provider_states.bp)

    return app

    # This should be activated IF the front end is integrated without a separate port
    # @app.route('/', defaults={'path': ''})
    # @app.route('/<path:path>')
    # def render_vue(path):
    #     return render_template("index.html")

    # return app


@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(["en", "fr"])
