module.exports = {
  moduleFileExtensions: ['js', 'jsx', 'json', 'vue'],
  transform: {
      '^.+\\.vue$': 'vue-jest',
      '.+\\.(css|styl|less|sass|scss|svg|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
      '^.+\\.jsx?$': 'babel-jest'
  },
  moduleNameMapper: {
      '^@/(.*)$': '<rootDir>/src/$1'
  },
  snapshotSerializers: ['jest-serializer-vue'],
  testMatch: [
      '**/tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'
  ],
  coverageThreshold: {
      global: {
          statements: 90.1,
          branches: 72.73,
          functions: 81.58,
          lines: 90.1
      }
  },
  collectCoverageFrom: [
      'src/views/stage_gate_studies/checklists/input/*',
      'src/views/stage_gate_studies/home/*',
      'src/views/stage_gate_studies/checklists/output/*',
      'src/views/stage_gate_studies/checklists/output/stage/*',
      'src/views/stage_gate_studies/*',
      'src/views/frameworks/view/*',
      'src/views/frameworks/*',
  ],
  coverageDirectory: '<rootDir>/tests/unit/coverage',
  'collectCoverage': true,
  'coverageReporters': [
      'html',
      'text-summary',
      'clover'
  ],

  reporters: [
      'default'
  ],
  testURL: 'http://localhost/'
}
