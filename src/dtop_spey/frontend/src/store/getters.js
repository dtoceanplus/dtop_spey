const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  speyId: state => state.spey.speyId,
  status: state => state.spey.status
}
export default getters
