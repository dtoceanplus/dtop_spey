const state = {
  speyId: '',
  status: '',
  // Load_For_Main_Module: 'False'
}

const mutations = {
  SET_ID: (state, speyId) => {
    state.speyId = speyId
  },
  SET_STATUS : (state, status) => {
    state.status = status
  },
  // set_Load_For_Main_Module: (state, Load_For_Main_Module) => {
  //   state.Load_For_Main_Module = Load_For_Main_Module
  // }
}

const actions = {
  setId({ commit }, { speyId }) {
    commit('SET_ID', speyId)
  },
  setStatus({ commit }, { status }) {
    commit('SET_STATUS', status)
  },
  // set_Load_For_Main_ModuleAction: ({commit, state}, newValue) => {
  //   commit('set_Load_For_Main_Module', newValue)
  //   return state.Load_For_Main_Module
  // }
}

export default {
  state,
  mutations,
  actions
}
