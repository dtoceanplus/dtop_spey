import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import { DTOP_DOMAIN, DTOP_PROTOCOL } from "@/dtop/dtopConsts"

export const constantRoutes = [
  // {
  //   path: '/login',
  //   component: () => import('@/views/login/index'),
  //   hidden: true
  // },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Home',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Home', icon: 'star' }
    }]
  },

  {
    path: '',
    name: 'spey-projects',
    component: Layout,
    redirect: '/spey',
    name: 'SPEY Studies',
    meta: { title: 'SPEY Studies', icon: 'table' },
    children: [{
      path: '/spey',
      hidden: true,
      // name: 'SpeyStudies',
      component: () => import('@/views/spey_studies/index'),
      meta: { title: 'List of Studies', icon: 'list' },

    },
    {
      path: '/spey-inputs',
      name: 'SpeyInputs',
      component: () => import('@/views/inputs_collector/index'),
      meta: { title: 'Collection of Inputs', icon: 'exit-fullscreen' }
    },
    {
      path: '/spey-outputs',
      name: 'SpeyOutputs',
      component: () => import('@/views/outputs/index'),
      meta: { title: 'Data and Results', icon: 'fullscreen' }
    }

    // {
    //   path: 'spey-inputs',
    //   // name: 'SpeyStudies',
    //   component: () => import('@/views/inputs_collector/index'),
    //   // meta: { title: 'Inputs Collector', icon: 'exit-fullscreen' }
    // },
    // {
    //   path: 'spey-outputs',
    //   // name: 'SpeyStudies',
    //   component: () => import('@/views/outputs/index'),
    //   // meta: { title: 'Inputs Collector', icon: 'exit-fullscreen' }
    // }
  ]
  },
  // {
  //   path: '',
  //   name: 'spey-inputs',
  //   component: Layout,
  //   redirect: '/spey-inputs',
  //   children: [{
  //     path: 'spey-inputs',
  //     // name: 'SpeyStudies',
  //     component: () => import('@/views/inputs_collector/index'),
  //     meta: { title: 'Inputs', icon: 'exit-fullscreen' }
  //   }]
  // },
  // {
  //   path: '',
  //   name: 'spey-outputs',
  //   component: Layout,
  //   redirect: '/spey-outputs',
  //   children: [{
  //     path: 'spey-outputs',
  //     // name: 'SpeyStudies',
  //     component: () => import('@/views/outputs/index'),
  //     meta: { title: 'Outputs', icon: 'exit-fullscreen' }
  //   }]
  // },
  // {
  //   path: '',
  //   component: Layout,
  //   redirect: '/efficiency/outputs',
  //   name: 'Efficiency',
  //   meta: { title: 'Efficiency', icon: 'example' },
  //   children: [
  //     {
  //       path: 'efficiency/inputs',
  //       name: 'Efficiency Inputs',
  //       component: () => import('@/views/inputs_efficiency/index'),
  //       meta: { title: 'Inputs', icon: 'exit-fullscreen' }
  //     },
  //     {
  //       path: 'efficiency/outputs',
  //       name: 'Efficiency Outputs',
  //       component: () => import('@/views/outputs_efficiency/index'),
  //       meta: { title: 'Outputs', icon: 'fullscreen' }
  //     },
  //     {
  //       path: 'outputs',
  //       name: 'Outputs',
  //       component: () => import('@/views/outputs/index'),
  //       meta: { title: 'Outputs', icon: 'fullscreen' }
  //     }
  //   ]
  // },
  // {
  //   path: '',
  //   component: Layout,
  //   redirect: '/alternative-metrics/outputs',
  //   name: 'Alternative Metrics',
  //   meta: { title: 'Alternative Metrics', icon: 'example' },
  //   children: [
  //     {
  //       path: 'alternative-metrics/inputs',
  //       name: 'Alternative Metrics Inputs',
  //       component: () => import('@/views/inputs_AM/index'),
  //       meta: { title: 'Inputs', icon: 'exit-fullscreen' }
  //     },
  //     {
  //       path: 'alternative-metrics/outputs',
  //       name: 'Alternative Metrics Outputs',
  //       component: () => import('@/views/outputs_AM/index'),
  //       meta: { title: 'Outputs', icon: 'fullscreen' }
  //     }
  //   ]
  // },
  // {
  //   path: '',
  //   component: Layout,
  //   redirect: '/power-quality/outputs',
  //   name: 'Power Quality',
  //   meta: { title: 'Power Quality', icon: 'example' },
  //   children: [
  //     {
  //       path: 'power-quality/inputs',
  //       name: 'Power Quality Inputs',
  //       component: () => import('@/views/inputs_PQ/index'),
  //       meta: { title: 'Inputs', icon: 'exit-fullscreen' }
  //     },
  //     {
  //       path: 'power-quality/outputs',
  //       name: 'Power Quality Outputs',
  //       component: () => import('@/views/outputs_PQ/index'),
  //       meta: { title: 'Outputs', icon: 'fullscreen' }
  //     }
  //   ]
  // },
  // {
  //   path: '',
  //   component: Layout,
  //   redirect: '/energy-production/outputs',
  //   name: 'Energy Production',
  //   meta: { title: 'Energy Production', icon: 'example' },
  //   children: [
  //     {
  //       path: 'energy-production/inputs',
  //       name: 'Energy Production Inputs',
  //       component: () => import('@/views/inputs_EP/index'),
  //       meta: { title: 'Inputs', icon: 'exit-fullscreen' }
  //     },
  //     {
  //       path: 'energy-production/outputs',
  //       name: 'Energy Production Outputs',
  //       component: () => import('@/views/outputs_EP/index'),
  //       meta: { title: 'Outputs', icon: 'fullscreen' }
  //     }
  //   ]
  // },
  {
    path: '/api',
    component: Layout,
    children: [
      {
        path: `${DTOP_PROTOCOL}//spey.${DTOP_DOMAIN}/api`,
        meta: { title: 'API', icon: 'documentation' }}

    ]
  },
  {
    path: '/external-link',
    component: Layout,
    children: [
      {
        path: 'https://dtoceanplus.eu',
        meta: { title: 'DTOcean+', icon: 'link' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
