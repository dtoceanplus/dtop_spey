# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from sqlalchemy.ext import mutable
from sqlalchemy.types import TypeDecorator
import json

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Assessment_Table(db.Model):
    __tablename__ = "assessment_table"
    speyId = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    date = db.Column(db.String(50))
    status = db.Column(db.Integer)
    description = db.Column(db.String(50), nullable=False)
    scId = db.Column(db.String(5))
    mcId = db.Column(db.String(5))
    ecId = db.Column(db.String(5))
    edId = db.Column(db.String(5))
    etId = db.Column(db.String(5))
    lmoId = db.Column(db.String(5))

    # one-to-one relationship with the inputs
    inputs_table = db.relationship(
        "Inputs_Table",
        uselist=False,
        back_populates="assessment_table",
        cascade="all, delete-orphan",
    )

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)


class Inputs_Table(db.Model):
    __tablename__ = "inputs_table"
    inputsId = db.Column(db.Integer, primary_key=True)
    spey_id = db.Column(
        db.Integer, db.ForeignKey("assessment_table.speyId", onupdate="cascade")
    )
    scId = db.Column(db.String(5))
    mcId = db.Column(db.String(5))
    ecId = db.Column(db.String(5))
    edId = db.Column(db.String(5))
    etId = db.Column(db.String(5))
    lmoId = db.Column(db.String(5))
    wave_EJPD = db.Column(db.JSON)
    current_scenarii = db.Column(db.JSON)
    wave_flux = db.Column(db.JSON)
    current_flux = db.Column(db.JSON)
    sc_farm = db.Column(db.JSON)
    ec_devices = db.Column(db.JSON)
    ec_farm = db.Column(db.JSON)
    et_results = db.Column(db.JSON)
    et_results_array = db.Column(db.JSON)
    ed_results = db.Column(db.JSON)
    downtime_hours = db.Column(db.JSON)
    device_characteristics_general = db.Column(db.JSON, nullable=False)
    device_characteristics_machine = db.Column(db.JSON)
    device_characteristics_dimensions = db.Column(db.JSON)

    assessment_table = db.relationship(
        "Assessment_Table", back_populates="inputs_table"
    )

    # one-to-one relationship with the efficiency
    efficiency = db.relationship(
        "Efficiency",
        uselist=False,
        back_populates="inputs_table",
        cascade="all, delete-orphan",
    )
    # one-to-one relationship with the alterantive metrics
    alternative_metrics = db.relationship(
        "Alternative_Metrics",
        uselist=False,
        back_populates="inputs_table",
        cascade="all, delete-orphan",
    )
    # one-to-one relationship with the representation
    power_quality = db.relationship(
        "Power_Quality",
        uselist=False,
        back_populates="inputs_table",
        cascade="all, delete-orphan",
    )

    # one-to-one relationship with the representation
    representation = db.relationship(
        "Representation",
        uselist=False,
        back_populates="inputs_table",
        cascade="all, delete-orphan",
    )

    # one-to-one relationship with the energy production
    energy_production = db.relationship(
        "Energy_Production",
        uselist=False,
        back_populates="inputs_table",
        cascade="all, delete-orphan",
    )

    # def update(self, **kwargs):
    #     for key, value in kwargs.items():
    #         setattr(self, key, value)


class Energy_Production(db.Model):
    __tablename__ = "energy_production"
    EP_id = db.Column(db.Integer, primary_key=True)
    inputsId = db.Column(
        db.Integer, db.ForeignKey("inputs_table.inputsId", onupdate="cascade")
    )
    monthly_resource_histogram = db.Column(db.JSON)
    power_delivery_histogram = db.Column(db.JSON, nullable=False)
    number_devices = db.Column(db.JSON)
    project_life = db.Column(db.JSON)
    downtime_hours_per_device = db.Column(db.JSON)

    array_lifetime_gross_energy_pd = db.Column(db.JSON)

    array_annual_gross_energy_pd = db.Column(db.JSON)

    array_monthly_gross_energy_pd = db.Column(db.JSON)

    device_lifetime_gross_energy_pd = db.Column(db.JSON)

    device_annual_gross_energy_pd = db.Column(db.JSON)

    device_monthly_gross_energy_pd = db.Column(db.JSON)

    array_lifetime_net_energy_pd = db.Column(db.JSON)

    array_annual_net_energy_pd = db.Column(db.JSON)

    array_monthly_net_energy_pd = db.Column(db.JSON)

    device_lifetime_net_energy_pd = db.Column(db.JSON)

    device_annual_net_energy_pd = db.Column(db.JSON)

    device_monthly_net_energy_pd = db.Column(db.JSON)

    array_lifetime_lost_energy_pd = db.Column(db.JSON)

    array_annual_lost_energy_pd = db.Column(db.JSON)

    array_monthly_lost_energy_pd = db.Column(db.JSON)

    device_lifetime_lost_energy_pd = db.Column(db.JSON)

    device_annual_lost_energy_pd = db.Column(db.JSON)

    device_monthly_lost_energy_pd = db.Column(db.JSON)

    array_lifetime_net_ratio_pd = db.Column(db.JSON)

    array_annual_net_ratio_pd = db.Column(db.JSON)

    array_monthly_net_ratio_pd = db.Column(db.JSON)

    device_lifetime_net_ratio_pd = db.Column(db.JSON)

    device_annual_net_ratio_pd = db.Column(db.JSON)

    device_monthly_net_ratio_pd = db.Column(db.JSON)

    array_lifetime_lost_ratio_pd = db.Column(db.JSON)

    array_annual_lost_ratio_pd = db.Column(db.JSON)

    array_monthly_lost_ratio_pd = db.Column(db.JSON)

    device_lifetime_lost_ratio_pd = db.Column(db.JSON)

    device_annual_lost_ratio_pd = db.Column(db.JSON)

    device_monthly_lost_ratio_pd = db.Column(db.JSON)

    inputs_table = db.relationship("Inputs_Table", back_populates="energy_production")

    # def update(self, **kwargs):
    #     for key, value in kwargs.items():
    #         setattr(self, key, value)


class Alternative_Metrics(db.Model):
    __tablename__ = "alternative_metrics"
    AM_id = db.Column(db.Integer, primary_key=True)
    inputsId = db.Column(
        db.Integer, db.ForeignKey("inputs_table.inputsId", onupdate="cascade")
    )

    average_flux = db.Column(db.JSON)
    tech_type = db.Column(db.JSON)
    number_devices = db.Column(db.JSON)
    rated_power = db.Column(db.JSON)
    charact_dimension = db.Column(db.JSON)
    wetted_area_device = db.Column(db.JSON)
    device_mass = db.Column(db.JSON)
    lease_area = db.Column(db.JSON)
    annual_captured_energy = db.Column(db.JSON)
    annual_transformed_energy = db.Column(db.JSON)
    annual_delivered_energy = db.Column(db.JSON)
    device_annual_captured_energy = db.Column(db.JSON)
    device_annual_transformed_energy = db.Column(db.JSON)
    export_cable_length = db.Column(db.JSON)
    intra_array_cable_length = db.Column(db.JSON)

    array_captured_wetted = db.Column(db.JSON)
    device_captured_wetted = db.Column(db.JSON)
    array_transformed_wetted = db.Column(db.JSON)
    device_transformed_wetted = db.Column(db.JSON)
    array_delivered_wetted = db.Column(db.JSON)

    array_captured_mass = db.Column(db.JSON)
    device_captured_mass = db.Column(db.JSON)
    array_transformed_mass = db.Column(db.JSON)
    device_transformed_mass = db.Column(db.JSON)
    array_delivered_mass = db.Column(db.JSON)

    PWR = db.Column(db.JSON)

    array_CL = db.Column(db.JSON)
    device_CL = db.Column(db.JSON)
    array_CL_rated_power = db.Column(db.JSON)
    device_CL_rated_power = db.Column(db.JSON)
    array_CL_ratio = db.Column(db.JSON)
    device_CL_ratio = db.Column(db.JSON)

    IA_ratio = db.Column(db.JSON)

    Export_ratio = db.Column(db.JSON)

    Cable_ratio = db.Column(db.JSON)

    array_captured_lease = db.Column(db.JSON)

    array_transformed_lease = db.Column(db.JSON)

    array_delivered_lease = db.Column(db.JSON)

    inputs_table = db.relationship("Inputs_Table", back_populates="alternative_metrics")

    # def update(self, **kwargs):
    #     for key, value in kwargs.items():
    #         setattr(self, key, value)


class Power_Quality(db.Model):
    __tablename__ = "power_quality"
    PQ_id = db.Column(db.Integer, primary_key=True)
    inputsId = db.Column(
        db.Integer, db.ForeignKey("inputs_table.inputsId", onupdate="cascade")
    )
    transformed_active_power = db.Column(db.JSON)
    transformed_reactive_power = db.Column(db.JSON)
    delivered_active_power = db.Column(db.JSON)
    delivered_reactive_power = db.Column(db.JSON)

    device_transformed_phase = db.Column(db.JSON)
    array_transformed_phase = db.Column(db.JSON)
    array_delivered_phase = db.Column(db.JSON)

    inputs_table = db.relationship("Inputs_Table", back_populates="power_quality")

    # def update(self, **kwargs):
    #     for key, value in kwargs.items():
    #         setattr(self, key, value)


class Efficiency(db.Model):
    __tablename__ = "efficiency"
    EFF_id = db.Column(db.Integer, primary_key=True)
    inputsId = db.Column(
        db.Integer, db.ForeignKey("inputs_table.inputsId", onupdate="cascade")
    )
    average_flux = db.Column(db.JSON)
    tech_type = db.Column(db.JSON, nullable=False)
    number_devices = db.Column(db.JSON)
    rated_power = db.Column(db.JSON)
    charact_dimension = db.Column(db.JSON)
    annual_captured_energy = db.Column(db.JSON)
    annual_transformed_energy = db.Column(db.JSON)
    annual_delivered_energy = db.Column(db.JSON)
    q_factor = db.Column(db.JSON)
    device_annual_captured_energy = db.Column(db.JSON)
    device_annual_transformed_energy = db.Column(db.JSON)
    device_q_factor = db.Column(db.JSON)

    rated_power_flux = db.Column(db.JSON)

    array_capt_eff = db.Column(db.JSON)

    device_capt_eff = db.Column(db.JSON)

    array_abs_transf_eff = db.Column(db.JSON)

    device_abs_transf_eff = db.Column(db.JSON)

    array_rel_transf_eff = db.Column(db.JSON)

    device_rel_transf_eff = db.Column(db.JSON)

    array_abs_deliv_eff = db.Column(db.JSON)

    array_rel_deliv_eff = db.Column(db.JSON)

    array_q_f = db.Column(db.JSON)

    device_q_f = db.Column(db.JSON)

    inputs_table = db.relationship("Inputs_Table", back_populates="efficiency")

    # def update(self, **kwargs):
    #     for key, value in kwargs.items():
    #         setattr(self, key, value)


class Representation(db.Model):
    __tablename__ = "representation"
    repr_id = db.Column(db.Integer, primary_key=True)
    inputsId = db.Column(
        db.Integer, db.ForeignKey("inputs_table.inputsId", onupdate="cascade")
    )
    inputs_table = db.relationship("Inputs_Table", back_populates="representation")

    array = db.Column(db.JSON)
    device = db.Column(db.JSON)
    transmission_to_shore = db.Column(db.JSON)
    array_network = db.Column(db.JSON)
    power_transmission = db.Column(db.JSON)

    # def update(self, **kwargs):
    #     for key, value in kwargs.items():
    #         setattr(self, key, value)

    # @staticmethod
    # def to_camel_case(snake_str):
    #     components = snake_str.split("-")
    #     if len(components) > 1:
    #         # capitalize the first letter of each component except the first one
    #         # with the 'title' method and join them together.
    #         return components[0] + "".join(x.title() for x in components[1:])
    #     return components[0]

    # def update(self, **kwargs):
    #     for key, value in kwargs.items():
    #         key_camel = self.to_camel_case(key)
    #         setattr(self, key_camel, value)
