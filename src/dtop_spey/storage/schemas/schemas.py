# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# from dtop_energycapt.service import ma
from ..models.models import (
    Assessment_Table,
    Inputs_Table,
    Energy_Production,
    Alternative_Metrics,
    Power_Quality,
    Efficiency,
    Representation,
)

# from..models.models import Assessment


from flask_marshmallow import Marshmallow
from marshmallow import post_dump, post_load

ma = Marshmallow()


class AssessmentSchema(ma.ModelSchema):
    class Meta:
        model = Assessment_Table


class InputsSchema(ma.ModelSchema):
    class Meta:
        model = Inputs_Table


class EfficiencySchema(ma.ModelSchema):
    class Meta:
        model = Efficiency


class AlternativeMetricsSchema(ma.ModelSchema):
    class Meta:
        model = Alternative_Metrics


class PowerQualitySchema(ma.ModelSchema):
    class Meta:
        model = Power_Quality


class EnergyProductionSchema(ma.ModelSchema):
    class Meta:
        model = Energy_Production


class RepresentationSchema(ma.ModelSchema):
    class Meta:
        model = Representation
