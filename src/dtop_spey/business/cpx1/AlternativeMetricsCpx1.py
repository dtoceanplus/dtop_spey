# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt


class AlternativeMetricsCpx1(object):
    """This class assesses a set of *alternative metrics* of the Ocean Energy System 
    at the various subsystems and globally for projects at Level 1 of complexity.
    
    Args:
        average_flux (float): Average Annual Flux Resource available in the site.
        tech_type (str): Tidal or Wave Energy device.
        charact_dimension (float): Tidal or Wave Energy device.
        number_devices (int): number of devices.
        rated_power (float): rated power of the Device.
        wetted_area_device (float): the wetted area of the Tidal or Wave Energy converter.
        device_mass (float): mass of the device.
        lease_area (float): lease area (escluding no-go zones).
        annual_captured_energy (float, optional): Annual Array Captured Energy by the 
            devices because of the hydrodynamic interactions. Defaults to None.
        device_annual_captured_energy (list of float, optional): Annual Device Captured Energy by 
            the devices because of the hydrodynamic interactions. Defaults to None.
        annual_transformed_energy (float, optional): Annual Array Transformed Energy after 
            the PTO/control system. Defaults to None.
        device_annual_transformed_energy (list of float, optional): Annual Device Transformed Energy
            after the PTO/control system. Defaults to None.
        PTO_force(float, optional): the average of the RMS of the PTO force. Defaults to None.
        annual_delivered_energy(float, optional): Annual Array Delivered Energy after
            the Electrical Dispatch system. Defaults to None.
        export_cable_length(float, optional): the total lenght of the export cable
        intra_array_cable_length(float, optional): the total length of the intra-array cable
                system. Defaults to None.

    Attributes:
        array_captured_wetted (float): the ratio between the Array Annual 
            Captured Energy and the wetted surface.  Defaults to None.
        device_captured_wetted (list of float): the ratio between the Annual Captured 
            Energy per Device and the wetted surface.  Defaults to None.
        array_transformed_wetted (float): the ratio between the Array Annual 
            Transformed Energy and the wetted surface. Defaults to None.
        device_transformed_wetted (list of float): the ratio between the Annual 
            Transformed Energy per Device and the wetted surface.  Defaults to None.
        array_delivered_wetted (float): the ratio between the Array Annual 
            Delivered Energy and the wetted surface.  Defaults to None.
        array_captured_mass (float): the ratio between the Array Annual 
            Captured Energy and the mass of the device. Defaults to None.
        device_captured_mass (list of float): the ratio between the Captured per device 
            Energy and the mass of the device.  Defaults to None.
        array_transformed_mass (float): the ratio between the Array Annual 
            transformed Energy and the mass of the device.  Defaults to None.
        device_transformed_mass (list of float): the ratio between the  Annual transformed
            per device Energy and the mass of the device.  Defaults to None.
        array_delivered_mass (float): the ratio between the Array Annual Delivered
            Energy and the mass of the device.  Defaults to None.
        PWR (float): Power to Weight Ratio.  Defaults to None.
        array_CL (float): the Array Capture Length.  Defaults to None.
        device_CL (list of float): the  Capture Length per device (wave energy).  Defaults to None.
        array_CL_rated_power (float): the array Capture Length per installed capacity.  Defaults to None.
        device_CL_rated_power (list of float): the  Capture Length per device  per 
            installed capacity. Defaults to None.
        array_CL_ratio (float): the array Capture Length divided by the 
            characteristic length.  Defaults to None.
        device_CL_ratio: the  Capture Length per device divided by the
            characteristic length.  Defaults to None.
        IA_ratio (float): the ratio between the total intra array length 
            and the rated power of the array.  Defaults to None.
        Export_ratio (float): the ratio between the total length of the export 
            cable and the rated power of the array.  Defaults to None.
        Cable_ratio (float): the ratio between the total length of the  cables
                (intra array and export) and the rated power of the array.  Defaults to None.
        array_captured_lease (float): the ratio between the Array Annual 
            Captured Energy and the lease area.  Defaults to None.
        array_transformed_lease (float): the ratio between the Array Annual 
            transformed Energy and the lease area.  Defaults to None.
        array_delivered_lease (float): the ratio between the Array Annual 
            Delivered Energy and the lease area.  Defaults to None.
    """

    cpx = "Level of Complexity 1"

    def __init__(
        self,
        average_flux,
        tech_type,
        charact_dimension,
        number_devices,
        rated_power,
        wetted_area_device,
        device_mass,
        annual_captured_energy=None,
        device_annual_captured_energy=None,
        annual_transformed_energy=None,
        device_annual_transformed_energy=None,
        PTO_force=None,
        annual_delivered_energy=None,
        export_cable_length=None,
        intra_array_cable_length=None,
        lease_area=None,
    ):

        self.AF = average_flux
        self.ADE = annual_delivered_energy
        self.ATE = annual_transformed_energy
        self.ACE = annual_captured_energy
        self.DATE = device_annual_transformed_energy
        self.DACE = device_annual_captured_energy
        self.LA = lease_area

        self.type = tech_type
        self.charact_dimension = charact_dimension
        self.number_devices = number_devices
        self.rated_power = rated_power
        self.WAD = wetted_area_device
        self.mass = device_mass
        self.ECL = export_cable_length
        self.IACL = intra_array_cable_length

        self.PTO_force = PTO_force

        self.array_captured_wetted = None
        self.device_captured_wetted = None
        self.array_transformed_wetted = None
        self.device_transformed_wetted = None
        self.array_delivered_wetted = None

        self.array_captured_mass = None
        self.device_captured_mass = None
        self.array_transformed_mass = None
        self.device_transformed_mass = None
        self.array_delivered_mass = None

        self.PWR = None
        self.array_CL = None
        self.device_CL = None
        self.array_CL_rated_power = None
        self.device_CL_rated_power = None
        self.array_CL_ratio = None
        self.device_CL_ratio = None

        self.IA_ratio = None
        self.Export_ratio = None
        self.Cable_ratio = None

        self.array_captured_lease = None
        self.array_transformed_lease = None
        self.array_delivered_lease = None
        self.inputs = {}
        self.outputs = {}

    def get_inputs(self):
        """
            Returns the inputs used for this class
        """
        units_af = {"Tidal": "kW/m^2", "Wave": "kW/m"}
        self.inputs = {
            "tech_type": {
                "value": self.type,
                "Label": "Technology Type",
                "Unit": "-",
                "Description": "Tidal or Wave Energy Converter",
            },
            "charact_dimension": {
                "value": self.charact_dimension,
                "Label": "Characteristic Dimension",
                "Unit": "m",
                "Description": "Charcteristic Dimension of the Prime Mover",
            },
            "number_devices": {
                "value": self.number_devices,
                "Label": "Number of Devices",
                "Unit": "-",
                "Description": "Number of Devices",
            },
            "rated_power": {
                "value": self.rated_power,
                "Label": "Rated Capacity of the Device",
                "Unit": "kW",
                "Description": "Rated Capacity of the Device",
            },
            "average_flux": {
                "value": self.AF,
                "Label": "Annual Average flux of Energy",
                "Unit": units_af[self.type],
                "Description": "Annual Average flux of Energy",
            },
            "annual_captured_energy": {
                "value": self.ACE,
                "Label": "Annual captured energy",
                "Unit": "kWh",
                "Description": "Annual captured energy",
            },
            "device_annual_captured_energy": {
                "value": self.DACE,
                "Label": "Device Annual captured energy",
                "Unit": "kWh",
                "Description": "Annual captured energy  per device",
            },
            "annual_transformed_energy": {
                "value": self.ATE,
                "Label": "Annual transformed energy",
                "Unit": "kWh",
                "Description": "Annual Energy after transformation stage",
            },
            "device_annual_transformed_energy": {
                "value": self.DATE,
                "Label": "Device Annual transformed energy",
                "Unit": "kWh",
                "Description": "Annual Energy after transformation stage per device",
            },
            "annual_delivered_energy": {
                "value": self.ADE,
                "Label": "Annual Delivered Energy",
                "Unit": "kWh",
                "Description": "Annual nergy delivered onshore",
            },
            "wetted_area_device": {
                "value": self.WAD,
                "Label": "Wetted area of the Device",
                "Unit": "-",
                "Description": "Wetted surface of the Prime Mover",
            },
            "export_cable_length": {
                "value": self.ECL,
                "Label": "Export Cable Length",
                "Unit": "m",
                "Description": "Length of the export cable",
            },
            "intra_array_cable_length": {
                "value": self.IACL,
                "Label": "Intra Array Cable Length",
                "Unit": "m",
                "Description": "Length of the cables in the network",
            },
            "device_mass": {
                "value": self.mass,
                "Label": "Device Mass",
                "Unit": "kg",
                "Description": "Mass of the Prime Mover",
            },
            "lease_area": {
                "value": self.LA,
                "Label": "Lease area extension",
                "Unit": "km^2",
                "Description": "Extension of the lease area",
            },
        }
        return {
            "Level of Complexity": self.cpx,
            "Technology Type": self.type,
            "Characteristic Dimension": self.charact_dimension,
            "Number of devices": self.number_devices,
            "Device Rated Power": self.rated_power,
            "Annual Flux": self.AF,
            "Annual Array Captured Energy": self.ACE,
            "Annual Captured Energy per Device": self.DACE,
            "Annual Array Transformed Energy": self.ATE,
            "Annual Transformed Energy per Device": self.DATE,
            "Annual Array Delivered Energy": self.ADE,
            "Device Wetted Area": self.WAD,
            "RMS of the PTO force": self.PTO_force,
            "Total Export Cable Length": self.ECL,
            "Total Intra Array Cable Length": self.IACL,
            "Device Mass": self.mass,
            "Lease Area": self.LA,
        }

    def wetted_area_parameters(self):
        """ 
            Returns the metrics based on the wetted area
            Raises:
                ZeroDivisionError: If wetted area or Number of Devices are zero.
                TypeError: If Device/Array captured/transformed/delivered Energy is None.
        """

        try:
            self.array_captured_wetted = self.ACE / (self.WAD * self.number_devices)

        except ZeroDivisionError as err:
            print("Infinity: Wetted Area or Number of Devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Wetted Area or Number of Devices cannot be null."
            )

        except TypeError:
            print("NoneType: Annual Array Captured Energy is None.")

        try:
            self.device_captured_wetted = [
                self.DACE[i] / (self.WAD) for i in range(len(self.DACE))
            ]

        except ZeroDivisionError:
            print("Infinity: Wetted Area cannot be null.")
            raise ZeroDivisionError("Infinity: Wetted Area cannot be null.")

        except TypeError:
            print("NoneType: Annual Device Captured Energy is None.")

        try:
            self.array_transformed_wetted = self.ATE / (self.WAD * self.number_devices)

        except ZeroDivisionError:
            print("Infinity: Wetted Area or Number of Devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Wetted Area or Number of Devices cannot be null."
            )
        except TypeError:
            print("NoneType: Annual Array Transformed Energy is None.")

        try:
            self.device_transformed_wetted = [
                self.DATE[i] / (self.WAD) for i in range(len(self.DATE))
            ]

        except ZeroDivisionError:
            print("Infinity: Wetted Area cannot be null.")
            raise ZeroDivisionError("Infinity: Wetted Area cannot be null.")

        except TypeError:
            print("NoneType: Annual Device Transformed Energy is None.")

        try:
            self.array_delivered_wetted = self.ADE / (self.WAD * self.number_devices)

        except ZeroDivisionError:
            print("Infinity: Wetted Area or Number of Devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Wetted Area or Number of Devices cannot be null."
            )

        except TypeError:
            print("NoneType: Annual Array Delivered Energy is None.")

        self.outputs["array_captured_wetted"] = {
            "value": self.array_captured_wetted,
            "Aggregation-Level": "array",
            "Label": "Array Captured Energy per Wetted Surface",
            "Unit": "kWh/m^2",
            "Description": "Ratio between the Array Captured Energy and the wetted surface of the prime mover",
            "Tag": "Wetted Surface",
        }
        self.outputs["device_captured_wetted"] = {
            "value": self.device_captured_wetted,
            "Aggregation-Level": "device",
            "Label": "Device Captured Energy per Wetted Surface",
            "Unit": "kWh/m^2",
            "Description": "Ratio between the Captured Energy per device and the wetted surface of the prime mover",
            "Tag": "Wetted Surface",
        }

        self.outputs["array_transformed_wetted"] = {
            "value": self.array_transformed_wetted,
            "Aggregation-Level": "array",
            "Label": "Array Transformed Energy per Wetted Surface",
            "Unit": "kWh/m^2",
            "Description": "Ratio between the Array Transformed Energy and the wetted surface of the prime mover",
            "Tag": "Wetted Surface",
        }
        self.outputs["device_transformed_wetted"] = {
            "value": self.device_transformed_wetted,
            "Aggregation-Level": "device",
            "Label": "Device Transformed Energy per Wetted Surface",
            "Unit": "kWh/m^2",
            "Description": "Ratio between the Transformed Energy per device and the wetted surface of the prime mover",
            "Tag": "Wetted Surface",
        }

        self.outputs["array_delivered_wetted"] = {
            "value": self.array_delivered_wetted,
            "Aggregation-Level": "array",
            "Label": "Array Delivered Energy per Wetted Surface",
            "Unit": "kWh/m^2",
            "Description": "Ratio between the Array Delivered Energy and the wetted surface of the prime mover",
            "Tag": "Wetted Surface",
        }

        return (
            self.array_captured_wetted,
            self.device_captured_wetted,
            self.array_transformed_wetted,
            self.device_transformed_wetted,
            self.array_delivered_wetted,
        )

    def mass_parameters(self):
        """ 
            Returns the metrics based on the mass of the prime mover
            Raises:
                ZeroDivisionError: If mass or Number of Devices are zero.
                TypeError: If Device/Array captured/transformed/delivered Energy is None.
        """

        try:
            self.array_captured_mass = self.ACE / (self.mass * self.number_devices)

        except ZeroDivisionError:
            print("Infinity: Mass or Number of Devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Mass or Number of Devices cannot be null."
            )

        except TypeError:
            print("NoneType: Annual Array Captured Energy is None.")

        try:
            self.device_captured_mass = [
                self.DACE[i] / (self.mass) for i in range(len(self.DACE))
            ]

        except ZeroDivisionError:
            print("Infinity: Mass or Number of Devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Mass or Number of Devices cannot be null."
            )

        except TypeError:
            print("NoneType: Annual Device Captured Energy is None.")

        try:
            self.array_transformed_mass = self.ATE / (self.mass * self.number_devices)

        except ZeroDivisionError:
            print("Infinity: Mass or Number of Devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Mass or Number of Devices cannot be null."
            )
        except TypeError:
            print("NoneType: Annual Array Transformed Energy is None.")

        try:
            self.device_transformed_mass = [
                self.DATE[i] / (self.mass) for i in range(len(self.DATE))
            ]

        except ZeroDivisionError:
            print("Infinity: Mass  cannot be null.")
            raise ZeroDivisionError("Infinity: Mass  cannot be null.")
        except TypeError:
            print("NoneType: Annual Device Transformed Energy is None.")

        try:
            self.array_delivered_mass = self.ADE / (self.mass * self.number_devices)

        except ZeroDivisionError:
            print("Infinity: Mass or Number of Devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Mass or Number of Devices cannot be null."
            )

        except TypeError:
            print("NoneType: Annual Array Delivered Energy is None.")
        self.outputs["array_captured_mass"] = {
            "value": self.array_captured_mass,
            "Aggregation-Level": "array",
            "Label": "Array Captured Energy per unit of Mass",
            "Unit": "kWh/kg",
            "Description": "Ratio between the Array Captured Energy and the mass of the prime mover",
            "Tag": "Mass",
        }
        self.outputs["device_captured_mass"] = {
            "value": self.device_captured_mass,
            "Aggregation-Level": "device",
            "Label": "Device Captured Energy per unit of Mass",
            "Unit": "kWh/kg",
            "Description": "Ratio between the Captured Energy per device and the mass of the prime mover",
            "Tag": "Mass",
        }
        self.outputs["array_transformed_mass"] = {
            "value": self.array_transformed_mass,
            "Aggregation-Level": "array",
            "Label": "Array Transformed Energy per unit of Mass",
            "Unit": "kWh/kg",
            "Description": "Ratio between the Array Transformed Energy and the mass of the prime mover",
            "Tag": "Mass",
        }
        self.outputs["device_transformed_mass"] = {
            "value": self.device_transformed_mass,
            "Aggregation-Level": "device",
            "Label": "Device Transformed Energy per unit of Mass",
            "Unit": "kWh/kg",
            "Description": "Ratio between the Transformed Energy per device and the mass of the prime mover",
            "Tag": "Mass",
        }
        self.outputs["array_delivered_mass"] = {
            "value": self.array_delivered_mass,
            "Aggregation-Level": "array",
            "Label": "Array Delivered Energy per unit of Mass",
            "Unit": "kWh/kg",
            "Description": "Ratio between the Array Delivered Energy and the mass of the prime mover",
            "Tag": "Mass",
        }

        return (
            self.array_captured_mass,
            self.device_captured_mass,
            self.array_transformed_mass,
            self.device_transformed_mass,
            self.array_delivered_mass,
        )

    def calculate_PWR(self):
        """
            Returns the metrics based on the PWR (power to weight ratio)
        """

        self.PWR = self.rated_power / self.mass
        self.outputs["PWR"] = {
            "value": self.PWR,
            "Aggregation-Level": "device",
            "Label": "PWR - Power to Weight Ratio",
            "Unit": "kW/kg",
            "Description": "Ratio between the Rated Power and the mass of the prime mover",
            "Tag": "Mass",
        }
        return self.PWR

    def CL_parameters(self):
        """ 
            Returns the metrics based on the characteristic length
            Raises:
                ZeroDivisionError: If annual flux is zero.
                TypeError: If Device/Array captured/transformed/delivered Energy is None.
        """

        if self.type == "Wave":

            try:
                self.array_CL = self.ACE / (self.AF * 365.25 * 24.0)

            except ZeroDivisionError:
                print("Infinity: Annual Flux cannot be null.")
                raise ZeroDivisionError("Infinity: Annual Flux cannot be null.")

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.device_CL = [
                    self.DACE[i] / (self.AF * 365.25 * 24.0)
                    for i in range(len(self.DACE))
                ]

            except ZeroDivisionError:
                print("Infinity: Annual Flux cannot be null.")
                raise ZeroDivisionError("Infinity: Annual Flux cannot be null.")

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.array_CL_rated_power = self.ACE / (
                    self.AF * self.number_devices * self.rated_power * 365.25 * 24.0
                )

            except ZeroDivisionError:
                print(
                    "Infinity: Annual Flux, number of devices and rated power cannot be null."
                )
                raise ZeroDivisionError(
                    "Infinity: Annual Flux, number of devices and rated power cannot be null."
                )

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.device_CL_rated_power = [
                    self.DACE[i] / (self.AF * self.rated_power * 365.25 * 24.0)
                    for i in range(len(self.DACE))
                ]

            except ZeroDivisionError:
                print("Infinity: Annual Flux and/or rated power cannot be null.")
                raise ZeroDivisionError(
                    "Infinity: Annual Flux and/or rated power cannot be null."
                )

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.array_CL_ratio = self.ACE / (
                    self.AF
                    * self.number_devices
                    * self.charact_dimension
                    * 365.25
                    * 24.0
                )

            except ZeroDivisionError:
                print(
                    "Infinity: Annual Flux, number of devices and/or characteristic dimension cannot be null."
                )
                raise ZeroDivisionError(
                    "Infinity: Annual Flux, number of devices and/or characteristic dimension cannot be null."
                )

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.device_CL_ratio = [
                    self.DACE[i] / (self.AF * self.charact_dimension * 365.25 * 24.0)
                    for i in range(len(self.DACE))
                ]

            except ZeroDivisionError:
                print(
                    "Infinity: Annual Flux and/or characteristic dimension cannot be null."
                )
                raise ZeroDivisionError(
                    "Infinity: Annual Flux and/or characteristic dimension cannot be null."
                )

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

        if self.type == "Tidal":

            try:
                self.array_CL = np.sqrt(
                    4.0 * self.ACE / (np.pi * self.AF * 365.25 * 24.0)
                )

            except ZeroDivisionError:
                print("Infinity: Annual Flux cannot be null.")
                raise ZeroDivisionError("Infinity: Annual Flux cannot be null.")

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.device_CL = [
                    np.sqrt(4.0 * self.DACE[i] / (np.pi * self.AF * 365.25 * 24.0))
                    for i in range(len(self.DACE))
                ]

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.array_CL_rated_power = np.sqrt(
                    4.0
                    * self.ACE
                    / (
                        np.pi
                        * self.AF
                        * 365.25
                        * 24.0
                        * self.number_devices ** 2
                        * self.rated_power ** 2
                    )
                )

            except ZeroDivisionError:
                print(
                    "Infinity: Annual Flux, number of devices and rated power cannot be null."
                )
                raise ZeroDivisionError(
                    "Infinity: Annual Flux, number of devices and rated power cannot be null."
                )

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.device_CL_rated_power = [
                    np.sqrt(
                        4.0
                        * self.DACE[i]
                        / (np.pi * self.AF * 365.25 * 24.0 * self.rated_power ** 2)
                    )
                    for i in range(len(self.DACE))
                ]

            except ZeroDivisionError:
                print("Infinity: Annual Flux and/or rated power cannot be null.")
                raise ZeroDivisionError(
                    "Infinity: Annual Flux and/or rated power cannot be null."
                )

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.array_CL_ratio = np.sqrt(
                    4.0
                    * self.ACE
                    / (
                        np.pi
                        * self.AF
                        * 365.25
                        * 24.0
                        * self.charact_dimension ** 2
                        * self.number_devices ** 2
                    )
                )

            except ZeroDivisionError:
                print(
                    "Infinity: Annual Flux, number of devices and/or characteristic dimension cannot be null."
                )
                raise ZeroDivisionError(
                    "Infinity: Annual Flux, number of devices and/or characteristic dimension cannot be null."
                )

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

            try:
                self.device_CL_ratio = [
                    np.sqrt(
                        4.0
                        * self.DACE[i]
                        / (
                            np.pi
                            * self.AF
                            * 365.25
                            * 24.0
                            * self.charact_dimension ** 2
                        )
                    )
                    for i in range(len(self.DACE))
                ]

            except ZeroDivisionError:
                print(
                    "Infinity: Annual Flux and/or characteristic dimension cannot be null."
                )
                raise ZeroDivisionError(
                    "Infinity: Annual Flux and/or characteristic dimension cannot be null."
                )

            except TypeError:
                print("NoneType: Annual Array Captured Energy is None.")

        self.outputs["device_CL"] = {
            "value": self.device_CL,
            "Aggregation-Level": "device",
            "Label": "Device Capture length",
            "Unit": "m",
            "Description": "Ratio between the Captured Energy per device and the average annual flux",
            "Tag": "Capture length",
        }
        self.outputs["array_CL"] = {
            "value": self.array_CL,
            "Aggregation-Level": "array",
            "Label": "Array Capture length",
            "Unit": "m",
            "Description": "Ratio between the Array Captured Energy and the average annual flux",
            "Tag": "Capture length",
        }
        self.outputs["device_CL_rated_power"] = {
            "value": self.device_CL_rated_power,
            "Aggregation-Level": "device",
            "Label": "Device Capture length per Rated Power",
            "Unit": "m/kW",
            "Description": "Ratio between the device capture lengh and the rated power",
            "Tag": "Capture length",
        }
        self.outputs["array_CL_rated_power"] = {
            "value": self.array_CL_rated_power,
            "Aggregation-Level": "array",
            "Label": "Array Capture length per Rated Power",
            "Unit": "m/kW",
            "Description": "Ratio between the array capture lengh and the rated power",
            "Tag": "Capture length",
        }
        self.outputs["device_CL_ratio"] = {
            "value": self.device_CL_ratio,
            "Aggregation-Level": "device",
            "Label": "Device Capture length ratio",
            "Unit": "-",
            "Description": "Ratio between the device capture lenght and the characteristic length",
            "Tag": "Capture length",
        }
        self.outputs["array_CL_ratio"] = {
            "value": self.array_CL_ratio,
            "Aggregation-Level": "array",
            "Label": "Array Capture length ratio",
            "Unit": "-",
            "Description": "Ratio between the array capture lenght and the characteristic length of all the devices",
            "Tag": "Capture length",
        }
        return (
            self.device_CL,
            self.array_CL,
            self.device_CL_rated_power,
            self.array_CL_rated_power,
            self.device_CL_ratio,
            self.array_CL_ratio,
        )

    def cable_length_parameters(self):
        """ 
            Returns the metrics based on the intra and inter array cable lenghts
            Raises:
                ZeroDivisionError: If Rated Power and/or the number of devices are zero.
                TypeError: If Intra/export Array Cable Lenght is None is None.
        """
        try:
            self.IA_ratio = self.IACL / (self.rated_power * self.number_devices)

        except ZeroDivisionError:
            print("Infinity: Rated Power and/or the number of devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Rated Power and/or the number of devices cannot be null."
            )

        except TypeError:
            print("NoneType: Intra Array Cable Lenght is None.")

        try:

            self.Export_ratio = self.ECL / (self.rated_power * self.number_devices)

        except ZeroDivisionError:
            print("Infinity: Rated Power and/or the number of devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Rated Power and/or the number of devices cannot be null."
            )

        except TypeError:
            print("NoneType: Export Cable Lenght is None.")

        try:
            self.Cable_ratio = (self.IACL + self.ECL) / (
                self.rated_power * self.number_devices
            )

        except TypeError:
            print("NoneType: Intra Array Cable Lenght or Export Cable Length is None.")

        self.outputs["IA_ratio"] = {
            "value": self.IA_ratio,
            "Aggregation-Level": "array",
            "Label": "Intra-array cable ratio",
            "Unit": "kWh/m",
            "Description": "Ratio between the delivered energy and the intra-array cable length",
            "Tag": "Cable length",
        }
        self.outputs["Export_ratio"] = {
            "value": self.Export_ratio,
            "Aggregation-Level": "array",
            "Label": "Export cable ratio",
            "Unit": "kWh/m",
            "Description": "Ratio between the delivered energy and the export cable length",
            "Tag": "Cable length",
        }
        self.outputs["Cable_ratio"] = {
            "value": self.Cable_ratio,
            "Aggregation-Level": "array",
            "Label": "Total cable ratio",
            "Unit": "kWh/m",
            "Description": "Ratio between the delivered energy and the total cable length",
            "Tag": "Cable length",
        }

        return self.IA_ratio, self.Export_ratio, self.Cable_ratio

    def lease_area_parameters(self):
        """
            Returns the metrics based on the lease area extension
            Raises:
                ZeroDivisionError: If Lease area is zero.
                TypeError: If Device/Array captured/transformed/delivered Energy is None.
            
        """

        try:
            self.array_captured_lease = self.ACE / (self.LA)

        except ZeroDivisionError:
            print("Infinity: Lease Areea cannot be null.")
            raise ZeroDivisionError("Infinity: Lease Areea cannot be null.")

        except TypeError:
            print("NoneType: Annual Array Captured Energy is None.")

        try:
            self.array_transformed_lease = self.ATE / (self.LA)

        except ZeroDivisionError:
            print("Infinity: Lease Area cannot be null.")
            raise ZeroDivisionError("Infinity: Lease Area cannot be null.")
        except TypeError:
            print("NoneType: Annual Array Transformed Energy is None.")

        try:
            self.array_delivered_lease = self.ADE / (self.LA)

        except ZeroDivisionError:
            print("Infinity: Lease Area cannot be null.")
            raise ZeroDivisionError("Infinity: Lease Area cannot be null.")

        except TypeError:
            print("NoneType: Annual Array Delivered Energy is None.")

        self.outputs["array_captured_lease"] = {
            "value": self.array_captured_lease,
            "Aggregation-Level": "array",
            "Label": "Array Captured Energy per unit of surface",
            "Unit": "kWh/km^2",
            "Description": "Ratio between the captured energy and the surface extension of the lease area",
            "Tag": "Lease Area",
        }
        self.outputs["array_transformed_lease"] = {
            "value": self.array_transformed_lease,
            "Aggregation-Level": "array",
            "Label": "Array Transformed Energy per unit of surface",
            "Unit": "kWh/km^2",
            "Description": "Ratio between the transformed energy and the surface extension of the lease area",
            "Tag": "Lease Area",
        }
        self.outputs["array_delivered_lease"] = {
            "value": self.array_delivered_lease,
            "Aggregation-Level": "array",
            "Label": "Array Delivered Energy per unit of surface",
            "Unit": "kWh/km^2",
            "Description": "Ratio between the delivered energy and the surface extension of the lease area",
            "Tag": "Cable length",
        }

        return (
            self.array_captured_lease,
            self.array_transformed_lease,
            self.array_delivered_lease,
        )
