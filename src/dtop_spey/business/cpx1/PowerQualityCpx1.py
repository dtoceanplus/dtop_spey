# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
import numpy as np


class PowerQualityCpx1(object):
    """This class assesses the phase between reactive and active power after the transformation phase and the delivery phase at Level 1 of complexity.

    Args:
        transformed_active_power (pandas, optional): the active power after the 
            transformation process. Pandas table, columns are the devices and rows are the sea states
        transformed_reactive_power (pandas, optional): the reactive power after the transformation process. Pandas table, columns are the 
            devices and rows are the sea states
        delivered_active_power (pandas, optional): the active power after the delivery process. Pandas table, just one column (the total array) and rows are the sea states
        delivered_reactive_power (pandas, optional): the reactive power after the delivery process. Pandas table, just one column (the total array) and rows are the sea states 
    
    Attributes: 
        device_transformed_phase (pandas): pandas table of the phase (namely, the cosine fof the phase) between active and 
            reactive power, per device, per sea state. Columns are the device, rows are the sea states. 
        array_transformed_phase (pandas): pandas table (just one column) of the phase (namely, the cosine fo the phase) between active 
                and reactive power,    per sea state. Column is the Array, rows are the sea states.
        array_delivered_phase (pandas): pandas table (just one column) of the phase (namely, the cosine fof the phase) between active and
                reactive power,  per sea state. Column is the Array, rows are the sea states.       

    """

    cpx = "Level of Complexity 1"

    def __init__(
        self,
        transformed_active_power=None,
        transformed_reactive_power=None,
        delivered_active_power=None,
        delivered_reactive_power=None,
    ):

        self.transformed_active_power = transformed_active_power
        self.transformed_reactive_power = transformed_reactive_power
        self.delivered_active_power = delivered_active_power
        self.delivered_reactive_power = delivered_reactive_power

        self.device_transformed_phase = None
        self.array_transformed_phase = None
        self.array_delivered_phase = None
        self.device_transformed_phase_json = None
        self.array_transformed_phase_json = None
        self.array_delivered_phase_json = None
        self.inputs = {}
        self.outputs = {}

    def get_inputs(self):
        """
            Returns the inputs used for this class
        """
        self.inputs["transformed_active_power"] = {
            "Label": "Transformed Active Power",
            "Unit": "kW",
            "Description": "The active power per device at the Transformation stage",
        }
        if self.transformed_active_power is not None:
            self.inputs["transformed_active_power"][
                "value"
            ] = self.transformed_active_power.to_json()
        else:
            self.inputs["transformed_active_power"][
                "value"
            ] = self.transformed_active_power

        self.inputs["transformed_reactive_power"] = {
            "Label": "Transformed Reactive Power",
            "Unit": "kW",
            "Description": "The reactive power per device at the Transformation stage",
        }

        if self.transformed_reactive_power is not None:
            self.inputs["transformed_reactive_power"][
                "value"
            ] = self.transformed_reactive_power.to_json()
        else:
            self.inputs["transformed_reactive_power"][
                "value"
            ] = self.transformed_reactive_power

        self.inputs["delivered_active_power"] = {
            "Label": "Delivered Active Power",
            "Unit": "kW",
            "Description": "The active power at array level at the Delivery stage",
        }

        if self.delivered_active_power is not None:
            self.inputs["delivered_active_power"][
                "value"
            ] = self.delivered_active_power.to_json()
        else:
            self.inputs["delivered_active_power"]["value"] = self.delivered_active_power

        self.inputs["delivered_reactive_power"] = {
            "Label": "Delivered Rective Power",
            "Unit": "kW",
            "Description": "The reactive power at array level at the Delivery stage",
        }
        if self.delivered_reactive_power is not None:
            self.inputs["delivered_reactive_power"][
                "value"
            ] = self.delivered_reactive_power.to_json()
        else:
            self.inputs["delivered_reactive_power"][
                "value"
            ] = self.delivered_reactive_power

        return {
            "Level of Complexity": self.cpx,
            "Tranformed Active Power per Device": self.transformed_active_power,
            "Tranformed Reactive Power per Device": self.transformed_reactive_power,
            "Delivered Active Power of the array": self.delivered_active_power,
            "Delivered Rective Power of the array": self.delivered_reactive_power,
        }

    def transformed_phase(self):
        """
            Returns the phase at Energy transformation level
            Raises:
                TypeError: If Transformed Active and/or Reactive Power is None.
        """

        try:
            device_transformed_module = np.sqrt(
                self.transformed_active_power ** 2.0
                + self.transformed_reactive_power ** 2.0
            )
            self.device_transformed_phase = (
                self.transformed_active_power / device_transformed_module
            )

            self.device_transformed_phase_json = self.device_transformed_phase.to_json()

            array_transformed_active = self.transformed_active_power.sum(axis=1)
            array_transformed_reactive = self.transformed_reactive_power.sum(axis=1)

            array_transformed_module = np.sqrt(
                array_transformed_active ** 2.0 + array_transformed_reactive ** 2.0
            )
            self.array_transformed_phase = (
                array_transformed_active / array_transformed_module
            )

            self.array_transformed_phase_json = self.array_transformed_phase.to_json()
        except TypeError:
            print("NoneType: Transformed Active and/or Reactive Power is None.")

        self.outputs["device_transformed_phase"] = {
            "value": self.device_transformed_phase_json,
            "Aggregation-Level": "device",
            "Label": "Device Transfomerd energy phase",
            "Unit": "-",
            "Description": "Phase of energy after transformation stage per device",
            "Tag": "Transformed Energy",
        }
        self.outputs["array_transformed_phase"] = {
            "value": self.array_transformed_phase_json,
            "Aggregation-Level": "array",
            "Label": "Array Transformed energy phase",
            "Unit": "-",
            "Description": "Phase of energy after transformation stage at array level",
            "Tag": "Delivered Energy",
        }
        return self.device_transformed_phase, self.array_transformed_phase

    def delivered_phase(self):
        """
            Returns the phase at Energy Delivery phase
            Raises:
                TypeError: If Delivered Active and/or Reactive Power is None.
        """

        try:
            array_delivered_module = np.sqrt(
                self.delivered_active_power ** 2.0
                + self.delivered_reactive_power ** 2.0
            )
            self.array_delivered_phase = (
                self.delivered_active_power / array_delivered_module
            )
            self.array_delivered_phase_json = self.array_delivered_phase.to_json()
        except TypeError:
            print("NoneType: Delivered Active and/or Reactive Power is None.")
        self.outputs["array_delivered_phase"] = {
            "value": self.array_delivered_phase_json,
            "Aggregation-Level": "array",
            "Label": "Array Delivered energy phase",
            "Unit": "-",
            "Description": "Phase of energy at delivery at array level",
            "Tag": "Delivered Energy",
        }
        return self.array_delivered_phase
