# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt


class EfficiencyCpx1(object):
    """This class assesses *efficiency* of the Ocean Energy System 
    at the various subsystems and globally for projects at Level 1 of complexity.

    Args:
        average_flux (float): Average Annual Flux Resource 
            available in the site
        type (string): Tidal or Wave Energy Device
        charact_dimension (float): Characteristic dimension 
            of the Ocean Energy Absorber
        number_devices (int): number of devices
        rated_power (float): rated power of the Device
        annual_captured_energy (float, optional): Annual Array Captured Energy 
            by the devices because of the hydrodynamic interactions
            
        q_factor (float, optional): Ratio between the Annual Array Captured Energy 
            in the array and the product of the number of devices 
            multiplied by the Annual Caputerd Energy of the isolated 
            device
            
        annual_transformed_energy (float, optional): Annual Array Transformed Energy 
            after the PTO/control system
            
        annual_delivered_energy (float, optional): Annual Array Delivered Energy after 
            the Electrical Dispatch system

        device_annual_captured_energy (list of float, optional): Annual Device Captured Energy by 
            the devices because of the hydrodynamic interactions
            
        device_q_factor (list of float, optional): Ratio between the Annual Device Captured Energy  
                and the Annual Caputerd Energy of the isolated device
            
        device_annual_transformed_energy (list of float, optional): Annual Device Transformed Energy 
            after the PTO/control system
    
    Attributes:      
        captured_eff (float): Array Capture efficiency, defined as the ratio 
            between Annual Array Captured Energy and Rated Power of 
            the Array        
        transformed_abs_eff (float): Absolute Array Transformation Efficiency, 
            defined as the ratio between Annual Array Transformed Energy 
            and Rated Power of the Array        
        transformed_rel_eff (float): Relative Array Transforamtion Efficiency, 
            defined as the ratio between Annual Array Transformed Energy 
            and Annual Array Captured Resource        
        delivered_abs_eff (float): Absolute Array Delivery Efficiency, defined as 
            the ratio between Annual Array Delivered Energy and Rated Power of 
            the Array        
        delivered_rel_eff (float): Relative Array Delivery Efficiency, defined as the 
            ratio between Annual Array Delivered Energy and Annual Array Captured 
            Resource
        device_captured_eff (list of float): Device Capture efficiency, defined as the ratio 
            between Annual Device Captured Energy and Rated Power of the Device        
        device_transformed_abs_eff (list of float): Absolute Device Transformation Efficiency, 
            defined as the ratio between Annual Device Transformed Energy and 
            Rated Power of the Device        
        device_transformed_rel_eff (list of float): Relative Array Transforamtion Efficiency, 
            defined as the ratio between Annual Device Transformed Energy and 
            Annual Device Captured Resource
        rated_flux (float): ratio between the Rated Power of the device and the average 
            Power production of the Device
    """

    cpx = "Level of Complexity 1"

    def __init__(
        self,
        average_flux,
        tech_type,
        charact_dimension,
        number_devices,
        rated_power,
        annual_captured_energy=None,
        device_annual_captured_energy=None,
        q_factor=None,
        device_q_factor=None,
        annual_transformed_energy=None,
        device_annual_transformed_energy=None,
        annual_delivered_energy=None,
    ):

        self.ADE = annual_delivered_energy
        self.ATE = annual_transformed_energy
        self.ACE = annual_captured_energy
        self.DATE = device_annual_transformed_energy
        self.DACE = device_annual_captured_energy
        self.AF = average_flux
        self.type = tech_type
        self.charact_dimension = charact_dimension
        self.number_devices = number_devices
        self.rated_power = rated_power
        self.q_factor = q_factor
        self.device_q_factor = device_q_factor
        self.rated_flux = None
        self.captured_eff = None
        self.transformed_abs_eff = None
        self.device_captured_eff = None
        self.device_transformed_abs_eff = None
        self.delivered_abs_eff = None
        self.transformed_rel_eff = None
        self.device_transformed_rel_eff = None
        self.delivered_rel_eff = None
        self.inputs = {}
        self.outputs = {}

    def get_inputs(self):
        """
            Returns the inputs used for this class
        """
        units_af = {"Tidal": "kW/m^2", "Wave": "kW/m"}
        self.inputs = self.inputs = {
            "tech_type": {
                "value": self.type,
                "Label": "Technology Type",
                "Unit": "-",
                "Description": "Tidal or Wave Energy Converter",
            },
            "charact_dimension": {
                "value": self.charact_dimension,
                "Label": "Characteristic Dimension",
                "Unit": "m",
                "Description": "Charcteristic Dimension of the Prime Mover",
            },
            "number_devices": {
                "value": self.number_devices,
                "Label": "Number of Devices",
                "Unit": "-",
                "Description": "Number of Devices",
            },
            "rated_power": {
                "value": self.rated_power,
                "Label": "Rated Capacity of the Device",
                "Unit": "kW",
                "Description": "Rated Capacity of the Device",
            },
            "average_flux": {
                "value": self.AF,
                "Label": "Annual Average flux of Energy",
                "Unit": units_af[self.type],
                "Description": "Annual Average flux of Energy",
            },
            "annual_captured_energy": {
                "value": self.ACE,
                "Label": "Annual captured energy",
                "Unit": "kWh",
                "Description": "Annual captured energy",
            },
            "device_annual_captured_energy": {
                "value": self.DACE,
                "Label": "Device Annual captured energy",
                "Unit": "kWh",
                "Description": "Annual captured energy  per device",
            },
            "annual_transformed_energy": {
                "value": self.ATE,
                "Label": "Annual transformed energy",
                "Unit": "kWh",
                "Description": "Annual Energy after transformation stage",
            },
            "device_annual_transformed_energy": {
                "value": self.DATE,
                "Label": "Device Annual transformed energy",
                "Unit": "kWh",
                "Description": "Annual Energy after transformation stage per device",
            },
            "annual_delivered_energy": {
                "value": self.ADE,
                "Label": "Annual Delivered Energy",
                "Unit": "kWh",
                "Description": "Annual nergy delivered onshore",
            },
            "device_q_factor": {
                "value": self.device_q_factor,
                "Label": "Device q-factor",
                "Unit": "-",
                "Description": "q-factor per device",
            },
            "q_factor": {
                "value": self.q_factor,
                "Label": "Array q-factor",
                "Unit": "-",
                "Description": "q-factor of the array",
            },
        }
        return {
            "Level of Complexity": self.cpx,
            "Average Annual Flux": self.AF,
            "Technology Type": self.type,
            "Characteristic Dimension": self.charact_dimension,
            "Number of devices": self.number_devices,
            "Device Rated Power": self.rated_power,
            "Annual Array Captured Energy": self.ACE,
            "Annual Captured Energy per Device": self.DACE,
            "Array q-factor": self.q_factor,
            "q-factor per Device": self.device_q_factor,
            "Annual Array Transformed Energy": self.ATE,
            "Annual Transformed Energy per Device": self.DATE,
            "Annual Array Delivered Energy": self.ADE,
        }

    def estimate_rated_flux(self):
        """
            Returns the rated flux
        """

        if self.type == "Tidal":

            self.rated_flux = self.rated_power / (
                self.charact_dimension ** 2.0 * np.pi / 4.0 * self.AF
            )
        elif self.type == "Wave":
            self.rated_flux = self.rated_power / (self.charact_dimension * self.AF)

        self.outputs["rated_power_flux"] = {
            "value": self.rated_flux,
            "Aggregation-Level": "device",
            "Label": "Average Flux per Rated Power",
            "Unit": "-",
            "Description": "Ratio between the Rated Power and the Average Energy Undistrubed Flux",
            "Tag": "Rated Power",
        }
        return self.rated_flux

    def captured_efficiency(self):
        """
            Returns the effiency of the energy capture system
            Raises:
                ZeroDivisionError: If Rated Power is zero.
                TypeError: If Device/Array captured/transformed/delivered Energy is None.
        """

        try:
            self.captured_eff = self.ACE / (
                self.rated_power * self.number_devices * 365.25 * 24.0
            )

        except ZeroDivisionError:
            print("Infinity: Rated Power cannot be null.")
            raise ZeroDivisionError("Infinity: Rated Power cannot be null.")

        except TypeError:
            print("NoneType: Annual Array Captured Energy is None.")

        try:
            self.device_captured_eff = [
                self.DACE[i] / (self.rated_power * 365.25 * 24.0)
                for i in range(len(self.DACE))
            ]

        except ZeroDivisionError:
            print("Infinity: Rated Power cannot be null.")
            raise ZeroDivisionError("Infinity: Rated Power cannot be null.")

        except TypeError:
            print("NoneType: Annual Array Captured Energy is None.")

        self.outputs["array_capt_eff"] = {
            "value": self.captured_eff,
            "Aggregation-Level": "array",
            "Label": "Array Captured Efficiency",
            "Unit": "-",
            "Description": "Ratio between the Captured Energy at array level and the available energy",
            "Tag": "Captured Energy",
        }
        self.outputs["device_capt_eff"] = {
            "value": self.device_captured_eff,
            "Aggregation-Level": "device",
            "Label": "Device Captured Efficiency",
            "Unit": "-",
            "Description": "Ratio between the Captured Energy at device level and the available energy",
            "Tag": "Captured Energy",
        }
        return self.captured_eff, self.device_captured_eff

    def transformed_efficiency(self):
        """
            Returns the effiency of the energy transformation system
            Raises:
                ZeroDivisionError: If Rated Power or number of devices are zero.
                TypeError: If Device/Array captured/transformed/delivered Energy is None.
        """

        try:
            self.transformed_abs_eff = self.ATE / (
                self.rated_power * self.number_devices * 365.25 * 24.0
            )
            self.transformed_rel_eff = self.ATE / self.ACE

        except ZeroDivisionError:
            print("Infinity: Rated Power or number of devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Rated Power or number of devices cannot be null."
            )

        except TypeError:
            print("NoneType: Annual Transformed Energy is None.")

        try:
            self.device_transformed_abs_eff = [
                self.DATE[i] / (self.rated_power * 365.25 * 24.0)
                for i in range(len(self.DATE))
            ]
            self.device_transformed_rel_eff = [
                self.DATE[i] / (self.DACE[i]) for i in range(len(self.DATE))
            ]

        except ZeroDivisionError:
            print("Infinity: Rated Power cannot be null.")
            raise ZeroDivisionError("Infinity: Rated Power cannot be null.")

        except TypeError:
            print("NoneType: Annual Array Captured Energy is None.")

        self.outputs["array_abs_transf_eff"] = {
            "value": self.transformed_abs_eff,
            "Aggregation-Level": "array",
            "Label": "Absolute Array Transformed Efficiency",
            "Unit": "-",
            "Description": "Ratio between the Transformed Energy at array level and the available energy",
            "Tag": "Transformed Energy",
        }
        self.outputs["device_abs_transf_eff"] = {
            "value": self.device_transformed_abs_eff,
            "Aggregation-Level": "device",
            "Label": "Absolute Device Transformed Efficiency",
            "Unit": "-",
            "Description": "Ratio between the transformed energy at device level and the available energy",
            "Tag": "Captured Energy",
        }
        self.outputs["array_rel_transf_eff"] = {
            "value": self.transformed_rel_eff,
            "Aggregation-Level": "array",
            "Label": "Relative Array Transformed Efficiency",
            "Unit": "-",
            "Description": "Ratio between the transformed energy at array level and the captured energy",
            "Tag": "Transformed Energy",
        }
        self.outputs["device_rel_transf_eff"] = {
            "value": self.device_transformed_rel_eff,
            "Aggregation-Level": "device",
            "Label": "Relative Device Transformed Efficiency",
            "Unit": "-",
            "Description": "Ratio between the transformed energy at device level and the captured energy",
            "Tag": "Transformed Energy",
        }

        return (
            self.transformed_abs_eff,
            self.transformed_rel_eff,
            self.device_transformed_abs_eff,
            self.device_transformed_rel_eff,
        )

    def delivered_efficiency(self):
        """
            Returns the effiency of the energy delivery system
            Raises:
                ZeroDivisionError: If Rated Power or number of devices are zero.
                TypeError: If Device/Array captured/transformed/delivered Energy is None.
        """

        try:
            self.delivered_abs_eff = self.ADE / (
                self.rated_power * self.number_devices * 365.25 * 24.0
            )
            self.delivered_rel_eff = self.ADE / self.ATE

        except ZeroDivisionError:
            print("Infinity: Rated Power or number of devices cannot be null.")
            raise ZeroDivisionError(
                "Infinity: Rated Power or number of devices cannot be null."
            )

        except TypeError:
            print("NoneType: Annual Delivered Energy is None.")

        self.outputs["array_abs_deliv_eff"] = {
            "value": self.delivered_abs_eff,
            "Aggregation-Level": "array",
            "Label": "Absolute Array Delivered Efficiency",
            "Unit": "-",
            "Description": "Ratio between the delivered energy to shore at array level and the available energy",
            "Tag": "Delivered Energy",
        }
        self.outputs["array_rel_deliv_eff"] = {
            "value": self.delivered_rel_eff,
            "Aggregation-Level": "array",
            "Label": "Relative Array Delivered Efficiency",
            "Unit": "-",
            "Description": "Ratio between the delivered energy to shore at array level and the transformed energy",
            "Tag": "Transformed Energy",
        }

        return self.delivered_abs_eff, self.delivered_rel_eff
