# This is is the System Performance and Energy Yield (SPEY) for the DTOceanPlus suite of Tools.
# Copyright (C) 2021 TECNALIA - Vincenzo Nava
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
"""

from dtop_spey.business.cpx1 import (
    EfficiencyCpx1,
    PowerQualityCpx1,
    # EnergyProductionCpx1,
    AlternativeMetricsCpx1,
)
from dtop_spey.business.cpx2 import (
    EfficiencyCpx2,
    PowerQualityCpx2,
    # EnergyProductionCpx2,
    AlternativeMetricsCpx2,
)
from dtop_spey.business.cpx3 import (
    EfficiencyCpx3,
    PowerQualityCpx3,
    # EnergyProductionCpx3,
    AlternativeMetricsCpx3,
)

from dtop_shared_library.energy_production_base import EnergyProductionBase


class Efficiency:
    @staticmethod
    def get_cpx(
        cpx,
        average_flux,
        tech_type,
        charact_dimension,
        number_devices,
        rated_power,
        annual_captured_energy=None,
        device_annual_captured_energy=None,
        q_factor=None,
        device_q_factor=None,
        annual_transformed_energy=None,
        device_annual_transformed_energy=None,
        annual_delivered_energy=None,
    ):

        try:
            if cpx == "1":
                return EfficiencyCpx1(
                    average_flux,
                    tech_type,
                    charact_dimension,
                    number_devices,
                    rated_power,
                    annual_captured_energy,
                    device_annual_captured_energy,
                    q_factor,
                    device_q_factor,
                    annual_transformed_energy,
                    device_annual_transformed_energy,
                    annual_delivered_energy,
                )
            elif cpx == "2":
                return EfficiencyCpx2(
                    average_flux,
                    tech_type,
                    charact_dimension,
                    number_devices,
                    rated_power,
                    annual_captured_energy,
                    device_annual_captured_energy,
                    q_factor,
                    device_q_factor,
                    annual_transformed_energy,
                    device_annual_transformed_energy,
                    annual_delivered_energy,
                )
            elif cpx == "3":
                return EfficiencyCpx3(
                    average_flux,
                    tech_type,
                    charact_dimension,
                    number_devices,
                    rated_power,
                    annual_captured_energy,
                    device_annual_captured_energy,
                    q_factor,
                    device_q_factor,
                    annual_transformed_energy,
                    device_annual_transformed_energy,
                    annual_delivered_energy,
                )

            raise AssertionError("Stage not found")
        except AssertionError as _e:
            print(_e)
            raise


class PowerQuality:
    @staticmethod
    def get_cpx(
        cpx,
        transformed_active_power=None,
        transformed_reactive_power=None,
        delivered_active_power=None,
        delivered_reactive_power=None,
    ):

        try:
            if cpx == "1":
                return PowerQualityCpx1(
                    transformed_active_power,
                    transformed_reactive_power,
                    delivered_active_power,
                    delivered_reactive_power,
                )
            elif cpx == "2":
                return PowerQualityCpx2(
                    transformed_active_power,
                    transformed_reactive_power,
                    delivered_active_power,
                    delivered_reactive_power,
                )
            elif cpx == "3":
                return PowerQualityCpx3(
                    transformed_active_power,
                    transformed_reactive_power,
                    delivered_active_power,
                    delivered_reactive_power,
                )

            raise AssertionError("Stage not found")
        except AssertionError as _e:
            print(_e)
            raise


class EnergyProduction:
    @staticmethod
    def get_cpx(
        cpx,
        monthly_resource_histogram,
        power_delivery_histogram,
        number_devices,
        project_life,
        downtime_hours_per_device=None,
    ):

        try:
            if cpx == "1":
                energy_production = EnergyProductionBase(
                    monthly_resource_histogram,
                    power_delivery_histogram,
                    number_devices,
                    project_life,
                    downtime_hours_per_device,
                )
                setattr(energy_production, "cpx", "Level of Complexity 1")
                return energy_production
            elif cpx == "2":
                energy_production = EnergyProductionBase(
                    monthly_resource_histogram,
                    power_delivery_histogram,
                    number_devices,
                    project_life,
                    downtime_hours_per_device,
                )
                setattr(energy_production, "cpx", "Level of Complexity 2")
                return energy_production
            elif cpx == "3":
                energy_production = EnergyProductionBase(
                    monthly_resource_histogram,
                    power_delivery_histogram,
                    number_devices,
                    project_life,
                    downtime_hours_per_device,
                )
                setattr(energy_production, "cpx", "Level of Complexity 3")
                return energy_production

            raise AssertionError("Stage not found")
        except AssertionError as _e:
            print(_e)
            raise


class AlternativeMetrics:
    @staticmethod
    def get_cpx(
        cpx,
        average_flux,
        tech_type,
        charact_dimension,
        number_devices,
        rated_power,
        wetted_area_device,
        device_mass,
        annual_captured_energy=None,
        device_annual_captured_energy=None,
        annual_transformed_energy=None,
        device_annual_transformed_energy=None,
        PTO_force=None,
        annual_delivered_energy=None,
        export_cable_length=None,
        intra_array_cable_length=None,
        lease_area=None,
    ):
        try:
            if cpx == "1":
                return AlternativeMetricsCpx1(
                    average_flux,
                    tech_type,
                    charact_dimension,
                    number_devices,
                    rated_power,
                    wetted_area_device,
                    device_mass,
                    annual_captured_energy,
                    device_annual_captured_energy,
                    annual_transformed_energy,
                    device_annual_transformed_energy,
                    PTO_force,
                    annual_delivered_energy,
                    export_cable_length,
                    intra_array_cable_length,
                    lease_area,
                )
            elif cpx == "2":
                return AlternativeMetricsCpx2(
                    average_flux,
                    tech_type,
                    charact_dimension,
                    number_devices,
                    rated_power,
                    wetted_area_device,
                    device_mass,
                    annual_captured_energy,
                    device_annual_captured_energy,
                    annual_transformed_energy,
                    device_annual_transformed_energy,
                    PTO_force,
                    annual_delivered_energy,
                    export_cable_length,
                    intra_array_cable_length,
                    lease_area,
                )
            elif cpx == "3":
                return AlternativeMetricsCpx3(
                    average_flux,
                    tech_type,
                    charact_dimension,
                    number_devices,
                    rated_power,
                    wetted_area_device,
                    device_mass,
                    annual_captured_energy,
                    device_annual_captured_energy,
                    annual_transformed_energy,
                    device_annual_transformed_energy,
                    PTO_force,
                    annual_delivered_energy,
                    export_cable_length,
                    intra_array_cable_length,
                    lease_area,
                )

            raise AssertionError("Stage not found")
        except AssertionError as _e:
            print(_e)
            raise


# if __name__ == "__main__":
#     # EFFICIENCY = Efficiency.get_cpx('2',100,90,0.8,80,70)
#     Efficiency()
#     AlternativeMetrics()
